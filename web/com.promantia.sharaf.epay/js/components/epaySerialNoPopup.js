/*global enyo */
enyo.kind({
  kind: 'OB.UI.ModalAction',
  name: 'CPSE.UI.epayActivationSerialNo',
  hideCloseButton: true,
  autoDismiss: false,
  closeOnEscKey: false,
  events: {
    onHideThisPopup: '',
    onShowPopup: ''
  },
  bodyContent: {
    components: [{
      style: 'border-bottom: 1px solid #cccccc; text-align:center; margin-bottom: 15px;',
      name: 'product'
    }, {
      kind: 'epayserialNo',
    }]
  },
  bodyButtons: {
    components: [{
      kind: 'OB.UI.ModalDialogButton',
      name: 'ok',
      isDefaultAction: true,
      i18nContent: 'CPSE_LblOkButton',
      tap: function () {
        this.owner.owner.actionApply();
      }
    }]
  },

  actionApply: function () {
    var digiSerialno = this.$.bodyContent.$.epayserialNo.$.serialNoInput.getValue();
    if (digiSerialno.length === 0) {
      OB.UTIL.showConfirmation.display('Serial number cannot be blank- Please enter valid Serial Number');
      return;
    } else {
      if (!OB.UTIL.isNullOrUndefined(this.args.data)) {
        if (OB.UTIL.isNullOrUndefined(this.args.data.productToAdd)) {
          this.args.data.set('digiSerialNo', digiSerialno);
        } else {
          this.args.data.productToAdd.set('digiSerialNo', digiSerialno);
        }
      } else if (!OB.UTIL.isNullOrUndefined(this.args.epaydata)) {
        this.args.epaydata.set('digiSerialNo', digiSerialno);
      }
      this.doHideThisPopup();
      this.args.callback();
    }

  },

  executeOnShow: function () {
    if (this.args.data) {
      this.$.bodyContent.$.epayserialNo.$.serialNoInput.setValue('');
      if (OB.UTIL.isNullOrUndefined(this.args.data.productToAdd)) {
        this.$.bodyContent.$.product.setContent(this.args.data.get('_identifier'));
      } else {
        this.$.bodyContent.$.product.setContent(this.args.data.productToAdd.get('_identifier'));
      }
    } else if (!OB.UTIL.isNullOrUndefined(this.args.epaydata)) {
      this.$.bodyContent.$.epayserialNo.$.serialNoInput.setValue('');
      this.$.bodyContent.$.product.setContent(this.args.epaydata.get('_identifier'));
    }
  },
  init: function () {
    this.inherited(arguments);
  }
});

enyo.kind({
  name: 'epayserialNo',
  style: 'text-align: center;height: 80px;margin-left:5%;margin-right:5%;padding-bottom: 10px;',
  components: [{
    name: 'number',
    style: 'color:white;font-size:30px;font-weight:bold;'
  }, {
    name: 'serialNoInput',
    kind: 'enyo.Input',
    style: 'margin-top:10px;height: 25px;width: 80%;'
  }],

  init: function () {
    this.inherited(arguments);
    this.$.number.setContent(OB.I18N.getLabel('CPSE_LblSerialNo'));
  }
});


OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
  kind: 'CPSE.UI.epayActivationSerialNo',
  name: 'CPSE.UI.epayActivationSerialNo'
});