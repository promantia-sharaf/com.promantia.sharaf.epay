OB.EPAY_R = OB.EPAY_R || {};
OB.EPAY_R.Reinvoice = OB.EPAY_R.Reinvoice || {};

(function () {
  enyo.kind({
    kind: 'OB.UI.ModalAction',
    name: 'CPSE.UI.epayInvoiceType',
    hideCloseButton: true,
    autoDismiss: false,
    closeOnEscKey: false,
    bodyContent: {
      components: [{
        name: 'ReinvoiceLabel',
        classes: 'properties-label',
        components: [{
          style: 'padding: 5px 5px 5px 5px; font-size: 20px;  margin-left: 0px;',
          name: 'ReinvoiceLbl'
        }]
      }, {
        name: 'ReinvoiceOption',
        components: [{
          kind: 'OB.UI.List',
          name: 'ReinvoiceOptionsList',
          tag: 'select',
          classes: 'modal-dialog-profile-combo',
          renderEmpty: enyo.Control,
          renderLine: enyo.kind({
            kind: 'enyo.Option',
            initComponents: function () {
              this.inherited(arguments);
              this.setValue(this.model.get('id'));
              this.setContent(this.model.get('_identifier'));
            }
          })
        }]
      }, {
        tag: "br"
      }]
    },
    bodyButtons: {
      components: [{
        kind: 'CPSE.UI.reInvoiceBtnContinue'
      }]
    },
    executeOnShow: function () {
      var invType = new Backbone.Collection();
      this.invoicetype = null;
      this.$.bodyContent.$.ReinvoiceOptionsList.setCollection(invType);
      var type = [{
        'id': 'new_invoice',
        '_identifier': 'New Invoice'
      }, {
        'id': 're_invoice',
        '_identifier': 'Re Invoice'
      }]
      invType.reset(type);
    },
    executeOnHide: function () {
      var me = this;
      if (!OB.UTIL.isNullOrUndefined(this.args.data.productToAdd)) {
        if (this.$.bodyContent.$.ReinvoiceOptionsList.getValue() === 'new_invoice') {
          this.args.data.productToAdd.set('invoice_type', 'New Invoice')
        } else if (this.$.bodyContent.$.ReinvoiceOptionsList.getValue() === 're_invoice') {
          this.args.data.productToAdd.set('invoice_type', 'Re Invoice')
        }
        this.args.data.receipt.save();
      } else if (!OB.UTIL.isNullOrUndefined(this.args.data.context)) {
        var order = this.args.data.context.get('order');
        order.get('lines').forEach(function (line) {
          if (OB.UTIL.isNullOrUndefined(line.get('product').get('invoice_type'))) {
             if (me.$.bodyContent.$.ReinvoiceOptionsList.getValue() === 'new_invoice') {
                line.get('product').set('invoice_type', 'New Invoice')
             } else if (me.$.bodyContent.$.ReinvoiceOptionsList.getValue() === 're_invoice') {
                line.get('product').set('invoice_type', 'Re Invoice')
             }
          }
        });
        this.args.data.invoice_type = me.$.bodyContent.$.ReinvoiceOptionsList.getValue();
        order.save();
      } else if (!OB.UTIL.isNullOrUndefined(this.args.data)) {
        var order = this.args.data;
        order.get('lines').models.forEach(function (line) {
          if (OB.UTIL.isNullOrUndefined(line.get('cpseEpayresponse')) && OB.UTIL.isNullOrUndefined(line.get('invoice_type'))) {
            if (me.$.bodyContent.$.ReinvoiceOptionsList.getValue() === 'new_invoice') {
              line.get('product').set('invoice_type', 'New Invoice')
            } else if (me.$.bodyContent.$.ReinvoiceOptionsList.getValue() === 're_invoice') {
              line.get('product').set('invoice_type', 'Re Invoice')
            }
          }
        });
        this.args.data.invoice_type = me.$.bodyContent.$.ReinvoiceOptionsList.getValue();
        order.save();
      }

      this.args.callback();
    },
    initComponents: function () {

      this.inherited(arguments);
      enyo.forEach(this.newAttributes, function (natt) {
        this.$.bodyContent.$.attributes.createComponent(natt);
      }, this);
      this.$.header.setContent('Select Invoice Type');
      this.$.bodyContent.$.ReinvoiceLbl.setContent('Invoice Type');

    }
  });

  OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
    kind: 'CPSE.UI.epayInvoiceType',
    name: 'CPSE.UI.epayInvoiceType'
  });

  enyo.kind({
    kind: 'OB.UI.ModalDialogButton',
    name: 'CPSE.UI.reInvoiceBtnContinue',
    handlers: {
      ontap: "tapped",
    },
    initComponents: function () {
      this.inherited(arguments);
      this.setContent(OB.I18N.getLabel('OBMOBC_LblOk'));
    },
    tapped: function (inEvent, inSender) {
      this.doHideThisPopup();
    }
  });

}());
