
//register menu - Diagnostic API flow
  OB.OBPOSPointOfSale.UI.LeftToolbarImpl.prototype.menuEntries.push({
    kind: 'CPSE.UI.MenuQwikCilverInitialize'
  });
  
  enyo.kind({
	    name: 'CPSE.UI.MenuQwikCilverInitialize',
	    kind: 'OB.UI.MenuAction',
	    i18nLabel: 'CPSE_LblEPAYConnectivityCheck',
	    permission: 'CPSE_EPAY.CheckConnectivty',
	    tap: function () {
	      this.inherited(arguments); // auto close the menu
	      if (!OB.MobileApp.model.get('connectedToERP')) {
	        OB.UTIL.showError(OB.I18N.getLabel('OBPOS_OfflineWindowRequiresOnline'));
	        return;
	      }
	      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CPSE_EPAYConnectionCheckTitle'), OB.I18N.getLabel('CPSE_EPAYConnectionCheckMsg'), [{
	        isConfirmButton: true,
	        label: OB.I18N.getLabel('OBMOBC_LblOk'),
	        action: function () {
	        	var processEPAYDiagnostic = new OB.DS.Process('com.promantia.sharaf.epay.process.Diagnostic');
	        	var pos_date = new Date();
	        	var txn_id = Math.floor((Math.random() * 1000000000000000));
	        	processEPAYDiagnostic.exec({
	        		isDiagnostic: true,
	        		date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
	        		txnId: txn_id
	        	}, function(data){
	        		if (data && !data.exception) {
	  	              OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblSuccess'));
	  	            } else {
	  	              OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), data.exception.status.errorMessage);
	  	            }
	        	}, function(error){
	        		OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), error.exception.message);
	        	});

	        	return true;
	        }
	      }]);
	    },
	    init: function (model) {
	      this.model = model;
	      if (OB.MobileApp.model.hasPermission(this.permission, true)) {
	        this.show();
	      } else {
	        this.hide();
	      }
	    }
	  });