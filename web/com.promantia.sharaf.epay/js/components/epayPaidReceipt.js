/*global OB, Backbone, _, moment, enyo */

/*header of scrollable table*/
enyo.kind({
  name: 'CPSE.UI.ModalPRScrollableHeader',
  kind: 'OB.UI.ScrollableTableHeader',
  events: {
    onSearchAction: '',
    onClearAction: ''
  },
  handlers: {
    onFiltered: 'searchAction'
  },
  components: [{
    style: 'padding: 10px;',
    components: [{
      style: 'display: table;',
      components: [{
        style: 'display: table-cell; width: 100%;',
        components: [{
          kind: 'OB.UI.SearchInputAutoFilter',
          name: 'filterText',
          style: 'width: 100%',
          skipAutoFilterPref: 'OBPOS_remote.order'
        }]
      }, {
        style: 'display: table-cell;',
        components: [{
          kind: 'OB.UI.SmallButton',
          name: 'clearButton',
          classes: 'btnlink-gray btn-icon-small btn-icon-clear',
          style: 'width: 100px; margin: 0px 5px 8px 19px;',
          ontap: 'clearAction'
        }]
      }, {
        style: 'display: table-cell;',
        components: [{
          kind: 'OB.UI.SmallButton',
          name: 'searchButton',
          classes: 'btnlink-yellow btn-icon-small btn-icon-search',
          style: 'width: 100px; margin: 0px 0px 8px 5px;',
          ontap: 'searchAction'
        }]
      }]
    }, {
      style: 'display: table;',
      components: [{
        style: 'display: table-cell;',
        components: [{
          tag: 'h4',
          initComponents: function () {
            this.setContent(OB.I18N.getLabel('OBPOS_LblStartDate'));
          },
          style: 'width: 200px;  margin: 0px 0px 2px 5px;'
        }]
      }, {
        style: 'display: table-cell;',
        components: [{
          tag: 'h4',
          initComponents: function () {
            this.setContent(OB.I18N.getLabel('OBPOS_LblEndDate'));
          },
          style: 'width 200px; margin: 0px 0px 2px 65px;'
        }]
      }]
    }, {
      style: 'display: table;',
      components: [{
        style: 'display: table-cell;',
        components: [{
          kind: 'enyo.Input',
          name: 'startDate',
          size: '10',
          type: 'text',
          style: 'width: 100px;  margin: 0px 0px 8px 5px;',
          onchange: 'searchAction'
        }]
      }, {
        style: 'display: table-cell;',
        components: [{
          tag: 'h4',
          initComponents: function () {
            this.setContent(OB.I18N.getDateFormatLabel());
          },
          style: 'width: 100px; color:gray;  margin: 0px 0px 8px 5px;'
        }]
      }, {
        kind: 'enyo.Input',
        name: 'endDate',
        size: '10',
        type: 'text',
        style: 'width: 100px;  margin: 0px 0px 8px 50px;',
        onchange: 'searchAction'
      }, {
        style: 'display: table-cell;',
        components: [{
          tag: 'h4',
          initComponents: function () {
            this.setContent(OB.I18N.getDateFormatLabel());
          },
          style: 'width: 100px; color:gray;  margin: 0px 0px 8px 5px;'
        }]
      }]
    }]
  }],
  showValidationErrors: function (stDate, endDate) {
    var me = this;
    if (stDate === false) {
      this.$.startDate.addClass('error');
      setTimeout(function () {
        me.$.startDate.removeClass('error');
      }, 5000);
    }
    if (endDate === false) {
      this.$.endDate.addClass('error');
      setTimeout(function () {
        me.$.endDate.removeClass('error');
      }, 5000);
    }
  },
  disableFilterText: function (value) {
    this.$.filterText.setDisabled(value);
  },
  disableFilterButtons: function (value) {
    this.$.searchButton.setDisabled(value);
    this.$.clearButton.setDisabled(value);
  },
  clearAction: function () {
    if (!this.$.filterText.disabled) {
      this.$.filterText.setValue('');
    }
    this.$.startDate.setValue('');
    this.$.endDate.setValue('');
    this.doClearAction();
  },

  getDateFilters: function () {
    var startDate, endDate, startDateValidated = true,
        endDateValidated = true,
        formattedStartDate = '',
        formattedEndDate = '';
    startDate = this.$.startDate.getValue();
    endDate = this.$.endDate.getValue();

    if (startDate !== '') {
      startDateValidated = OB.Utilities.Date.OBToJS(startDate, OB.Format.date);
      if (startDateValidated) {
        formattedStartDate = OB.Utilities.Date.JSToOB(
        startDateValidated, 'yyyy-MM-dd');
      }
    }

    if (endDate !== '') {
      endDateValidated = OB.Utilities.Date.OBToJS(endDate, OB.Format.date);
      if (endDateValidated) {
        formattedEndDate = OB.Utilities.Date.JSToOB(
        endDateValidated, 'yyyy-MM-dd');
      }
    }

    if (startDate !== '' && startDateValidated && endDate !== '' && endDateValidated) {
      if (moment(endDateValidated).diff(moment(startDateValidated)) < 0) {
        endDateValidated = null;
        startDateValidated = null;
      }
    }

    if (startDateValidated === null || endDateValidated === null) {
      this.showValidationErrors(
      startDateValidated !== null, endDateValidated !== null);
      return false;
    }
    this.$.startDate.removeClass('error');
    this.$.endDate.removeClass('error');

    this.filters = _.extend(this.filters, {
      startDate: formattedStartDate,
      endDate: formattedEndDate
    });

    return true;
  },

  searchAction: function () {
    var params = this.parent.parent.parent.parent.parent.parent.parent.parent.params;

    this.filters = {
      documentType: params.isQuotation ? [OB.MobileApp.model.get('terminal').terminalType.documentTypeForQuotations] : [OB.MobileApp.model.get('terminal').terminalType.documentType, OB.MobileApp.model.get('terminal').terminalType.documentTypeForReturns],
      docstatus: params.isQuotation ? 'UE' : null,
      isQuotation: params.isQuotation ? true : false,
      isLayaway: params.isLayaway ? true : false,
      isReturn: params.isReturn ? true : false,
      isReinvoice: params.isReinvoice ? true : false,
      filterText: this.$.filterText.getValue(),
      pos: OB.MobileApp.model.get('terminal').id,
      client: OB.MobileApp.model.get('terminal').client,
      organization: OB.MobileApp.model.get('terminal').organization
    };

    if (!this.getDateFilters()) {
      return true;
    }

    this.doSearchAction({
      filters: this.filters
    });
    return true;
  }
});

/*items of collection*/
enyo.kind({
  name: 'CPSE.UI.ListPRsLine_reinvoice',
  kind: 'OB.UI.listItemButton',
  allowHtml: true,
  events: {
    onHideThisPopup: ''
  },
  tap: function () {
    this.inherited(arguments);
    this.doHideThisPopup();
  },
  components: [{
    name: 'line',
    style: 'line-height: 23px;',
    components: [{
      name: 'topLine'
    }, {
      style: 'color: #888888',
      name: 'bottonLine'
    }, {
      style: 'clear: both;'
    }]
  }],
  create: function () {
    var returnLabel = '';
    this.inherited(arguments);
    if (
    this.model.get('documentTypeId') === OB.MobileApp.model.get('terminal').terminalType.documentTypeForReturns) {
      this.model.set('totalamount', OB.DEC.mul(this.model.get('totalamount'), -1));
      returnLabel = ' (' + OB.I18N.getLabel('OBPOS_ToReturn') + ')';
    }
    this.$.topLine.setContent(
    this.model.get('documentNo') + ' - ' + this.model.get('businessPartner') + returnLabel);
    this.$.bottonLine.setContent(
    this.model.get('totalamount') + ' (' + this.model.get('orderDate').substring(0, 10) + ') ');

    OB.UTIL.HookManager.executeHooks('OBPOS_RenderPaidReceiptLine', {
      paidReceiptLine: this
    });

    this.render();
  }
});

/*scrollable table (body of modal)*/
enyo.kind({
  name: 'CPSE.UI.ListPRs',
  classes: 'row-fluid',
  handlers: {
    onSearchAction: 'searchAction',
    onClearAction: 'clearAction'
  },
  events: {
    onChangePaidReceipt: '',
    onShowPopup: '',
    onAddProduct: '',
    onHideThisPopup: '',
    onChangeCurrentOrder: ''
  },
  components: [{
    classes: 'span12',
    components: [{
      style: 'border-bottom: 1px solid #cccccc;',
      classes: 'row-fluid',
      components: [{
        classes: 'span12',
        components: [{
          name: 'prslistitemprinter',
          kind: 'OB.UI.ScrollableTable',
          scrollAreaMaxHeight: '350px',
          renderHeader: 'CPSE.UI.ModalPRScrollableHeader',
          renderLine: 'CPSE.UI.ListPRsLine_reinvoice',
          renderEmpty: 'OB.UI.RenderEmpty'
        }, {
          name: 'renderLoading',
          style: 'border-bottom: 1px solid #cccccc; padding: 20px; text-align: center; font-weight: bold; font-size: 30px; color: #cccccc',
          showing: false,
          initComponents: function () {
            this.setContent(OB.I18N.getLabel('OBPOS_LblLoading'));
          }
        }]
      }]
    }]
  }],
  clearAction: function () {
    this.prsList.reset();
    return true;
  },
  searchAction: function (inSender, inEvent) {
    var me = this,
        ordersLoaded = [],
        existingOrders = [],
        process = new OB.DS.Process('com.promantia.sharaf.epay.EPAYPaidReceiptsHeader'),
        i;
    me.filters = inEvent.filters;
    this.clearAction();
    this.$.prslistitemprinter.$.tempty.hide();
    this.$.prslistitemprinter.$.tbody.hide();
    this.$.prslistitemprinter.$.tlimit.hide();
    this.$.renderLoading.show();
    this.$.prslistitemprinter.$.theader.$.modalPRScrollableHeader.disableFilterButtons(true);
    var limit;
    if (!OB.MobileApp.model.hasPermission('OBPOS_remote.order', true)) {
      limit = OB.Model.Order.prototype.dataLimit;
    } else {
      limit = OB.Model.Order.prototype.remoteDataLimit ? OB.Model.Order.prototype.remoteDataLimit : OB.Model.Order.prototype.dataLimit;
    }
    for (i = 0; i < this.model.get('orderList').length; i++) {
      // When an canceller order is ready, the cancelled order cannot be opened
      if (this.model.get('orderList').models[i].get('replacedorder')) {
        existingOrders.push(
        this.model.get('orderList').models[i].get('replacedorder'));
      }
    }
    if (OB.MobileApp.model.hasPermission('OBPOS_orderLimit', true)) {
      limit = OB.DEC.abs(
      OB.MobileApp.model.hasPermission('OBPOS_orderLimit', true));
      OB.Model.Order.prototype.tempDataLimit = OB.DEC.abs(
      OB.MobileApp.model.hasPermission('OBPOS_orderLimit', true));
    }
    process.exec({
      filters: me.filters,
      _limit: limit + 1,
      _dateFormat: OB.Format.date
    }, function (data) {
      if (data) {
        if (data.exception) {
          me.$.prslistitemprinter.$.theader.$.modalPRScrollableHeader.disableFilterButtons(
          false);
          me.$.renderLoading.hide();
          me.prsList.reset();
          OB.UTIL.showConfirmation.display(
          OB.I18N.getLabel('OBMOBC_Error'), data.exception.message ? data.exception.message : OB.I18N.getLabel('OBMOBC_OfflineWindowRequiresOnline'));
          return;
        }
        ordersLoaded = [];
        _.each(data, function (iter) {
          me.model.get('orderList').newDynamicOrder(iter, function (order) {
            if (existingOrders.indexOf(order.id) === -1) {
              // Only push the order if not exists in previous receipts
              ordersLoaded.push(order);
            }
          });
        });
        me.prsList.reset(ordersLoaded);
        me.$.renderLoading.hide();
        if (data && data.length > 0) {
          me.$.prslistitemprinter.$.tbody.show();
        } else {
          me.$.prslistitemprinter.$.tempty.show();
        }
        me.$.prslistitemprinter.getScrollArea().scrollToTop();
      } else {
        OB.UTIL.showError(OB.I18N.getLabel('OBPOS_MsgErrorDropDep'));
      }
      me.$.prslistitemprinter.$.theader.$.modalPRScrollableHeader.disableFilterButtons(
      false);
    }, function (error) {
      me.$.prslistitemprinter.$.theader.$.modalPRScrollableHeader.disableFilterButtons(
      false);
      me.$.renderLoading.hide();
      me.prsList.reset();
      OB.UTIL.showConfirmation.display(
      OB.I18N.getLabel('OBMOBC_Error'), error && error.exception && error.exception.message ? error.exception.message : OB.I18N.getLabel('OBMOBC_OfflineWindowRequiresOnline'));
      return;
    }, true, 30000);
    return true;
  },
  prsList: null,
  init: function (model) {
    var me = this,
        process = new OB.DS.Process('org.openbravo.retail.posterminal.PaidReceipts');
    this.model = model;
    this.prsList = new Backbone.Collection();
    this.$.prslistitemprinter.setCollection(this.prsList);
    this.prsList.on('click', function (model) {
      function loadOrder(model) {
        OB.UTIL.showLoading(true);
        process.exec({
          orderid: model.get('id')
        }, function (data) {
          if (data && data[0]) {
            OB.UTIL.showLoading(false);
            //OB.EPAY_R.Reinvoice.args.callback();
            OB.EPAY_R.Reinvoice.args.return_order = data[0];
            OB.MobileApp.view.waterfallDown('onShowPopup', {
              popup: 'modalReturnReceipt_modified',
              args: {
                data: OB.EPAY_R.Reinvoice.args,
                callback: function () {
                  OB.EPAY_R.Reinvoice.args.callback();
                  //OB.UTIL.showLoading(false);
                  //OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                }
              }
            });
          } else {
            OB.UTIL.showError(OB.I18N.getLabel('OBPOS_MsgErrorDropDep'));
          }
        });
        return true;
      }
      OB.MobileApp.model.orderList.checkForDuplicateReceipts(model, loadOrder, undefined, undefined, true);
      return true;
    }, this);
  }
});

/*Modal definiton*/
enyo.kind({
  name: 'CPSE.UI.ModalPaidReceiptsModified',
  kind: 'OB.UI.Modal',
  topPosition: '125px',
  autoDismiss: false,
  i18nHeader: 'OBPOS_LblPaidReceipts',
  published: {
    params: null
  },
  body: {
    kind: 'CPSE.UI.ListPRs'
  },
  handlers: {
    onChangePaidReceipt: 'changePaidReceipt'
  },
  changePaidReceipt: function (inSender, inEvent) {
    this.model.get('orderList').addPaidReceipt(inEvent.newPaidReceipt);
    if (inEvent.newPaidReceipt.get('isLayaway')) {
      this.model.attributes.order.calculateReceipt();
    } else {
      this.model.attributes.order.calculateGrossAndSave(fals/'[]');
    }
    return true;
  },
  executeOnShow: function () {
    this.$.header.setContent('Re Invoice');
    OB.EPAY_R.Reinvoice.args = this.args;
    this.$.body.$.listPRs.$.prslistitemprinter.$.theader.$.modalPRScrollableHeader.disableFilterText(false);
    this.$.body.$.listPRs.$.prslistitemprinter.$.theader.$.modalPRScrollableHeader.clearAction();
  },
  executeOnHide: function () {
    if (!OB.UTIL.isNullOrUndefined(this.args.ontap) && this.args.ontap === 'hide') {
        var lines = [];
        var primaryLineid;
        for (var i = 0; i < OB.MobileApp.model.receipt.get('lines').models.length; i++) {
            line = OB.MobileApp.model.receipt.get('lines').models[i];
            if (line.get('custdisOffer') && line.productType !== 'S') {
                if (!OB.UTIL.isNullOrUndefined(line.get('custdisOrderline'))) {
                    primaryLineid = line.get('custdisOrderline');
                } else {
                    primaryLineid = line.id;
                }
            }
        }
        if (!OB.UTIL.isNullOrUndefined(primaryLineid)) {
            lines = _.filter(OB.MobileApp.model.receipt.get('lines').models, function(line) {
                return line.get('custdisOrderline') === primaryLineid || line.id === primaryLineid;
            });
        }
             OB.MobileApp.model.receipt.deleteLinesFromOrder(lines);
             return;
   }          
  },
  init: function (model) {
    this.model = model;
  }
});
OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
  kind: 'CPSE.UI.ModalPaidReceiptsModified',
  name: 'CPSE.UI.ModalPaidReceiptsModified'
});





//////// Product selection from paid receipt popup /////////////////////
/*global OB, enyo, _ */

(function () {

  enyo.kind({
    name: 'CPSE.UI.CheckboxButtonAll',
    kind: 'OB.UI.CheckboxButton',
    classes: 'modal-dialog-btn-check span1',
    style: 'width: 8%',
    events: {
      onCheckedAll: ''
    },
    handlers: {
      //onAllSelected: 'allSelected'
    },
    allSelected: function (inSender, inEvent) {
      if (inEvent.allSelected) {
        this.check();
      } else {
        this.unCheck();
      }
      return true;
    },
    tap: function () {
/*this.inherited(arguments);
      this.doCheckedAll({
        checked: this.checked
      });*/
    }
  });

  enyo.kind({
    name: 'CPSE.UI.CheckboxButtonReturn',
    kind: 'OB.UI.CheckboxButton',
    classes: 'modal-dialog-btn-check span1',
    style: 'width: 8%',
    handlers: {
      onCheckAll: 'checkAll'
    },
    events: {
      onLineSelected: ''
    },
    isGiftCard: false,
    checkAll: function (inSender, inEvent) {
      if (this.isGiftCard) {
        return;
      }
      if (inEvent.checked && this.parent.$.quantity.value > 0 && !this.parent.newAttribute.notReturnable) {
        this.check();
      } else {
        this.unCheck();
      }
      if (this.parent.$.quantity.value !== 0) {
        this.parent.$.quantity.setDisabled(!inEvent.checked);
        this.parent.$.qtyplus.setDisabled(!inEvent.checked);
        this.parent.$.qtyminus.setDisabled(!inEvent.checked);
      }
      if (this.parent.newAttribute.productType === 'S') {
        this.parent.$.quantity.setDisabled(true);
        this.parent.$.qtyplus.setDisabled(true);
        this.parent.$.qtyminus.setDisabled(true);
      }
    },
    tap: function () {
      if (this.isGiftCard || this.parent.$.quantity.value === 0 || this.parent.newAttribute.notReturnable) {
        OB.UTIL.showWarning(OB.I18N.getLabel('OBMOBC_LineCanNotBeSelected'));
        return;
      }
      this.inherited(arguments);
      this.parent.$.quantity.setDisabled(!this.checked);
      this.parent.$.qtyplus.setDisabled(!this.checked);
      this.parent.$.qtyminus.setDisabled(!this.checked);

      if (this.checked) {
        this.parent.$.quantity.focus();
      }
      this.doLineSelected({
        selected: this.checked
      });
      if (this.parent.newAttribute.productType === 'S') {
        this.parent.$.quantity.setDisabled(true);
        this.parent.$.qtyplus.setDisabled(true);
        this.parent.$.qtyminus.setDisabled(true);
      }
    }
  });

  enyo.kind({
    name: 'CPSE.UI.EditOrderLine',
    style: 'border-bottom: 1px solid #cccccc; text-align: center; color: black; padding-top: 9px;',
    handlers: {
      onApplyChange: 'applyChange'
    },
    events: {
      onCorrectQty: ''
    },
    isGiftCard: false,
    applyChange: function (inSender, inEvent) {
      var me = this;
      var model = inEvent.model;
      var index = inEvent.lines.indexOf(this.newAttribute);
      var line, promotionsfactor;
      if (index !== -1) {
        if (this.$.checkboxButtonReturn.checked) {
          var initialQty = inEvent.lines[index].quantity;
          var qty = this.$.quantity.getValue();
          var orderList = OB.MobileApp.model.orderList;
          enyo.forEach(orderList.models, function (order) {
            enyo.forEach(order.get('lines').models, function (l) {
              if (l.get('originalOrderLineId')) {
                if (l.get('product').id === me.newAttribute.id && l.get('originalOrderLineId') === me.newAttribute.lineId) {
                  qty = qty - l.get('qty');
                }
              }
            });
          });
          if (qty > inEvent.lines[index].remainingQuantity) {
            OB.UTIL.showWarning(OB.I18N.getLabel('OBRETUR_ExceedsQuantity') + ' ' + me.newAttribute.name);
            inEvent.lines[index].exceedsQuantity = true;
          }
          inEvent.lines[index].remainingQuantity = inEvent.lines[index].remainingQuantity - this.$.quantity.getValue();
          inEvent.lines[index].selectedQuantity = this.$.quantity.getValue();
          // update promotions amount to the quantity returned
          enyo.forEach(this.newAttribute.promotions, function (p) {
            if (!OB.UTIL.isNullOrUndefined(p)) {
              p.amt = OB.DEC.mul(p.amt, (me.$.quantity.getValue() / initialQty));
              p.actualAmt = OB.DEC.mul(p.actualAmt, (me.$.quantity.getValue() / initialQty));
              p.displayedTotalAmount = OB.DEC.mul(p.displayedTotalAmount, (me.$.quantity.getValue() / initialQty));
            }
          });
        } else {
          inEvent.lines.splice(index, 1);
        }
      }
    },
    components: [{
      kind: 'CPSE.UI.CheckboxButtonReturn',
      name: 'checkboxButtonReturn'
    }, {
      name: 'product',
      classes: 'span4'
    }, {
      name: 'attribute',
      classes: 'span4'
    }, {
      name: 'totalQuantity',
      classes: 'span2'
    }, {
      name: 'remainingQuantity',
      classes: 'span2'
    }, {
      name: 'fullyReturned',
      classes: 'btn-returned span2',
      showing: false
    }, {
      name: 'qtyminus',
      kind: 'OB.UI.SmallButton',
      classes: 'btnlink-gray btnlink-cashup-edit span1',
      ontap: 'subUnit'
    }, {
      kind: 'enyo.Input',
      type: 'text',
      classes: 'input span1',
      name: 'quantity',
      isFirstFocus: true,
      selectOnFocus: true,
      autoKeyModifier: 'num-lock',
      onchange: 'validate'
    }, {
      name: 'qtyplus',
      kind: 'OB.UI.SmallButton',
      classes: 'btnlink-gray btnlink-cashup-edit span1',
      ontap: 'addUnit'
    }, {
      name: 'price',
      classes: 'span2'
    }, {
      style: 'clear: both;'
    }],
    addUnit: function (inSender, inEvent) {
      var units = parseInt(this.$.quantity.getValue(), 10);
      if (!isNaN(units) && units < this.$.quantity.getAttribute('max')) {
        this.$.quantity.setValue(units + 1);
        this.validate();
      }

    },
    subUnit: function (inSender, inEvent) {
      var units = parseInt(this.$.quantity.getValue(), 10);
      if (!isNaN(units) && units > this.$.quantity.getAttribute('min')) {
        this.$.quantity.setValue(units - 1);
        this.validate();
      }
    },
    validate: function () {
      var value, maxValue;
      value = this.$.quantity.getValue();
      try {
        value = parseFloat(this.$.quantity.getValue());
      } catch (e) {
        this.addStyles('background-color: red');
        this.doCorrectQty({
          correctQty: false
        });
        return true;
      }
      maxValue = OB.DEC.toNumber(OB.DEC.toBigDecimal(this.$.quantity.getAttribute('max')));

      if (!_.isNumber(value) || _.isNaN(value)) {
        this.addStyles('background-color: red');
        this.doCorrectQty({
          correctQty: false
        });
        return true;
      }

      value = OB.DEC.toNumber(OB.DEC.toBigDecimal(value));
      this.$.quantity.setValue(value);


      if (value > maxValue || value <= 0) {
        this.addStyles('background-color: red');
        this.doCorrectQty({
          correctQty: false
        });
      } else {
        this.addStyles('background-color: white');
        this.doCorrectQty({
          correctQty: true
        });
        return true;
      }
    },
    markAsGiftCard: function () {
      this.isGiftCard = true;
      this.$.checkboxButtonReturn.isGiftCard = this.isGiftCard;
    },
    tap: function () {
      if (this.isGiftCard === true || this.$.quantity.value === 0 || this.newAttribute.notReturnable) {
        OB.UTIL.showWarning(OB.I18N.getLabel('OBMOBC_LineCanNotBeSelected'));
      }
    },
    initComponents: function () {
      var movementQty = OB.DEC.Zero,
          me = this,
          windowWidth = window.innerWidth,
          fontSize = 17,
          componentWidth = [179, 85, 35, 45, 25, 100],
          backgroundSize = '34px 27px';
      _.each(this.newAttribute.shipmentlines, function (shipment) {
        //shipment.remainingQty = -shipment.remainingQty
        movementQty += shipment.remainingQty;
      });
      this.newAttribute.remainingQuantity = movementQty;
      this.inherited(arguments);
      me.$.qtyminus.setContent(OB.I18N.getLabel('OBMOBC_Character')[3]);
      me.$.qtyplus.setContent(OB.I18N.getLabel('OBMOBC_Character')[4]);
      if (me.newAttribute.shipmentlines.length === 0) {
        me.$.checkboxButtonReturn.setDisabled(true);
        me.$.checkboxButtonReturn.removeClass('btn-icon-check');
        me.$.checkboxButtonReturn.addClass('btn-icon-canceled');
        me.$.quantity.hide();
        me.$.qtyplus.hide();
        me.$.qtyminus.hide();
        me.$.fullyReturned.setContent(OB.I18N.getLabel('OBPOS_Cancelled'));
        me.$.fullyReturned.addStyles('color: #CC9999');
        me.$.fullyReturned.show();
      } else if (!me.newAttribute.returnable) {
        me.$.checkboxButtonReturn.setDisabled(true);
        me.$.checkboxButtonReturn.removeClass('btn-icon-check');
        me.$.checkboxButtonReturn.addClass('btn-icon-notreturnable');
        me.$.quantity.hide();
        me.$.qtyplus.hide();
        me.$.qtyminus.hide();
        me.$.fullyReturned.setContent(OB.I18N.getLabel('OBRETUR_notreturnable'));
        me.$.fullyReturned.addStyles('color: #C80000');
        me.$.fullyReturned.show();
      } else if (me.newAttribute.remainingQuantity < -1) {
        me.$.checkboxButtonReturn.setDisabled(true);
        me.$.checkboxButtonReturn.removeClass('btn-icon-check');
        me.$.checkboxButtonReturn.addClass('btn-icon-returned');
        me.$.quantity.hide();
        me.$.qtyplus.hide();
        me.$.qtyminus.hide();
        me.$.fullyReturned.setContent('Fully Re Invoiced');
        me.$.fullyReturned.show();
      }
      me.$.product.setContent(me.newAttribute.name);
      if (OB.MobileApp.model.hasPermission('OBPOS_EnableSupportForProductAttributes', true)) {
        if (me.newAttribute.attributeValue && _.isString(me.newAttribute.attributeValue)) {
          var processedAttValues = OB.UTIL.AttributeUtils.generateDescriptionBasedOnJson(me.newAttribute.attributeValue);
          if (processedAttValues && processedAttValues.keyValue && _.isArray(processedAttValues.keyValue) && processedAttValues.keyValue.length > 0) {
            _.each(processedAttValues.keyValue, function (item) {
              me.$.attribute.createComponent({
                content: item
              });
            });
            me.newAttribute.attSetInstanceDesc = processedAttValues.description;
          } else {
            me.$.attribute.setContent('NA');
          }
        } else {
          me.$.attribute.setContent('NA');
        }
      } else {
        me.$.attribute.hide();
      }

      if (me.newAttribute.remainingQuantity < -1) {
        me.$.remainingQuantity.setContent(-me.newAttribute.quantity);
        me.$.quantity.setValue(-me.newAttribute.quantity);
      } else {
        me.$.remainingQuantity.setContent(-me.newAttribute.remainingQuantity);
        me.$.quantity.setValue(-me.newAttribute.remainingQuantity);
      }
      me.$.totalQuantity.setContent(-me.newAttribute.quantity);
      me.$.quantity.setDisabled(true);
      me.$.qtyplus.setDisabled(true);
      me.$.qtyminus.setDisabled(true);
      //me.$.quantity.setValue(-me.newAttribute.remainingQuantity);
      me.$.quantity.setAttribute('max', me.newAttribute.remainingQuantity);
      me.$.quantity.setAttribute('min', OB.DEC.One);
      me.$.price.setContent(me.newAttribute.priceIncludesTax ? me.newAttribute.unitPrice : me.newAttribute.baseNetUnitPrice);

      //Font dynamically reduced
      if (windowWidth >= 510 && windowWidth < 610) {
        componentWidth[0] = 130;
        componentWidth[1] = 75;
        componentWidth[5] = 70;
        backgroundSize = '30px 18px';
      } else if (windowWidth >= 410 && windowWidth < 510) {
        fontSize = 12;
        componentWidth[0] = 130;
        componentWidth[1] = 55;
        componentWidth[2] = 25;
        componentWidth[3] = 25;
        componentWidth[4] = 15;
        componentWidth[5] = 40;
        backgroundSize = '24px 16px';
      } else if (windowWidth >= 375 && windowWidth < 410) {
        fontSize = 12;
        componentWidth[0] = 110;
        componentWidth[1] = 55;
        componentWidth[2] = 20;
        componentWidth[3] = 25;
        componentWidth[4] = 20;
        componentWidth[5] = 40;
        backgroundSize = '22px 14px';
      } else if (windowWidth < 375) {
        fontSize = 10;
        componentWidth[0] = 100;
        componentWidth[1] = 45;
        componentWidth[2] = 18;
        componentWidth[3] = 20;
        componentWidth[4] = 10;
        componentWidth[5] = 25;
        backgroundSize = '20px 12px';
      }
      me.$.checkboxButtonReturn.style = 'background-size: ' + backgroundSize + ';';
      me.$.product.style = 'line-height: 35px; font-size: ' + fontSize + 'px; width: ' + componentWidth[0] + 'px;';
      me.$.attribute.style = 'line-height: 35px; font-size: ' + fontSize + 'px; width: ' + componentWidth[0] + 'px; text-align: left;';
      me.$.totalQuantity.style = 'line-height: 35px; font-size: ' + fontSize + 'px; width: ' + componentWidth[1] + 'px;';
      me.$.remainingQuantity.style = 'line-height: 35px; font-size: ' + fontSize + 'px; width: ' + componentWidth[1] + 'px;';
      me.$.qtyminus.style = 'width: ' + componentWidth[2] + 'px; color: #666666; height: ' + componentWidth[2] + 'px; margin-top: 0px; font-size: 20px; padding-right: 4px;padding-left: 4px;padding-bottom: 10px;';
      me.$.quantity.style = 'margin-right: 2px; text-align: center; width: ' + componentWidth[3] + 'px; height: ' + componentWidth[4] + 'px; font-size: ' + fontSize + 'px;';
      me.$.qtyplus.style = 'width: ' + componentWidth[2] + 'px; color: #666666; height: ' + componentWidth[2] + 'px; margin-top: 0px; font-size: 20px; padding-right: 4px;padding-left: 4px;padding-bottom: 10px;';
      me.$.price.style = 'line-height: 35px; font-size: ' + fontSize + 'px; width: ' + componentWidth[5] + 'px; margin-left: 15px;';

      if (me.newAttribute.promotions.length > 0) {
        me.$.quantity.addStyles('margin-bottom:0px');
        enyo.forEach(me.newAttribute.promotions, function (d) {
          if (d.hidden) {
            // continue
            return;
          }
          me.createComponent({
            style: 'display: block; color:gray; font-size:13px; line-height: 20px;  width: 150px;',
            components: [{
              content: '-- ' + d.name,
              attributes: {
                style: 'float: left; width: 155%; margin-top: -8px;padding-bottom: 15px;'
              }
            }, {
              content: OB.I18N.formatCurrency(-d.amt),
              attributes: {
                style: 'float: left; width: 40%; padding-left: 390%; margin-top:-35px; padding-bottom: 15px;'
              }
            }, {
              style: 'clear: both;'
            }]
          });
        }, me);

      }
      // shipment info
      if (!OB.UTIL.isNullOrUndefined(me.newAttribute.shiplineNo)) {
        me.createComponent({
          style: 'display: block; color:gray; font-size:13px; line-height: 20px;',
          components: [{
            content: me.newAttribute.shipment + ' - ' + me.newAttribute.shiplineNo,
            attributes: {
              style: 'float: left; width: 60%;'
            }
          }, {
            style: 'clear: both;'
          }]
        });
      }
    }
  });

  enyo.kind({
    kind: 'OB.UI.ModalDialogButton',
    name: 'CPSE.UI.ReturnReceiptDialogApply',
    events: {
      onApplyChanges: '',
      onCallbackExecutor: '',
      onCheckQty: '',
      onValidateAttributeValue: ''
    },
    tap: function () {
      if (this.doCheckQty()) {
        return true;
      }
      var me = this,
          selLine = this.owner.owner.getSelectedLines();
      receiptLines = this.owner.owner.args.data.return_order.receiptLines;

      if (this.owner.owner.getSelectedLines().length > 1 && OB.UTIL.isNullOrUndefined(selLine[0].line.custdisOffer)) {
        OB.UTIL.showError('Please select product ' + OB.EPAY_R.Reinvoice.args.data.productToAdd.get('description') + ' to Re-Invoice');
      } else if (!OB.UTIL.isNullOrUndefined(selLine[0]) && !OB.UTIL.isNullOrUndefined(selLine[0].line.custdisOffer) && !OB.UTIL.isNullOrUndefined(OB.EPAY_R.Reinvoice.promotion) && OB.EPAY_R.Reinvoice.promotion.get('id') !== selLine[0].line.custdisOffer) {
        OB.UTIL.showError('Please select promotion ' + OB.EPAY_R.Reinvoice.promotion.get('_identifier') + ' to Re-Invoice');
      } else if (this.owner.owner.getSelectedLines().length < 0) {
        OB.UTIL.showError('Please select the product to re-invoice and click on apply button');
      } else if (this.owner.owner.getSelectedLines().length === 0) {
        var lines = [];
        var primaryLineid;
        for (var i = 0; i < OB.MobileApp.model.receipt.get('lines').models.length; i++) {
            line = OB.MobileApp.model.receipt.get('lines').models[i];
            if (line.get('custdisOffer') && line.productType !== 'S') {
                if (!OB.UTIL.isNullOrUndefined(line.get('custdisOrderline'))) {
                    primaryLineid = line.get('custdisOrderline');
                } else {
                    primaryLineid = line.id;
                }
            }
        }
        if (!OB.UTIL.isNullOrUndefined(primaryLineid)) {
            lines = _.filter(OB.MobileApp.model.receipt.get('lines').models, function(line) {
                return line.get('custdisOrderline') === primaryLineid || line.id === primaryLineid;
            });
        }
             OB.MobileApp.model.receipt.deleteLinesFromOrder(lines);
             me.doHideThisPopup();
      } else {
        promotionLinkValidation(selLine, receiptLines, OB.EPAY_R.Reinvoice.args.data.receipt, function () {
          if (!OB.UTIL.isNullOrUndefined(OB.EPAY_R.Reinvoice.args.data.receipt)) {
            if (checkIfLineAlreadyAdded(selLine, OB.EPAY_R.Reinvoice.args.data.receipt)) {
              OB.UTIL.showError('This article is already added to the ticket');
            } else {
              if (selLine[0].line.id !== OB.EPAY_R.Reinvoice.args.data.productToAdd.id) {
                OB.UTIL.showError('Please select product ' + OB.EPAY_R.Reinvoice.args.data.productToAdd.get('description') + ' only to Re-Invoice');
              } else {
                //save original epay data in the reinvoice lines
                OB.EPAY_R.Reinvoice.args.data.productToAdd.set('originalEpaySerialno', me.owner.owner.getSelectedLines()[0].line.cpseEpaySerialno);
                OB.EPAY_R.Reinvoice.args.data.productToAdd.set('originalEpayRequest', me.owner.owner.getSelectedLines()[0].line.cpseEpayrequest);
                OB.EPAY_R.Reinvoice.args.data.productToAdd.set('originalEpayResponse', me.owner.owner.getSelectedLines()[0].line.cpseEpayresponse);
                // save original Epcccode, Serial No, Rfid and Barcode in the reinvoice lines
                if (!OB.UTIL.isNullOrUndefined(me.owner.owner.getSelectedLines()[0].line.obposEpccode)) {
                  OB.EPAY_R.Reinvoice.args.data.productToAdd.set('obposEpccode', me.owner.owner.getSelectedLines()[0].line.obposEpccode);
                }
                if (!OB.UTIL.isNullOrUndefined(me.owner.owner.getSelectedLines()[0].line.obposSerialNumber)) {
                  OB.EPAY_R.Reinvoice.args.data.productToAdd.set('obposSerialNumber', me.owner.owner.getSelectedLines()[0].line.obposSerialNumber);
                }
                if (!OB.UTIL.isNullOrUndefined(me.owner.owner.getSelectedLines()[0].line.obposRfidValid)) {
                  OB.EPAY_R.Reinvoice.args.data.productToAdd.set('obposRfidValid', me.owner.owner.getSelectedLines()[0].line.obposRfidValid);
                }
                if (!OB.UTIL.isNullOrUndefined(me.owner.owner.getSelectedLines()[0].line.obposBarcode)) {
                  OB.EPAY_R.Reinvoice.args.data.productToAdd.set('obposBarcode', me.owner.owner.getSelectedLines()[0].line.obposBarcode);
                }
                //Append Return order line Id to this line... used to link goods shipment line 
                OB.EPAY_R.Reinvoice.args.data.attrs.returnLineId = me.owner.owner.getSelectedLines()[0].line.lineId;
                OB.EPAY_R.Reinvoice.args.data.attrs.receiptLines = receiptLines;
                OB.EPAY_R.Reinvoice.args.data.attrs.selectedLines = selLine;
                OB.EPAY_R.Reinvoice.args.data.attrs.retDocumentNo = me.owner.owner.args.data.return_order.documentNo;
                OB.EPAY_R.Reinvoice.args.data.attrs.returnOrder = me.owner.owner.args.data.return_order;
                me.doHideThisPopup();
                OB.EPAY_R.Reinvoice.args.callback();
              }
            }
          } else {
            if (checkIfLineAlreadyAdded(selLine, OB.EPAY_R.Reinvoice.args.data)) {
              OB.UTIL.showError('This article is already added to the ticket');
            } else {
              if (selLine[0].line.id !== OB.EPAY_R.Reinvoice.prodToAddId && OB.UTIL.isNullOrUndefined(selLine[0].line.custdisOffer)) {
                OB.UTIL.showError('Please select product ' + selLine[0].line.name + ' only to Re-Invoice');
              } else {
                //save original epay data in the reinvoice lines
                OB.EPAY_R.Reinvoice.args.data.set('originalEpaySerialno', me.owner.owner.getSelectedLines()[0].line.cpseEpaySerialno);
                OB.EPAY_R.Reinvoice.args.data.set('originalEpayRequest', me.owner.owner.getSelectedLines()[0].line.cpseEpayrequest);
                OB.EPAY_R.Reinvoice.args.data.set('originalEpayResponse', me.owner.owner.getSelectedLines()[0].line.cpseEpayresponse);
                // save original Epcccode, Serial No, Rfid and Barcode in the reinvoice lines
                if (!OB.UTIL.isNullOrUndefined(me.owner.owner.getSelectedLines()[0].line.obposEpccode)) {
                  OB.EPAY_R.Reinvoice.args.data.set('obposEpccode', me.owner.owner.getSelectedLines()[0].line.obposEpccode);
                }
                if (!OB.UTIL.isNullOrUndefined(me.owner.owner.getSelectedLines()[0].line.obposSerialNumber)) {
                  OB.EPAY_R.Reinvoice.args.data.set('obposSerialNumber', me.owner.owner.getSelectedLines()[0].line.obposSerialNumber);
                }
                if (!OB.UTIL.isNullOrUndefined(me.owner.owner.getSelectedLines()[0].line.obposRfidValid)) {
                  OB.EPAY_R.Reinvoice.args.data.set('obposRfidValid', me.owner.owner.getSelectedLines()[0].line.obposRfidValid);
                }
                if (!OB.UTIL.isNullOrUndefined(me.owner.owner.getSelectedLines()[0].line.obposBarcode)) {
                  OB.EPAY_R.Reinvoice.args.data.set('obposBarcode', me.owner.owner.getSelectedLines()[0].line.obposBarcode);
                }
                //Append Return order line Id to this line... used to link goods shipment line 
                getReturnOrderLineForBundleReinvoice(OB.EPAY_R.Reinvoice.args.data, receiptLines);
                OB.EPAY_R.Reinvoice.args.data.set('receiptLines', receiptLines);
                OB.EPAY_R.Reinvoice.args.data.set('selectedLines', selLine);
                OB.EPAY_R.Reinvoice.args.data.set('retDocumentNo', me.owner.owner.args.data.return_order.documentNo);
                OB.EPAY_R.Reinvoice.args.data.set('returnOrder', me.owner.owner.args.data.return_order);
                me.doHideThisPopup();
                OB.EPAY_R.Reinvoice.args.callback();
              }
            }
          }
        });

      }
    },
    initComponents: function () {
      this.inherited(arguments);
      this.setContent(OB.I18N.getLabel('OBMOBC_LblApply'));
    }
  });

  promotionLinkValidation = function (selLine, receiptLines, receipt, callbackFound) {

    var returnUtils = {

      mainProductLine: [],
      selectedLineIds: [],

      // Determine if all products for a promotion are selected to return
      promotionHaveAllProductIncluded: function (lineId) {
        var result = true,
            promoLines = _.filter(receiptLines, function (line) {
            return line.custdisOrderline === lineId || line.lineId === lineId;
          });
        _.each(promoLines, function (line) {
          var selectedLine = _.find(selLine, function (l) {
            return line.lineId === l.line.lineId || (l.line.isSecondaryQtyZero ? l.line.isSecondaryQtyZero : false);
          });
          if (!selectedLine) {
            result = false;
          }
        });
        return result;
      },

      // Add main product line
      addMainProduct: function (lineId) {
        var line = _.find(this.mainProductLine, function (l) {
          return l === lineId;
        });
        if (!line) {
          this.mainProductLine.push(lineId);
        }
      }
    };

    // Find main promotion products selected to be returned
    var selectedProduct = [];
    _.each(selLine, function (line) {
      selectedProduct.push(line.line.custdisOrderline ? line.line.custdisOrderline : line.line.lineId);
    });

    // Find main promotion products in ticket
    _.each(receiptLines, function (line) {
      if (line.custdisOffer && line.productType !== 'S') {
        if (!OB.UTIL.isNullOrUndefined(line.custdisOrderline)) {
          returnUtils.addMainProduct(line.custdisOrderline);
        } else {
          returnUtils.addMainProduct(line.lineId);
        }
      }
    });

    // Verify if main product with promo has been already returned
    var retMainProductWithPromo = false;
    _.each(receiptLines, function (line) {
      if (line.lineId === returnUtils.mainProductLine[0] && line.remainingQuantity === 0) {
        retMainProductWithPromo = true;
      }
    });

    var error = false; {
      var indexArr = [];
      if (selectedProduct.length > 0) {
        for (var i = 0; i < returnUtils.mainProductLine.length; i++) {
          if (!selectedProduct.includes(returnUtils.mainProductLine[i])) {
            indexArr.push(returnUtils.mainProductLine[i]);
          }
        }
      }

      for (var i = 0; i < indexArr.length; i++) {
        var indexMP = returnUtils.mainProductLine.indexOf(indexArr[i]);
        if (indexMP > -1) {
          returnUtils.mainProductLine.splice(indexMP, 1);
        }
      }

      // Verify if return a complete promotion for other promotion types
      _.each(returnUtils.mainProductLine, function (line) {
        if (!returnUtils.promotionHaveAllProductIncluded(line)) {
          error = true;
          var info = [],
              promoLines = _.filter(receiptLines, function (l) {
              return l.custdisOrderline === line || l.lineId === line;
            });
          _.each(promoLines, function (l) {
            if (info.length === 0) {
              info.push('You must link these lines too:');
            }
            info.push('- ' + l.name + ' (' + OB.I18N.formatCurrency(l.unitPrice) + ')');
          });
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), info);
        }
      });
    }

    if (!error) {
      // Set Credit Notes payment method as selected payment method.
      if (OB.MobileApp.model.paymentnames['GCNV_payment.creditnote']) {
        OB.MobileApp.model.receipt.set('selectedPayment', 'GCNV_payment.creditnote');
      }
      callbackFound();
    }
  };

  getReturnOrderLineForBundleReinvoice = function (data, receiptLines) {
    var returnLineArr = [];
    for (var i = 0; i < OB.MobileApp.model.receipt.get('lines').models.length; i++) {
      var line = OB.MobileApp.model.receipt.get('lines').models[i];
      //Append Return order line Id to this line... used to link goods shipment line
      if (OB.UTIL.isNullOrUndefined(line.get('returnLineId'))) {
        for (var j = 0; j < receiptLines.length; j++) {
          returnLineArr.push(receiptLines[j].lineId);
        }
        data.set('returnLineId', returnLineArr);
      }
    }
  };

  checkIfLineAlreadyAdded = function (selLine, receipt) {
    var lineExists = false;
    if (!OB.UTIL.isNullOrUndefined(receipt.get('lines'))) {
      _.each(receipt.get('lines').models, function (line) {
        if (line.get('returnLineId') === selLine[0].line.lineId) {
          lineExists = true
        }
      });
    } else {
      _.each(OB.MobileApp.model.receipt.get('lines').models, function (line) {
        if (line.get('returnLineId') === selLine[0].line.lineId) {
          lineExists = true
        }
      });
    }
    return lineExists;
  };

  enyo.kind({
    kind: 'OB.UI.ModalDialogButton',
    name: 'CPSE.UI.ReturnReceiptDialogCancel',
    tap: function () {
      this.doHideThisPopup();
      var orderline, reinvoiceLines, reinvoicePrimaryLine, bundlePromoReInvoiceArr = [],
          promoIncomplete = false,
          receiptLines = OB.MobileApp.model.receipt.attributes.lines.models;

      for (var i = receiptLines.length - 1; i >= 0; i--) {
        orderline = receiptLines[i];
        break;
      }

      if (!OB.UTIL.isNullOrUndefined(orderline) && !OB.UTIL.isNullOrUndefined(orderline.get('custdisOffer')) && OB.UTIL.isNullOrUndefined(orderline.get('custdisOrderline'))) {
        reinvoiceLines = this.owner.owner.$.bodyContent.$.attributes.controls;

        for (var j = 0; j < reinvoiceLines.length; j++) {
          if (orderline.get('product').get('_identifier') === reinvoiceLines[j].newAttribute.name) {
            if (!OB.UTIL.isNullOrUndefined(reinvoiceLines[j].newAttribute.custdisOffer) && orderline.get('custdisOffer') === reinvoiceLines[j].newAttribute.custdisOffer) {
              reinvoicePrimaryLine = reinvoiceLines[j];
              break;
            }
          }
        }
        if (!OB.UTIL.isNullOrUndefined(reinvoicePrimaryLine)) {
          bundlePromoReInvoiceArr.push(reinvoicePrimaryLine);
          for (var j = 0; j < reinvoiceLines.length; j++) {
            if (!OB.UTIL.isNullOrUndefined(reinvoiceLines[j].newAttribute.custdisOrderline) && reinvoicePrimaryLine.newAttribute.lineId === reinvoiceLines[j].newAttribute.custdisOrderline) {
              bundlePromoReInvoiceArr.push(reinvoiceLines[j]);
            }
          }

          if (receiptLines.length < bundlePromoReInvoiceArr.length) {
            // delete receiptlines
            for (var i = 0; i < receiptLines.length; i++) {
              OB.MobileApp.model.receipt.deleteLine(receiptLines[i]);
            }
          } else {
            for (var i = 0; i < receiptLines.length; i++) {
              if (!bundlePromoReInvoiceArr.includes(receiptLines[i].get('product').name)) {
                promoIncomplete = true;
              }
            }
            if (promoIncomplete) {
              // delete receiptlines
              for (var i = 0; i < receiptLines.length; i++) {
                OB.MobileApp.model.receipt.deleteLine(receiptLines[i]);
              }
            }
          }
        } else {
          OB.MobileApp.model.receipt.deleteLine(orderline);
        }
      } else {
        var lines = [];
        var primaryLineid;
        for (var i = 0; i < OB.MobileApp.model.receipt.get('lines').models.length; i++) {
            line = OB.MobileApp.model.receipt.get('lines').models[i];
            if (line.get('custdisOffer')) {
                if (!OB.UTIL.isNullOrUndefined(line.get('custdisOrderline'))) {
                    primaryLineid = line.get('custdisOrderline');
                } else {
                    primaryLineid = line.id;
                }
            }
        }
        if (!OB.UTIL.isNullOrUndefined(primaryLineid)) {
            lines = _.filter(OB.MobileApp.model.receipt.get('lines').models, function(line) {
                return line.get('custdisOrderline') === primaryLineid || line.id === primaryLineid;
            });
        }
        OB.MobileApp.model.receipt.deleteLinesFromOrder(lines);
        return;
        }
    },
    initComponents: function () {
      this.inherited(arguments);
      this.setContent(OB.I18N.getLabel('OBMOBC_LblCancel'));
    }
  });

  enyo.kind({
    name: 'CPSE.UI.ModalReturnReceipt_modified',
    kind: 'OB.UI.ModalAction',
    classes: 'modal-dialog',
    correctQty: true,
    handlers: {
      onApplyChanges: 'applyChanges',
      onCallbackExecutor: 'callbackExecutor',
      //onCheckedAll: 'checkedAll',
      onCheckQty: 'checkQty',
      onCorrectQty: 'changeCorrectQty',
      onValidateAttributeValue: 'validateAttributeValue',
      onLineSelected: 'lineSelected'
    },
    lineShouldBeIncludedFunctions: [],
    bodyContent: {
      kind: 'Scroller',
      maxHeight: '225px',
      style: 'background-color: #ffffff;margin-top: -7px;',
      thumb: true,
      horizontal: 'hidden',
      components: [{
        name: 'attributes'
      }]
    },
    bodyButtons: {
      components: [{
        kind: 'CPSE.UI.ReturnReceiptDialogApply'
      }, {
        kind: 'CPSE.UI.ReturnReceiptDialogCancel'
      }]
    },
    applyChanges: function (inSender, inEvent) {
      this.waterfall('onApplyChange', {
        lines: this.args.args.order.receiptLines,
        model: this.args.args.context.model
      });
      return true;
    },
    validateAttributeValue: function (inSender, inEvent) {
      var i, productWithAttributeValue = [],
          lines = this.args.args.order.receiptLines,
          productHasAttribute = false;
      lines.forEach(function (theLine) {
        if (OB.UTIL.isNullOrUndefined(theLine.attributeValue) === false) {
          productWithAttributeValue.push(theLine);
          productHasAttribute = true;
        }
      });
    },
    checkedAll: function (inSender, inEvent) {
      if (inEvent.checked) {
        this.selectedLines = this.numberOfLines;
        this.allSelected = true;
      } else {
        this.selectedLines = 0;
        this.allSelected = false;
      }
      this.waterfall('onCheckAll', {
        checked: inEvent.checked
      });

      return true;
    },
    checkQty: function (inSender, inEvent) {
      if (!this.correctQty) {
        return true;
      }
    },
    changeCorrectQty: function (inSender, inEvent) {
      this.correctQty = inEvent.correctQty;
    },
    lineSelected: function (inSender, inEvent) {
      if (inEvent.selected) {
        this.selectedLines += 1;
      } else {
        this.selectedLines -= 1;
      }
      if (this.selectedLines === this.numberOfLines) {
        this.allSelected = true;
        this.waterfall('onAllSelected', {
          allSelected: this.allSelected
        });
        return true;
      } else {
        if (this.allSelected) {
          this.allSelected = false;
          this.waterfall('onAllSelected', {
            allSelected: this.allSelected
          });
        }
      }
    },
    getSelectedLines: function () {
      var lines = [];
      _.each(this.$.bodyContent.$.attributes.$, function (line) {
        if (line.$.checkboxButtonReturn.checked) {
          lines.push({
            line: line.newAttribute,
            qty: line.$.quantity.getValue()
          });
        }
      });
      return lines;
    },
    executeOnShow: function () {
      //OB.EPAY_R.Reinvoice.args.callback();
      var me = this,
          lineNum = 0,
          windowWidth = window.innerWidth,
          fontSize = 17,
          componentWidth = [179, 85, 155, 100],
          backgroundSize = '34px 27px';
      OB.UTIL.showLoading(false);
      this.autoDismiss = false;
      this.closeOnEscKey = false;
      this.$.bodyContent.$.attributes.destroyComponents();
      this.$.header.destroyComponents();
      this.$.header.createComponent({
        name: 'CheckAllHeaderDocNum',
        style: 'text-align: center; color: white;',
        components: [{
          content: me.args.data.return_order.documentNo,
          name: 'documentNo',
          classes: 'span12',
          style: 'line-height: 5px; font-size: 24px;'
        }, {
          style: 'clear: both;'
        }]
      });
      if (!this.$.header.$.checkboxButtonAll) {
        this.$.header.addStyles('padding-bottom: 0px; margin: 0px; height: 102px;');

        //Font dynamically reduced
        if (windowWidth >= 700) {
          me.domStyles.width = '700px';
        } else {
          me.domStyles.width = '';
        }

        if (windowWidth >= 510 && windowWidth < 610) {
          componentWidth[0] = 130;
          componentWidth[1] = 75;
          componentWidth[2] = 155;
          componentWidth[3] = 70;
          backgroundSize = '30px 18px';
        } else if (windowWidth >= 410 && windowWidth < 510) {
          fontSize = 12;
          componentWidth[0] = 130;
          componentWidth[1] = 55;
          componentWidth[2] = 105;
          componentWidth[3] = 70;
          backgroundSize = '24px 16px';
        } else if (windowWidth >= 375 && windowWidth < 410) {
          fontSize = 12;
          componentWidth[0] = 110;
          componentWidth[1] = 55;
          componentWidth[2] = 105;
          componentWidth[3] = 50;
          backgroundSize = '22px 14px';
        } else if (windowWidth < 375) {
          fontSize = 10;
          componentWidth[0] = 100;
          componentWidth[1] = 45;
          componentWidth[2] = 80;
          componentWidth[3] = 50;
          backgroundSize = '20px 12px';
        }

        this.$.header.createComponent({
          name: 'CheckAllHeader',
          style: 'overflow: hidden; padding-top: 5px; border-bottom: 3px solid #cccccc; text-align: center; color: black; margin-top: 30px; padding-top: 7px; padding-bottom: 7px;  font-weight: bold; background-color: white; height:46px;',
          components: [{
            kind: 'CPSE.UI.CheckboxButtonAll',
            name: 'checkboxButtonAll',
            style: 'background-size: ' + backgroundSize + ';'
          }, {
            content: OB.I18N.getLabel('OBRETUR_LblProductName'),
            name: 'productNameLbl',
            classes: 'span4',
            style: 'line-height: 25px; font-size: ' + fontSize + 'px;  width: ' + componentWidth[0] + 'px; padding-top: 10px;'
          }, {
            content: OB.I18N.getLabel('OBRETUR_LblAttributeValue'),
            name: 'attributeValueLbl',
            classes: 'span2',
            style: 'line-height: 25px; font-size: ' + fontSize + 'px;  width: ' + componentWidth[0] + 'px; padding-top: 10px;'
          }, {
            name: 'totalQtyLbl',
            content: OB.I18N.getLabel('OBRETUR_LblTotalQty'),
            classes: 'span2',
            style: 'line-height: 25px; font-size: ' + fontSize + 'px; width: ' + componentWidth[1] + 'px; padding-top: 10px;'
          }, {
            name: 'remainingQtyLbl',
            content: OB.I18N.getLabel('OBRETUR_LblRemainingQty'),
            classes: 'span2',
            style: 'line-height: 25px; font-size: ' + fontSize + 'px; width: ' + componentWidth[1] + 'px; padding-top: 10px;'
          }, {
            content: OB.I18N.getLabel('OBRETUR_LblQty'),
            name: 'qtyLbl',
            classes: 'span3',
            style: 'line-height: 25px; font-size: ' + fontSize + 'px; width: ' + componentWidth[2] + 'px; padding-top: 10px;'
          }, {
            content: OB.I18N.getLabel('OBRETUR_LblPrice'),
            name: 'priceLbl',
            classes: 'span2',
            style: 'line-height: 25px; font-size: ' + fontSize + 'px; width: ' + componentWidth[3] + 'px; padding-top: 10px;'
          }, {
            style: 'clear: both;'
          }]
        });
      } else {
        this.$.header.$.checkboxButtonAll.unCheck();
      }
      this.numberOfLines = 0;
      this.selectedLines = 0;
      this.allSelected = false;

      enyo.forEach(this.args.data.return_order.receiptLines, function (line) {
        lineNum++;
        var isSelectableLine = true;
        _.each(this.lineShouldBeIncludedFunctions, function (f) {
          isSelectableLine = isSelectableLine && f.isSelectableLine(line);
        });
        var lineEnyoObject = this.$.bodyContent.$.attributes.createComponent({
          kind: 'CPSE.UI.EditOrderLine',
          name: 'line' + lineNum,
          newAttribute: line
        });
        if (!isSelectableLine) {
          lineEnyoObject.markAsGiftCard();
        }
        this.numberOfLines += 1;
        line.notReturnable = !line.returnable;
      }, this);

      this.$.bodyContent.$.attributes.render();
      if (!OB.MobileApp.model.hasPermission('OBPOS_EnableSupportForProductAttributes', true)) {
        this.$.header.$.attributeValueLbl.hide();
      } else {
        this.addStyles('width: 900px');
      }
      this.$.header.render();
      // Set correctQty to true onShow the popup.
      this.waterfall('onCorrectQty', {
        correctQty: true
      });
    },

    executeOnHide: function () {
      if (!OB.UTIL.isNullOrUndefined(this.args.ontap) && this.args.ontap === 'hide') {
        var lines = OB.MobileApp.model.receipt.attributes.lines.models;
        for (var i = 0; i < lines.length; i++) {
          OB.MobileApp.model.receipt.deleteLine(lines[i]);
        }
      }
    },

    initComponents: function () {
      this.inherited(arguments);
      this.attributeContainer = this.$.bodyContent.$.attributes;
    }
  });
  OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
    kind: 'CPSE.UI.ModalReturnReceipt_modified',
    name: 'modalReturnReceipt_modified'
  });
}());
