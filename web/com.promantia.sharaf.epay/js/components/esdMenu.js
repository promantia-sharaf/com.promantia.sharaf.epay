
//register menu - Diagnostic API flow
  OB.OBPOSPointOfSale.UI.LeftToolbarImpl.prototype.menuEntries.push({
    kind: 'CPSE.UI.MenuESDInitialize'
  });
  
  enyo.kind({
        name: 'CPSE.UI.MenuESDInitialize',
        kind: 'OB.UI.MenuAction',
        i18nLabel: 'CPSE_LblESDValidatePartner',
        permission: 'CPSE_ESD.ValidatePartner',
        tap: function () {
          this.inherited(arguments); // auto close the menu
          if (!OB.MobileApp.model.get('connectedToERP')) {
            OB.UTIL.showError(OB.I18N.getLabel('OBPOS_OfflineWindowRequiresOnline'));
            return;
          }
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CPSE_ESDValidatePartnerTitle'), OB.I18N.getLabel('CPSE_ESDValidateParnterMsg'), [{
            isConfirmButton: true,
            label: OB.I18N.getLabel('OBMOBC_LblOk'),
            action: function () {
                var processESDValidatePartner = new OB.DS.Process('com.promantia.sharaf.epay.process.ESDValidatePartner');
                var pos_date = new Date();
                var txn_id = Math.floor((Math.random() * 1000000000000000));
                processESDValidatePartner.exec({
                    isDiagnostic: true,
                    date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
                    txnId: txn_id
                }, function(data){
                    if (data && !data.exception) {
                      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblSuccess'));
                    } else {
                      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), data.exception.message);
                    }
                }, function(error){
                    OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), error.exception.message);
                });

                return true;
            }
          }]);
        },
        init: function (model) {
          this.model = model;
          if (OB.MobileApp.model.hasPermission(this.permission, true)) {
            this.show();
          } else {
            this.hide();
          }
        }
      });