/*global OB, enyo, _ */
(function () {
  OB.UTIL.HookManager.registerHook('OBPOS_PreDeleteCurrentOrder', function (args, callbacks) {
    var lineCount = 0;
    if (args.receipt.get('custsdtDocumenttypeSearchKey') !== 'BS') {

      //Logic before deleting a line to check if order is verified return order
      if (!OB.UTIL.isNullOrUndefined(args.receipt.get('isVerifiedReturn'))) {
        errorArticles = [], epayAPIError = false;
        epayApiReponseNullArticles = [], epayAPIResponseNull = false;

        for (var i = 0; i < args.receipt.get('lines').length; i++) {
          args.receipt.get('lines').models[i].set('lineNumber', i + 1);
        }

        args.receipt.get('lines').forEach(function (line) {
          if (OB.UTIL.isNullOrUndefined(line.get('originalDocumentNo')) && OB.UTIL.isNullOrUndefined(line.get('originalOrderLineId'))) {
            if (!OB.UTIL.isNullOrUndefined(line.get('product').get('invoice_type')) && line.get('product').get('invoice_type') !== 'Re Invoice' && !OB.UTIL.isNullOrUndefined(line.get('isVerifiedReturn')) && line.get('isVerifiedReturn')) {
              if (line.get('product').get('custqcDigiApiResponse') === true) {
                errorArticles.push(line.get('product').get('description') + ' in line no ' + line.get('lineNumber') + ' ');
                epayAPIError = true
              }

              //If API is called and response is not collected at the POS... do not delete the articles from the ticket
              if (!OB.UTIL.isNullOrUndefined(args.receipt.get('paymentButtonClicked')) && OB.UTIL.isNullOrUndefined(line.get('product').get('custqcDigiApiResponse'))) {
                epayApiReponseNullArticles.push(line.get('product').get('description') + ' in line no ' + line.get('lineNumber') + ' ');
                epayAPIResponseNull = true;
              }
            }
          }
        });
        if (epayAPIError) {
          OB.UTIL.showConfirmation.display('Cannot remove article ' + [errorArticles] + ' as it is de-activated');
          args.cancelOperation = true;
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
          return
        }
        if (epayAPIResponseNull) {
          OB.UTIL.showConfirmation.display('Cannot remove article ' + [epayApiReponseNullArticles] + '. Please proceed for payment');
          args.cancelOperation = true;
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
          return
        }
      }

      var me = this;
      if (args.receipt.get('isQuotation') || args.cancelOperation) {
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        return;
      }

      if (args.receipt.get('lines').length === 0) {
        //If deleting empty order... do nothing.
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      } else {
       callAPIOrder(args, args.receipt.get('lines'), lineCount, callbacks);
      }

    } else {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
  });
}());
      var callAPIOrder = function (args,lines,lineCount,callbacks) {
        if (lineCount < lines.length) {
          var line;
          if(!OB.UTIL.isNullOrUndefined(lines.models)) {
            var line = lines.models[lineCount];
          } else {
            var line = lines[lineCount];
          }
          
          if (line.get('product').has('epayResponse')) {
            var prod = line.get('product'),
                requestTpye;

            //Cancellation flow
            if (prod.get('epayResponse').TYPE === 'PINPRINTING') {
              var processEPAYPINCancel = new OB.DS.Process('com.promantia.sharaf.epay.process.PINPrintCancellation');
              OB.UTIL.showLoading(true);
              if (!OB.MobileApp.model.get('connectedToERP')) {
                var query = "insert into CPSE_DigitalProducts(id, upc, txnRef, amount, currency, salesRep, docNo, prodId, isPinCancel, digitalProductType, date, lineNo, txnId, esdResponse, isDeactivation, serialNo, organization, pos)" 
                          + "values('" + OB.UTIL.get_UUID() + "',"
                          + "'" + prod.get('uPCEAN') + "',"
                          + "'" + prod.get('epayResponse').TXID + "',"
                          + "'" + line.get('price') + "',"
                          + "'" + OB.MobileApp.model.get('currency').cpseEpayisocode + "',"
                          + "'" + args.receipt.get('salesRepresentative') + "',"
                          + "'" + args.receipt.get('documentNo') + "',"
                          + "'" + prod.get('id') + "',"
                          + "'" + true + "',"
                          + "'" + prod.get('digiProductType') + "',"
                          + "'" + moment(pos_date).format('YYYY-MM-DD HH:mm:ss') + "',"
                          + "'" + line.get('id') + "',"
                          + "'" + line.get('id') + '_' + Math.floor((Math.random() * 1000000)) + "',"
                          + "'" + JSON.stringify(prod.get('epayResponse')) + "',"
                          + "'" + false + "',"
                          + "'" + prod.get('digiSerialNo') + "',"
                          + "'" + OB.MobileApp.model.attributes.terminal.organization + "',"
                          + "'" + OB.MobileApp.model.attributes.terminal.id + "'"
                          +");";
            
                OB.Dal.queryUsingCache(OB.Model.CPSE_DigitalProducts, query, [], function (success) {
                    OB.UTIL.showLoading(false);
                    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                });
              } else {
                  var pos_date = new Date();
                  var txn_id = line.get('id') + '_' + Math.floor((Math.random() * 1000000));
                  var start_date = new Date();
                  processEPAYPINCancel.exec({
                    upc: prod.get('uPCEAN'),
                    txnRef: prod.get('epayResponse').TXID,
                    amount: line.get('price'),
                    currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
                    salesRep: args.receipt.get('salesRepresentative'),
                    docNo: args.receipt.get('documentNo'),
                    prodId: prod.get('id'),
                    digitalProductType: prod.get('digiProductType'),
                    isPinCancel: true,
                    date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
                    lineNo: line.get('id'),
                    txnId: txn_id,
                    esdResponse: prod.get('epayResponse')
                  }, function (data) {
                    OB.UTIL.showLoading(false);
    
                    if (data && !data.exception && data.eResponseCode === '0') {
                      lineCount++;
                      callAPIOrder(args,lines,lineCount,callbacks);
                    } else if (data.exception) {
                      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), data.exception.status.errorMessage);
                      lineCount++;
                      callAPIOrder(args,lines,lineCount,callbacks);
                    }
    
                  }, function (error) {
                    pos_date = new Date();
                    var is_cancel_success = false;
                    OB.UTIL.EPAY.call_cancel_API(args, txn_id, pos_date, line, function () {
                      if (args.is_cancel_api_success) {
                       lineCount++;
                       callAPIOrder(args,lines,lineCount,callbacks);
                      } else {
                        var is_cancel_success = false;
                        OB.UTIL.EPAY.call_cancel_API(args, txn_id, pos_date, line, function () {
                          if (args.is_cancel_api_success) {
                            lineCount++;
                            callAPIOrder(args,lines,lineCount,callbacks);
                          } else {
                            var is_cancel_success = false;
                            OB.UTIL.EPAY.call_cancel_API(args, txn_id, pos_date, line, function () {
                              if (args.is_cancel_api_success) {
                                lineCount++;
                                callAPIOrder(args,lines,lineCount,callbacks);
                              } else {
                                lineCount++;
                                callAPIOrder(args,lines,lineCount,callbacks);
                              }
                            });
                          }
                        });
                      }
                    });
                  });
              }
              //Deactivation flow
            } else if (prod.get('epayResponse').TYPE === 'ACTIVATE') {
              var processEPAYPINCancel = new OB.DS.Process('com.promantia.sharaf.epay.process.POSDeactivation');
              OB.UTIL.showLoading(true);
              if (!OB.MobileApp.model.get('connectedToERP')) {
                var query = "insert into CPSE_DigitalProducts(id, upc, txnRef, amount, currency, salesRep, docNo, prodId, isPinCancel, digitalProductType, date, lineNo, txnId, esdResponse, isDeactivation, serialNo, organization, pos)" 
                          + "values('" + OB.UTIL.get_UUID() + "',"
                          + "'" + prod.get('uPCEAN') + "',"
                          + "'" + prod.get('epayResponse').TXID + "',"
                          + "'" + line.get('price') + "',"
                          + "'" + OB.MobileApp.model.get('currency').cpseEpayisocode + "',"
                          + "'" + args.receipt.get('salesRepresentative') + "',"
                          + "'" + args.receipt.get('documentNo') + "',"
                          + "'" + prod.get('id') + "',"
                          + "'" + false + "',"
                          + "'" + prod.get('digiProductType') + "',"
                          + "'" + moment(pos_date).format('YYYY-MM-DD HH:mm:ss') + "',"
                          + "'" + line.get('id') + "',"
                          + "'" + line.get('id') + '_' + Math.floor((Math.random() * 1000000)) + "',"
                          + "'" + JSON.stringify(prod.get('epayResponse')) + "',"
                          + "'" + true + "',"
                          + "'" + prod.get('digiSerialNo') + "',"
                          + "'" + OB.MobileApp.model.attributes.terminal.organization + "',"
                          + "'" + OB.MobileApp.model.attributes.terminal.id + "'"
                          +");";
            
                OB.Dal.queryUsingCache(OB.Model.CPSE_DigitalProducts, query, [], function (success) {
                    OB.UTIL.showLoading(false);
                    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                });
              } else {
                  var pos_date = new Date();
                  var txn_id = line.get('id') + '_' + Math.floor((Math.random() * 1000000));
                  var start_date = new Date();
                  processEPAYPINCancel.exec({
                    upc: prod.get('uPCEAN'),
                    txnRef: prod.get('epayResponse').TXID,
                    amount: line.get('price'),
                    serialNo: prod.get('digiSerialNo'),
                    currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
                    salesRep: args.receipt.get('salesRepresentative'),
                    docNo: args.receipt.get('documentNo'),
                    prodId: prod.get('id'),
                    isDeactivation: true,
                    date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
                    lineNo: line.get('id'),
                    txnId: txn_id
                  }, function (data) {
                    OB.UTIL.showLoading(false);
    
                    if (data && !data.exception && data.eResponseCode === '0') {
                      lineCount++;
                      callAPIOrder(args,lines,lineCount,callbacks);
                    } else if (data.exception) {
                      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), data.exception.status.errorMessage);
                      lineCount++;
                      callAPIOrder(args,lines,lineCount,callbacks);
                    }
    
                  }, function (error) {
                    pos_date = new Date();
                    txn_id = Math.floor((Math.random() * 1000000000000000));
                    var is_cancel_success = false;
                    OB.UTIL.EPAY.call_cancel_API_Deactivation(args, txn_id, pos_date, line, function () {
                      if (args.is_cancel_api_success) {
                        lineCount++;
                        callAPIOrder(args,lines,lineCount,callbacks);
                      } else {
                        var is_cancel_success = false;
                        OB.UTIL.EPAY.call_cancel_API_Deactivation(args, txn_id, pos_date, line, function () {
                          if (args.is_cancel_api_success) {
                            lineCount++;
                            callAPIOrder(args,lines,lineCount,callbacks);
                          } else {
                            var is_cancel_success = false;
                            OB.UTIL.EPAY.call_cancel_API_Deactivation(args, txn_id, pos_date, line, function () {
                              if (args.is_cancel_api_success) {
                                lineCount++;
                                callAPIOrder(args,lines,lineCount,callbacks);
                              } else {
                                lineCount++;
                                callAPIOrder(args,lines,lineCount,callbacks);
                              }
                            });
                          }
                        });
                      }
                    });
                  });
              }
            } else if (prod.get('epayResponse').TYPE === 'SALE') {
              if(prod.get('digiProductType') === 'SP') {  
              var processSubscribePINPRINTCancel = new OB.DS.Process('com.promantia.sharaf.epay.process.SubscribePINPRINTCancellation');
              OB.UTIL.showLoading(true);
              if (!OB.MobileApp.model.get('connectedToERP')) {
                var query = "insert into CPSE_DigitalProducts(id, upc, txnRef, amount, currency, salesRep, docNo, prodId, isPinCancel, digitalProductType, date, lineNo, txnId, esdResponse, isDeactivation, serialNo, organization, pos)" 
                          + "values('" + OB.UTIL.get_UUID() + "',"
                          + "'" + prod.get('uPCEAN') + "',"
                          + "'" + prod.get('epayResponse').TXID + "',"
                          + "'" + selLine.get('price') + "',"
                          + "'" + OB.MobileApp.model.get('currency').cpseEpayisocode + "',"
                          + "'" + args.order.get('salesRepresentative') + "',"
                          + "'" + args.order.get('documentNo') + "',"
                          + "'" + prod.get('id') + "',"
                          + "'" + true + "',"
                          + "'" + prod.get('digiProductType') + "',"
                          + "'" + moment(pos_date).format('YYYY-MM-DD HH:mm:ss') + "',"
                          + "'" + selLine.get('id') + "',"
                          + "'" + selLine.get('id') + '_' + Math.floor((Math.random() * 1000000)) + "',"
                          + "'" + JSON.stringify(prod.get('epayResponse')) + "',"
                          + "'" + false + "',"
                          + "'" + prod.get('digiSerialNo') + "',"
                          + "'" + OB.MobileApp.model.attributes.terminal.organization + "',"
                          + "'" + OB.MobileApp.model.attributes.terminal.id + "'"
                          +");";
            
                OB.Dal.queryUsingCache(OB.Model.CPSE_DigitalProducts, query, [], function (success) {
                    OB.UTIL.showLoading(false);
                    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                });
              } else {
                  var pos_date = new Date();
                  var txn_id = line.get('id') + '_' + Math.floor((Math.random() * 1000000));
                  var start_date = new Date();
                  processSubscribePINPRINTCancel.exec({
                    upc: prod.get('uPCEAN'),
                    txnRef: prod.get('epayResponse').TXID,
                    amount: line.get('price'),
                    currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
                    salesRep: args.receipt.get('salesRepresentative'),
                    docNo: args.receipt.get('documentNo'),
                    prodId: prod.get('id'),
                    isSubscribePINPRINTCancel: true,
                    digitalProductType: prod.get('digiProductType'),
                    date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
                    lineNo: line.get('id'),
                    txnId: txn_id,
                  }, function (data) {
                    OB.UTIL.showLoading(false);
    
                    if (data && !data.exception && data.eResponseCode === '0') {
                      lineCount++;
                      callAPIOrder(args,lines,lineCount,callbacks);
                    } else if (data.exception) {
                      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), data.exception.status.errorMessage);
                      lineCount++;
                      callAPIOrder(args,lines,lineCount,callbacks);
                    }
    
                  }, function (error) {
                    pos_date = new Date();
                    var is_cancel_success = false;
                    OB.UTIL.EPAY.call_cancelAPI_SubscribePINPRINTCancel(args, txn_id, pos_date, selLine, function () {
                      if (args.is_cancel_api_success) {
                        lineCount++;
                        callAPIOrder(args,lines,lineCount,callbacks);
                      } else {
                        var is_cancel_success = false;
                        OB.UTIL.EPAY.call_cancelAPI_SubscribePINPRINTCancel(args, txn_id, pos_date, selLine, function () {
                          if (args.is_cancel_api_success) {
                            lineCount++;
                            callAPIOrder(args,lines,lineCount,callbacks);
                          } else {
                            var is_cancel_success = false;
                            OB.UTIL.EPAY.call_cancelAPI_SubscribePINPRINTCancel(args, txn_id, pos_date, selLine, function () {
                              if (args.is_cancel_api_success) {
                                lineCount++;
                                callAPIOrder(args,lines,lineCount,callbacks);
                              } else {
                                lineCount++;
                                callAPIOrder(args,lines,lineCount,callbacks);
                              }
                            });
                          }
                        });
                      }
                    });
                 });
                }
            }if(prod.get('digiProductType') === 'SA') {  
              var processSubscribeActivationCancel = new OB.DS.Process('com.promantia.sharaf.epay.process.SubscribeActivationCancellation');
              OB.UTIL.showLoading(true);
              if (!OB.MobileApp.model.get('connectedToERP')) {
                var query = "insert into CPSE_DigitalProducts(id, upc, txnRef, amount, currency, salesRep, docNo, prodId, isPinCancel, digitalProductType, date, lineNo, txnId, esdResponse, isDeactivation, serialNo, organization, pos)" 
                          + "values('" + OB.UTIL.get_UUID() + "',"
                          + "'" + prod.get('uPCEAN') + "',"
                          + "'" + prod.get('epayResponse').TXID + "',"
                          + "'" + selLine.get('price') + "',"
                          + "'" + OB.MobileApp.model.get('currency').cpseEpayisocode + "',"
                          + "'" + args.order.get('salesRepresentative') + "',"
                          + "'" + args.order.get('documentNo') + "',"
                          + "'" + prod.get('id') + "',"
                          + "'" + false + "',"
                          + "'" + prod.get('digiProductType') + "',"
                          + "'" + moment(pos_date).format('YYYY-MM-DD HH:mm:ss') + "',"
                          + "'" + selLine.get('id') + "',"
                          + "'" + selLine.get('id') + '_' + Math.floor((Math.random() * 1000000)) + "',"
                          + "'" + JSON.stringify(prod.get('epayResponse')) + "',"
                          + "'" + true + "',"
                          + "'" + prod.get('digiSerialNo') + "',"
                          + "'" + OB.MobileApp.model.attributes.terminal.organization + "',"
                          + "'" + OB.MobileApp.model.attributes.terminal.id + "'"
                          +");";
            
                OB.Dal.queryUsingCache(OB.Model.CPSE_DigitalProducts, query, [], function (success) {
                    OB.UTIL.showLoading(false);
                    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                });
              } else {
                  var pos_date = new Date();
                  var txn_id = line.get('id') + '_' + Math.floor((Math.random() * 1000000));
                  processSubscribeActivationCancel.exec({
                    upc: prod.get('uPCEAN'),
                    txnRef: prod.get('epayResponse').TXID,
                    amount: line.get('price'),
                    serialNo: prod.get('digiSerialNo'),
                    currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
                    salesRep: args.receipt.get('salesRepresentative'),
                    docNo: args.receipt.get('documentNo'),
                    prodId: prod.get('id'),
                    isSubscribeActivationCancel: true,
                    digitalProductType: prod.get('digiProductType'),
                    date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
                    lineNo: line.get('id'),
                    txnId: txn_id,                    
                  }, function (data) {
                    OB.UTIL.showLoading(false);
    
                    if (data && !data.exception && data.eResponseCode === '0') {
                      lineCount++;
                      callAPIOrder(args,lines,lineCount,callbacks);
                    } else if (data.exception) {
                      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), data.exception.status.errorMessage);
                      lineCount++;
                      callAPIOrder(args,lines,lineCount,callbacks);
                    }
    
                  }, function (error) {
                    pos_date = new Date();
                    var is_cancel_success = false;
                    OB.UTIL.EPAY.call_cancelAPI_SubscribeActivationCancel(args, txn_id, pos_date, selLine, function () {
                      if (args.is_cancel_api_success) {
                        lineCount++;
                        callAPIOrder(args,lines,lineCount,callbacks);
                      } else {
                        var is_cancel_success = false;
                        OB.UTIL.EPAY.call_cancelAPI_SubscribeActivationCancel(args, txn_id, pos_date, selLine, function () {
                          if (args.is_cancel_api_success) {
                            lineCount++;
                            callAPIOrder(args,lines,lineCount,callbacks);
                          } else {
                            var is_cancel_success = false;
                            OB.UTIL.EPAY.call_cancelAPI_SubscribeActivationCancel(args, txn_id, pos_date, selLine, function () {
                              if (args.is_cancel_api_success) {
                                lineCount++;
                                callAPIOrder(args,lines,lineCount,callbacks);
                              } else {
                                lineCount++;
                                callAPIOrder(args,lines,lineCount,callbacks);
                              }
                            });
                          }
                        });
                      }
                    });
                  });
                }
               }
            } else {
              args.cancelOperation = true;
              OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "Digital products (ESD/EPay) cannot be deleted.");
            }
          } else {
            //deleting non-digital products... do nothing.
            lineCount++;
            callAPIOrder(args,lines,lineCount,callbacks);
          }
        } else {
             OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        }
       }