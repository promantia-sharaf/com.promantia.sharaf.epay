/*global OB, enyo, _ */
(function () {
  OB.UTIL.HookManager.registerHook('OBPOS_PreDeleteLine', function (args, callbacks) {

    var error = false;
    var lineCount = 0;
    var deletePromotionUtils = {

      mainProductLine: [],

      // Determine if all products for a promotion are selected to delete
      promotionHaveAllProductIncluded: function (lineId) {
        var result = true,
           promoLines = _.filter(args.order.get('lines').models, function (line) {
             flg = true;  
              if(OB.UTIL.isNullOrUndefined(line.get('isVerifiedReturn')) && !line.get('isVerifiedReturn')) {
                flg = line.get('IsbuyXGetYpercen') !== 'G' ? true :false;
              }   
            return flg && line.get('custdisOrderline') === lineId || line.id === lineId;
          });
        _.each(promoLines, function (line) {
          var selectedLine = _.find(args.selectedLines, function (l) {
            return line.id === l.id;
          });
          if (!selectedLine) {
            result = false;
          }
        });
        return result;
      },

      // Add main product line
      addMainProduct: function (lineId) {
        var line = _.find(this.mainProductLine, function (l) {
          return l === lineId;
        });
        if (!line) {
          this.mainProductLine.push(lineId);
        }
      }

    };

    // Find main promotion products
    var deleteGross = 0;
    var deleteLines = [];
    _.each(args.selectedLines, function (line) {
    if(OB.UTIL.isNullOrUndefined(line.get('custdisOffer')) && line.get('IsbuyXGetYpercen')=== 'P' && !line.get('isVerifiedReturn')) {
        promoarr = _.filter(args.order.get('lines').models, function (l) {
            return l.get('custdisOrderline') === line.id ;
          });
        if(promoarr.length > 0) {
           promoarr[0].unset('custdisOffer');
           promoarr[0].unset('promotions');
           promoarr[0].unset('custdisAmount');
           promoarr[0].unset('custdisOrderline');
           promoarr[0].unset('IsbuyXGetYpercen');
        }      
        
      }else if(line.get('custdisOffer') && !OB.UTIL.isNullOrUndefined(line.get('IsbuyXGetYpercen')) &&
              line.get('IsbuyXGetYpercen')=== 'G' && line.get('promotions')[0].discountType === '9AC09641F86C46C4AF7BC8630325B26C' && !line.get('isVerifiedReturn')) {
        promoarr = _.filter(args.order.get('lines').models, function (l) {
            return line.get('custdisOrderline') === l.id ;
          });
        if(promoarr.length > 0) {
            promoarr[0].unset('IsbuyXGetYpercen');
            line.unset('custdisOrderline');
        }    
        
      }else if(line.get('custdisOffer')) {
        deletePromotionUtils.addMainProduct(line.get('custdisOrderline') ? line.get('custdisOrderline') : line.id);
        deleteGross = deleteGross + line.get('discountedGross');
      } else {
        deleteGross = deleteGross + line.get('discountedGross');
      }
      deleteLines.push(line);
    });

    // Verify if a complete promotion is deleted
    _.each(deletePromotionUtils.mainProductLine, function (line) {
      if (!deletePromotionUtils.promotionHaveAllProductIncluded(line)) {
        error = true;
        args.cancelOperation = true;
        enyo.$.scrim.hide();
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        var info = [],
            promoLines = _.filter(args.order.get('lines').models, function (l) {
            return l.get('custdisOrderline') === line || l.get('originalOrderLineId') === line || l.id === line;
          });
          
          ln = _.filter(args.order.get('lines').models, function (l) {
            return l.id === line;
          });
          if(OB.UTIL.isNullOrUndefined(ln[0].get('isVerifiedReturn')) || !ln[0].get('isVerifiedReturn')) {
                 promoLines = _.filter(promoLines, function (line) {
                return line.get('IsbuyXGetYpercen') !== 'G';
              });
          }
          
        _.each(promoLines, function (l) {
          if (info.length === 0) {
            var promo = _.find(l.get('promotions'), function (promo) {
              return promo.ruleId === l.get('custdisOffer');
            });
            info.push(OB.I18N.getLabel('CUSTDIS_ErrPromotionRemoveLines', [promo ? promo.name : '']));
          }
          info.push('- ' + l.get('product').get('_identifier') + ' (' + OB.I18N.formatCurrency(l.get('gross')) + ')');
        });
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), info);
      }
    });

    if (!error) {
         _.each(args.selectedLines, function (line) {
         promoarr = _.filter(args.order.get('lines').models, function (l) {
            return l.get('custdisOrderline') === line.id 
              && l.get('IsbuyXGetYpercen') === 'G' 
              && !l.get('isVerifiedReturn');
          });
       
         if(promoarr.length === 1) {
             promoarr[0].unset('custdisOffer');
             promoarr[0].unset('promotions');
             promoarr[0].unset('custdisAmount');
             promoarr[0].unset('custdisOrderline');
             promoarr[0].unset('IsbuyXGetYpercen');
         }
        });  
      if (args.order.get('custsdtDocumenttypeSearchKey') !== 'BS') {

        //Logic before deleting a line to check if order is verified return order
        if (!OB.UTIL.isNullOrUndefined(args.order.get('isVerifiedReturn'))) {
          var errorArticles = [],
              epayAPIError = false,
              lines = args.order.get('lines'),
              epayApiReponseNullArticles = [],
              epayAPIResponseNull = false;
          //set line number for each line
          for (var i = 0; i < lines.length; i++) {
            lines.models[i].set('lineNumber', i + 1);
          }

          args.selectedLines.forEach(function (selLine) {
            if (!OB.UTIL.isNullOrUndefined(selLine.get('product').get('epayResponse')) && OB.UTIL.isNullOrUndefined(selLine.get('originalDocumentNo')) && OB.UTIL.isNullOrUndefined(selLine.get('originalOrderLineId'))) {
              if (selLine.get('product').get('epayResponse').TYPE != 'PINPRINTING' && selLine.get('product').get('epayResponse').TYPE != 'ACTIVATE' && selLine.get('product').get('epayResponse').TYPE != 'SALE') {
                if (!OB.UTIL.isNullOrUndefined(selLine.get('product').get('invoice_type')) && selLine.get('product').get('invoice_type') !== 'Re Invoice' && !OB.UTIL.isNullOrUndefined(selLine.get('isVerifiedReturn')) && selLine.get('isVerifiedReturn')) {
                  if (selLine.get('product').get('custqcDigiApiResponse') === true) {
                    errorArticles.push(selLine.get('product').get('description') + ' in line no ' + selLine.get('lineNumber') + ' ');
                    epayAPIError = true;
                  }

                  //If API is called and response is not collected at the POS... do not delete the articles from the ticket
                  if (!OB.UTIL.isNullOrUndefined(args.order.get('paymentButtonClicked')) && OB.UTIL.isNullOrUndefined(selLine.get('product').get('custqcDigiApiResponse'))) {
                    epayAPIResponseNull = true;
                    epayApiReponseNullArticles.push(selLine.get('product').get('description') + ' in line no ' + selLine.get('lineNumber') + ' ');
                  }
                }
              }
            }
          });
          if (epayAPIError) {
            OB.UTIL.showConfirmation.display('Cannot remove article ' + [errorArticles] + ' as it is de-activated');
            args.cancelOperation = true;
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
            return;
          }

          if (epayAPIResponseNull) {
            OB.UTIL.showConfirmation.display('Cannot remove article ' + [epayApiReponseNullArticles] + '. Please proceed for payment');
            args.cancelOperation = true;
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
            return;
          }
        }

        var me = this;
        if (args.order.get('isQuotation') || args.cancelOperation) {
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
          return;
        }
     callAPI(args, args.selectedLines, lineCount, callbacks);

      } else {
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      }
    } else {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
  });
}());
     var callAPI = function (args,selectedLines,lineCount,callbacks) {
        if (lineCount < selectedLines.length) {
          var selLine = selectedLines[lineCount];
          if (selLine.get('product').has('epayResponse')) {
            var prod = selLine.get('product');

            //cancellation flow
            if (prod.get('epayResponse').TYPE === 'PINPRINTING') {
              var processEPAYPINCancel = new OB.DS.Process('com.promantia.sharaf.epay.process.PINPrintCancellation');
              OB.UTIL.showLoading(true);
              if (!OB.MobileApp.model.get('connectedToERP')) {
                var query = "insert into CPSE_DigitalProducts(id, upc, txnRef, amount, currency, salesRep, docNo, prodId, isPinCancel, digitalProductType, date, lineNo, txnId, esdResponse, isDeactivation, serialNo, organization, pos)" 
                          + "values('" + OB.UTIL.get_UUID() + "',"
                          + "'" + prod.get('uPCEAN') + "',"
                          + "'" + prod.get('epayResponse').TXID + "',"
                          + "'" + selLine.get('price') + "',"
                          + "'" + OB.MobileApp.model.get('currency').cpseEpayisocode + "',"
                          + "'" + args.order.get('salesRepresentative') + "',"
                          + "'" + args.order.get('documentNo') + "',"
                          + "'" + prod.get('id') + "',"
                          + "'" + true + "',"
                          + "'" + prod.get('digiProductType') + "',"
                          + "'" + moment(pos_date).format('YYYY-MM-DD HH:mm:ss') + "',"
                          + "'" + selLine.get('id') + "',"
                          + "'" + selLine.get('id') + '_' + Math.floor((Math.random() * 1000000)) + "',"
                          + "'" + JSON.stringify(prod.get('epayResponse')) + "',"
                          + "'" + false + "',"
                          + "'" + prod.get('digiSerialNo') + "',"
                          + "'" + OB.MobileApp.model.attributes.terminal.organization + "',"
                          + "'" + OB.MobileApp.model.attributes.terminal.id + "'"
                          +");";
            
                OB.Dal.queryUsingCache(OB.Model.CPSE_DigitalProducts, query, [], function (success) {
                    OB.UTIL.showLoading(false);
                    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                });
              } else {
                  var pos_date = new Date();
                  var txn_id = selLine.get('id') + '_' + Math.floor((Math.random() * 1000000));
                  var start_date = new Date();
                  processEPAYPINCancel.exec({
                    upc: prod.get('uPCEAN'),
                    txnRef: prod.get('epayResponse').TXID,
                    amount: selLine.get('price'),
                    currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
                    salesRep: args.order.get('salesRepresentative'),
                    docNo: args.order.get('documentNo'),
                    prodId: prod.get('id'),
                    isPinCancel: true,
                    digitalProductType: prod.get('digiProductType'),
                    date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
                    lineNo: selLine.get('id'),
                    txnId: txn_id,
                    esdResponse: prod.get('epayResponse')
                  }, function (data) {
                    OB.UTIL.showLoading(false);
    
                    if (data && !data.exception && data.eResponseCode === '0') {
                      lineCount++;
                      callAPI(args, args.selectedLines, lineCount, callbacks);
                    } else if (data.exception) {
                      lineCount++;
                      callAPI(args, args.selectedLines, lineCount, callbacks);
                      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), data.exception.status.errorMessage);
                    }
                    
                  }, function (error) {
                    pos_date = new Date();
                    var is_cancel_success = false;
                    OB.UTIL.EPAY.call_cancelAPI(args, txn_id, pos_date, selLine, function () {
                      if (args.is_cancel_api_success) {
                      lineCount++;
                      callAPI(args, args.selectedLines, lineCount, callbacks);                        
                      } else {
                        var is_cancel_success = false;
                        OB.UTIL.EPAY.call_cancelAPI(args, txn_id, pos_date, selLine, function () {
                          if (args.is_cancel_api_success) {
                            lineCount++;
                            callAPI(args, args.selectedLines, lineCount, callbacks);                               
                          } else {
                            var is_cancel_success = false;
                            OB.UTIL.EPAY.call_cancelAPI(args, txn_id, pos_date, selLine, function () {
                              if (args.is_cancel_api_success) {
                                lineCount++;
                                callAPI(args, args.selectedLines, lineCount, callbacks);
                              } else {
                                lineCount++;
                                callAPI(args, args.selectedLines, lineCount, callbacks);                                
                              }
                            });
                          }
                        });
                      }
                    });
                 });
              }
              //Deactivation flow
            } else if (prod.get('epayResponse').TYPE === 'ACTIVATE') {
              var processPOSDeactivate = new OB.DS.Process('com.promantia.sharaf.epay.process.POSDeactivation');
              OB.UTIL.showLoading(true);
              if (!OB.MobileApp.model.get('connectedToERP')) {
                var query = "insert into CPSE_DigitalProducts(id, upc, txnRef, amount, currency, salesRep, docNo, prodId, isPinCancel, digitalProductType, date, lineNo, txnId, esdResponse, isDeactivation, serialNo, organization, pos)" 
                          + "values('" + OB.UTIL.get_UUID() + "',"
                          + "'" + prod.get('uPCEAN') + "',"
                          + "'" + prod.get('epayResponse').TXID + "',"
                          + "'" + selLine.get('price') + "',"
                          + "'" + OB.MobileApp.model.get('currency').cpseEpayisocode + "',"
                          + "'" + args.order.get('salesRepresentative') + "',"
                          + "'" + args.order.get('documentNo') + "',"
                          + "'" + prod.get('id') + "',"
                          + "'" + false + "',"
                          + "'" + prod.get('digiProductType') + "',"
                          + "'" + moment(pos_date).format('YYYY-MM-DD HH:mm:ss') + "',"
                          + "'" + selLine.get('id') + "',"
                          + "'" + selLine.get('id') + '_' + Math.floor((Math.random() * 1000000)) + "',"
                          + "'" + JSON.stringify(prod.get('epayResponse')) + "',"
                          + "'" + true + "',"
                          + "'" + prod.get('digiSerialNo') + "',"
                          + "'" + OB.MobileApp.model.attributes.terminal.organization + "',"
                          + "'" + OB.MobileApp.model.attributes.terminal.id + "'"
                          +");";
            
                OB.Dal.queryUsingCache(OB.Model.CPSE_DigitalProducts, query, [], function (success) {
                    OB.UTIL.showLoading(false);
                    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                });
              } else {
                  var pos_date = new Date();
                  var txn_id = selLine.get('id') + '_' + Math.floor((Math.random() * 1000000));
                  processPOSDeactivate.exec({
                    upc: prod.get('uPCEAN'),
                    txnRef: prod.get('epayResponse').TXID,
                    amount: selLine.get('price'),
                    serialNo: prod.get('digiSerialNo'),
                    currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
                    salesRep: args.order.get('salesRepresentative'),
                    docNo: args.order.get('documentNo'),
                    prodId: prod.get('id'),
                    isDeactivation: true,
                    date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
                    lineNo: selLine.get('id'),
                    txnId: txn_id
                  }, function (data) {
                    OB.UTIL.showLoading(false);
    
                    if (data && !data.exception && data.eResponseCode === '0') {
                      lineCount++;
                      callAPI(args, args.selectedLines, lineCount, callbacks);
                    } else if (data.exception) {
                      lineCount++;
                      callAPI(args, args.selectedLines, lineCount, callbacks);
                      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), data.exception.status.errorMessage);
                    }
    
                  }, function (error) {
                    pos_date = new Date();
                    var is_cancel_success = false;
                    OB.UTIL.EPAY.call_cancelAPI_Deactivation(args, txn_id, pos_date, selLine, function () {
                      if (args.is_cancel_api_success) {
                        lineCount++;
                        callAPI(args, args.selectedLines, lineCount, callbacks);
                      } else {
                        var is_cancel_success = false;
                        OB.UTIL.EPAY.call_cancelAPI_Deactivation(args, txn_id, pos_date, selLine, function () {
                          if (args.is_cancel_api_success) {
                            lineCount++;
                            callAPI(args, args.selectedLines, lineCount, callbacks);                           
                          } else {
                            var is_cancel_success = false;
                            OB.UTIL.EPAY.call_cancelAPI_Deactivation(args, txn_id, pos_date, selLine, function () {
                              if (args.is_cancel_api_success) {
                                lineCount++;
                                callAPI(args, args.selectedLines, lineCount, callbacks);
                              } else {
                                lineCount++;
                                callAPI(args, args.selectedLines, lineCount, callbacks);
                              }
                            });
                          }
                        });
                      }
                    });
                  });
              }
            } else if (prod.get('epayResponse').TYPE === 'SALE') {
              if(prod.get('digiProductType') === 'SP') {  
              var processSubscribePINPRINTCancel = new OB.DS.Process('com.promantia.sharaf.epay.process.SubscribePINPRINTCancellation');
              OB.UTIL.showLoading(true);
              if (!OB.MobileApp.model.get('connectedToERP')) {
                var query = "insert into CPSE_DigitalProducts(id, upc, txnRef, amount, currency, salesRep, docNo, prodId, isPinCancel, digitalProductType, date, lineNo, txnId, esdResponse, isDeactivation, serialNo, organization, pos)" 
                          + "values('" + OB.UTIL.get_UUID() + "',"
                          + "'" + prod.get('uPCEAN') + "',"
                          + "'" + prod.get('epayResponse').TXID + "',"
                          + "'" + selLine.get('price') + "',"
                          + "'" + OB.MobileApp.model.get('currency').cpseEpayisocode + "',"
                          + "'" + args.order.get('salesRepresentative') + "',"
                          + "'" + args.order.get('documentNo') + "',"
                          + "'" + prod.get('id') + "',"
                          + "'" + true + "',"
                          + "'" + prod.get('digiProductType') + "',"
                          + "'" + moment(pos_date).format('YYYY-MM-DD HH:mm:ss') + "',"
                          + "'" + selLine.get('id') + "',"
                          + "'" + selLine.get('id') + '_' + Math.floor((Math.random() * 1000000)) + "',"
                          + "'" + JSON.stringify(prod.get('epayResponse')) + "',"
                          + "'" + false + "',"
                          + "'" + prod.get('digiSerialNo') + "',"
                          + "'" + OB.MobileApp.model.attributes.terminal.organization + "',"
                          + "'" + OB.MobileApp.model.attributes.terminal.id + "'"
                          +");";
            
                OB.Dal.queryUsingCache(OB.Model.CPSE_DigitalProducts, query, [], function (success) {
                    OB.UTIL.showLoading(false);
                    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                });
              } else {
                  var pos_date = new Date();
                  var txn_id = selLine.get('id') + '_' + Math.floor((Math.random() * 1000000));
                  var start_date = new Date();
                  processSubscribePINPRINTCancel.exec({
                    upc: prod.get('uPCEAN'),
                    txnRef: prod.get('epayResponse').TXID,
                    amount: selLine.get('price'),
                    currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
                    salesRep: args.order.get('salesRepresentative'),
                    docNo: args.order.get('documentNo'),
                    prodId: prod.get('id'),
                    isSubscribePINPRINTCancel: true,
                    digitalProductType: prod.get('digiProductType'),
                    date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
                    lineNo: selLine.get('id'),
                    txnId: txn_id,
                  }, function (data) {
                    OB.UTIL.showLoading(false);
    
                    if (data && !data.exception && data.eResponseCode === '0') {
                      lineCount++;
                      callAPI(args, args.selectedLines, lineCount, callbacks);
                      //OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                    } else if (data.exception) {
                      lineCount++;
                      callAPI(args, args.selectedLines, lineCount, callbacks);
                      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), data.exception.status.errorMessage);
                    }
    
                  }, function (error) {
                    pos_date = new Date();
                    var is_cancel_success = false;
                    OB.UTIL.EPAY.call_cancelAPI_SubscribePINPRINTCancel(args, txn_id, pos_date, selLine, function () {
                      if (args.is_cancel_api_success) {
                        lineCount++;
                        callAPI(args, args.selectedLines, lineCount, callbacks);
                      } else {
                        var is_cancel_success = false;
                        OB.UTIL.EPAY.call_cancelAPI_SubscribePINPRINTCancel(args, txn_id, pos_date, selLine, function () {
                          if (args.is_cancel_api_success) {
                            lineCount++;
                            callAPI(args, args.selectedLines, lineCount, callbacks);
                          } else {
                            var is_cancel_success = false;
                            OB.UTIL.EPAY.call_cancelAPI_SubscribePINPRINTCancel(args, txn_id, pos_date, selLine, function () {
                              if (args.is_cancel_api_success) {
                                lineCount++;
                                callAPI(args, args.selectedLines, lineCount, callbacks);
                              } else {
                                lineCount++;
                                callAPI(args, args.selectedLines, lineCount, callbacks);
                              }
                            });
                          }
                        });
                      }
                    });
                 });
              }
            }if(prod.get('digiProductType') === 'SA') {  
              var processSubscribeActivationCancel = new OB.DS.Process('com.promantia.sharaf.epay.process.SubscribeActivationCancellation');
              OB.UTIL.showLoading(true);
              if (!OB.MobileApp.model.get('connectedToERP')) {
                var query = "insert into CPSE_DigitalProducts(id, upc, txnRef, amount, currency, salesRep, docNo, prodId, isPinCancel, digitalProductType, date, lineNo, txnId, esdResponse, isDeactivation, serialNo, organization, pos)" 
                          + "values('" + OB.UTIL.get_UUID() + "',"
                          + "'" + prod.get('uPCEAN') + "',"
                          + "'" + prod.get('epayResponse').TXID + "',"
                          + "'" + selLine.get('price') + "',"
                          + "'" + OB.MobileApp.model.get('currency').cpseEpayisocode + "',"
                          + "'" + args.order.get('salesRepresentative') + "',"
                          + "'" + args.order.get('documentNo') + "',"
                          + "'" + prod.get('id') + "',"
                          + "'" + false + "',"
                          + "'" + prod.get('digiProductType') + "',"
                          + "'" + moment(pos_date).format('YYYY-MM-DD HH:mm:ss') + "',"
                          + "'" + selLine.get('id') + "',"
                          + "'" + selLine.get('id') + '_' + Math.floor((Math.random() * 1000000)) + "',"
                          + "'" + JSON.stringify(prod.get('epayResponse')) + "',"
                          + "'" + true + "',"
                          + "'" + prod.get('digiSerialNo') + "',"
                          + "'" + OB.MobileApp.model.attributes.terminal.organization + "',"
                          + "'" + OB.MobileApp.model.attributes.terminal.id + "'"
                          +");";
            
                OB.Dal.queryUsingCache(OB.Model.CPSE_DigitalProducts, query, [], function (success) {
                    OB.UTIL.showLoading(false);
                    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                });
              } else {
                  var pos_date = new Date();
                  var txn_id = selLine.get('id') + '_' + Math.floor((Math.random() * 1000000));
                  processSubscribeActivationCancel.exec({
                    upc: prod.get('uPCEAN'),
                    txnRef: prod.get('epayResponse').TXID,
                    amount: selLine.get('price'),
                    serialNo: prod.get('digiSerialNo'),
                    currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
                    salesRep: args.order.get('salesRepresentative'),
                    docNo: args.order.get('documentNo'),
                    prodId: prod.get('id'),
                    isSubscribeActivationCancel: true,
                    digitalProductType: prod.get('digiProductType'),
                    date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
                    lineNo: selLine.get('id'),
                    txnId: txn_id
                  }, function (data) {
                    OB.UTIL.showLoading(false);
    
                    if (data && !data.exception && data.eResponseCode === '0') {
                      lineCount++;
                      callAPI(args, args.selectedLines, lineCount, callbacks);
                    } else if (data.exception) {
                      lineCount++;
                      callAPI(args, args.selectedLines, lineCount, callbacks);
                      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), data.exception.status.errorMessage);
                    }
    
                  }, function (error) {
                    pos_date = new Date();
                    var is_cancel_success = false;
                    OB.UTIL.EPAY.call_cancelAPI_SubscribeActivationCancel(args, txn_id, pos_date, selLine, function () {
                      if (args.is_cancel_api_success) {
                        lineCount++;
                        callAPI(args, args.selectedLines, lineCount, callbacks);
                      } else {
                        var is_cancel_success = false;
                        OB.UTIL.EPAY.call_cancelAPI_SubscribeActivationCancel(args, txn_id, pos_date, selLine, function () {
                          if (args.is_cancel_api_success) {
                            lineCount++;
                            callAPI(args, args.selectedLines, lineCount, callbacks);
                          } else {
                            var is_cancel_success = false;
                            OB.UTIL.EPAY.call_cancelAPI_SubscribeActivationCancel(args, txn_id, pos_date, selLine, function () {
                              if (args.is_cancel_api_success) {
                                lineCount++;
                                callAPI(args, args.selectedLines, lineCount, callbacks);
                              } else {
                                lineCount++;
                                callAPI(args, args.selectedLines, lineCount, callbacks);
                              }
                            });
                          }
                        });
                      }
                    });
                  });
                }
              }
            } else {
              args.cancelOperation = true;
              OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "Digital products (ESD/EPay) cannot be deleted.");
            }
          } else {
            //deleting non-digital products... do nothing.
             lineCount++;
             callAPI(args, args.selectedLines, lineCount, callbacks);
          }
        
        } else {
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        }
       };
OB.UTIL.EPAY.call_cancelAPI = function (args, txn_id, pos_date, selLine, callback) {
  var processEPAYPINCancel = new OB.DS.Process('com.promantia.sharaf.epay.process.PINPrintCancellation');
  OB.UTIL.showLoading(true);
  var is_api_cancel_success = false;
  var prod = selLine.get('product');
  processEPAYPINCancel.exec({
    upc: prod.get('uPCEAN'),
    txnRef: prod.get('epayResponse').TXID,
    amount: prod.get('standardPrice'),
    serialNo: prod.get('digiSerialNo'),
    currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
    salesRep: args.order.get('salesRepresentative'),
    docNo: args.order.get('documentNo'),
    prodId: prod.get('id'),
    digitalProductType: prod.get('digiProductType'),
    date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
    lineNo: selLine.get('id'),
    isPinCancel: true,
    txnId: txn_id,
    esdResponse: prod.get('epayResponse')
  }, function (data) {
    OB.UTIL.showLoading(false);

    if (data && !data.exception && data.eResponseCode === '0') {
      args.is_cancel_api_success = true;
      callback();
    } else if (data.exception) {
      args.is_cancel_api_success = false;
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), 'Time Out');
      callback();
    }

  }, function (error) {
    OB.UTIL.showLoading(false);
    args.is_cancel_api_success = false;
    OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), error.exception.message);
    callback();
  });
};


OB.UTIL.EPAY.call_cancelAPI_Deactivation = function (args, txn_id, pos_date, selLine, callback) {
  var processEPAYPOSDeactivation = new OB.DS.Process('com.promantia.sharaf.epay.process.POSDeactivation');
  OB.UTIL.showLoading(true);
  var is_api_cancel_success = false;
  var prod = selLine.get('product');
  processEPAYPOSDeactivation.exec({
    upc: prod.get('uPCEAN'),
    txnRef: prod.get('epayResponse').TXID,
    amount: prod.get('standardPrice'),
    serialNo: prod.get('digiSerialNo'),
    currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
    salesRep: args.order.get('salesRepresentative'),
    docNo: args.order.get('documentNo'),
    prodId: prod.get('id'),
    date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
    lineNo: selLine.get('id'),
    isDeactivation: true,
    txnId: txn_id
  }, function (data) {
    OB.UTIL.showLoading(false);

    if (data && !data.exception && data.eResponseCode === '0') {
      args.is_cancel_api_success = true;
      callback();
    } else if (data.exception) {
      args.is_cancel_api_success = false;
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), 'Time Out');
      callback();
    }

  }, function (error) {
    OB.UTIL.showLoading(false);
    args.is_cancel_api_success = false;
    OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), error.exception.message);
    callback();
  });
};

OB.UTIL.EPAY.call_cancelAPI_SubscribeActivationCancel = function (args, txn_id, pos_date, order, line, epayResponse, callback) {
  var processSubscribeActivationCancel = new OB.DS.Process('com.promantia.sharaf.epay.process.SubscribeActivationCancellation');
  OB.UTIL.showLoading(true);
  var is_api_cancel_success = false;
  var prod = selLine.get('product');
  processSubscribeActivationCancel.exec({
    upc: prod.get('uPCEAN'),
    txnRef: prod.get('epayResponse').TXID,
    amount: prod.get('standardPrice'),
    serialNo: prod.get('digiSerialNo'),
    currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
    salesRep: args.order.get('salesRepresentative'),
    docNo: args.order.get('documentNo'),
    prodId: prod.get('id'),
    date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
    lineNo: selLine.get('id'),
    isSubscribeActivationCancel: true,
    txnId: txn_id
  }, function (data) {
    OB.UTIL.showLoading(false);
    if (data && !data.exception && data.eResponseCode === '0') {
      args.is_cancel_api_success = true;
      callback();
    } else if (data.exception) {
      args.is_cancel_api_success = false;
      //OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), 'Time Out');
      callback();
    }

  }, function (error) {
    args.is_cancel_api_success = false;
    args.recursiveError = error.exception.message;
    callback();
  });
};

OB.UTIL.EPAY.call_cancelAPI_SubscribePINPRINTCancel = function (args, txn_id, pos_date, order, line, epayResponse, callback) {
  var processSubscribePINPRINTCancel = new OB.DS.Process('com.promantia.sharaf.epay.process.SubscribePINPRINTCancellation');
  OB.UTIL.showLoading(true);
  var is_api_cancel_success = false;
  var prod = selLine.get('product');
  processSubscribePINPRINTCancel.exec({
    upc: prod.get('uPCEAN'),
    txnRef: prod.get('epayResponse').TXID,
    amount: prod.get('standardPrice'),
    currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
    salesRep: args.order.get('salesRepresentative'),
    docNo: args.order.get('documentNo'),
    prodId: prod.get('id'),
    digitalProductType: prod.get('digiProductType'),
    date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
    lineNo: selLine.get('id'),
    isSubscribePINPRINTCancel: true,
    txnId: txn_id,
  }, function (data) {
    OB.UTIL.showLoading(false);
    if (data && !data.exception && data.eResponseCode === '0') {
      args.is_cancel_api_success = true;
      callback();
    } else if (data.exception) {
      args.is_cancel_api_success = false;
      //OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), 'Time Out');
      callback();
    }

  }, function (error) {
    args.is_cancel_api_success = false;
    args.recursiveError = error.exception.message;
    callback();
  });
};