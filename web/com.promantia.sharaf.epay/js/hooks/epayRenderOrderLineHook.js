/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone, _ */

OB.UTIL.HookManager.registerHook('OBPOS_RenderOrderLine', function (args, callbacks) {

  var retDocumentNo;
  if (OB.MobileApp.model.receipt.get('lines').models.length > 0) {
    for (var i = 0; i < OB.MobileApp.model.receipt.get('lines').models.length; i++) {
      if (!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.receipt.get('lines').models[i].get('returnLineId'))) {
        retDocumentNo = OB.MobileApp.model.receipt.get('lines').models[i].get('retDocumentNo');
        if (OB.UTIL.isNullOrUndefined(retDocumentNo)) {
            retDocumentNo = OB.MobileApp.model.receipt.get('retDocumentNo');
        }
      } else if (!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.receipt.get('retDocumentNo'))) {
        retDocumentNo = OB.MobileApp.model.receipt.get('retDocumentNo');
      }
      if (!OB.UTIL.isNullOrUndefined(retDocumentNo)) {
        break;
      }
    }
  }

  if (!OB.UTIL.isNullOrUndefined(retDocumentNo)) {
    args.orderline.createComponent({
      style: 'float: left; width: 80%; display: block;',
      components: [{
        content: '-- ' + OB.I18N.getLabel('CPSE_LblReturnDocument') + ': ' + retDocumentNo,
        attributes: {
          style: 'clear: left;'
        }
      }]
    });
  }

  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
});