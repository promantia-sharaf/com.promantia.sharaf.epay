/*global OB, enyo, _ */
(function () {
  OB.UTIL.EPAY = OB.UTIL.EPAY || {};
  OB.UTIL.HookManager.registerHook('OBPOS_PreAddProductToOrder', function (args, callbacks) {
    var documentSK = args.receipt.get('custsdtDocumenttypeSearchKey'),
        documentTypeId = args.receipt.get('custsdtDocumenttype'),
        prod = args.productToAdd,
        prodArr = [],
        prodCatArr = [],
        orderline, returnOrderline, promotion, promoValid = false,
        isError = false;

    OB.CUSTSDT.Utils.fetchProdAndProdCatForDocumentType(documentSK, documentTypeId, prodCatArr, prodArr, prod, function () {
      OB.CUSTSDT.Utils.checkProdAndProdCatForDocumentType(documentSK, prodCatArr, prodArr, prod, isError, function () {
        if (prod.get('isError')) {
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), 'Article ' + prod.get('_identifier') + ' is not allowed in ' + documentSK + ' document type', [{
            label: OB.I18N.getLabel('OBMOBC_LblOk'),
            isConfirmButton: true,
            action: function () {
              return;
            }
          }]);
        } else {
          if (args.receipt.get('custsdtDocumenttypeSearchKey') !== 'BS') {
            enyo.$.scrim.show();
            var pinPrintProduct, activationProduct;
            if (args.productToAdd.get('digiProductType') === 'P' || args.productToAdd.get('digiProductType') === 'E'|| args.productToAdd.get('digiProductType') === 'SP') {
              pinPrintProduct = true;
            } else if (args.productToAdd.get('digiProductType') === 'A'|| args.productToAdd.get('digiProductType') === 'SA') {
              activationProduct = true;
            }
            var me = this;
            if (args.receipt.get('isQuotation') || args.cancelOperation) {
              enyo.$.scrim.hide();
              OB.UTIL.HookManager.callbackExecutor(args, callbacks);
              return;
            }

            if (pinPrintProduct || activationProduct) {
              OB.MobileApp.view.waterfall('onShowPopup', {
                popup: 'CPSE.UI.epayInvoiceType',
                args: {
                  data: args,
                  callback: function () {
                    if (args.productToAdd.get('invoice_type') === 'New Invoice') {
                      OB.UTIL.showLoading(true);
                      OB.UTIL.EPAY.EPAYInvoiceType(args, callbacks);
                      OB.UTIL.showLoading(false);
                    } else if (args.productToAdd.get('invoice_type') === 'Re Invoice') {
                      this.params = [];
                      this.params.isQuotation = false;
                      this.params.isReturn = false;
                      this.params.isReinvoice = true;

                      CPSE.UI.ModalPaidReceiptsModified.prototype.setParams(this.params);
                      OB.EPAY_R.Reinvoice.prodToAddId = args.productToAdd.id;
                      OB.EPAY_R.Reinvoice.prodToAddDescription = args.productToAdd.attributes.description;
                      OB.EPAY_R.Reinvoice.promotion = null;
                      OB.MobileApp.view.waterfallDown('onShowPopup', {
                        popup: 'CPSE.UI.ModalPaidReceiptsModified',
                        args: {
                          data: args,
                          callback: function () {
                            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                            if (!OB.UTIL.isNullOrUndefined(args.attrs.receiptLines) && args.attrs.receiptLines.length > 1) {
                              var returnLineArr = []
                              for (var i = 0; i < args.attrs.receiptLines.length; i++) {
                                returnLineArr.push(args.attrs.receiptLines[i].lineId);
                              }
                              args.receipt.set('returnLineId', returnLineArr);
                            } else {
                              args.receipt.set('returnLineId', prod.get('returnLineId'));
                            }
                            if (!OB.UTIL.isNullOrUndefined(args.attrs.selectedLines) && args.attrs.selectedLines.length > 1) {
                              var selectedLinesArr = []
                              for (var i = 0; i < args.attrs.selectedLines.length; i++) {
                                selectedLinesArr.push(args.attrs.selectedLines[i].line);
                              }
                              args.receipt.set('selectedLines', selectedLinesArr);
                            }
                            args.receipt.set('retDocumentNo', args.attrs.retDocumentNo);
                            args.receipt.save();

                            var selectedProductId = args.attrs.returnLineId;
                            for (var i = 0; i < args.attrs.receiptLines.length; i++) {
                              if (args.attrs.receiptLines[i].lineId === selectedProductId) {
                                product = args.attrs.receiptLines[i];
                              }
                            }

                            OB.UTIL.EPAY.promotionLine(args, product, promotion, callbacks, function (orderline) {
                              if (args.productToAdd && !args.productToAdd.get('custdisAddByPromo')) {
                                var query = "select distinct O.* from m_offer_product OF join m_offer O on O.m_offer_id = OF.m_offer_id left join m_offer_bp_group OBPG on OBPG.m_offer_id = O.m_offer_id" //
                                + " where O.m_offer_type_id in ('CA5491E6000647BD889B8D7CDF680795', '6A3C2313136147A6B2D1B8D2F5F768E6','1424B47A341E4D38B0F61CCF26450AF3') " //
                                + "and OF.em_custdis_is_gift = 'false' and OF.m_product_id = '" + args.productToAdd.id //
                                + "' and ((O.bp_group_selection = 'N' and OBPG.c_bp_group_id='" + args.receipt.get('bp').get('businessPartnerCategory') + "') or (O.bp_group_selection = 'Y'))" //
                                + " and O.datefrom <= '" + moment(OB.UTIL.localStorage.getItem('businessdate')).format('YYYY-MM-DD') + "' and (O.dateto is null or O.dateto >= '" + moment(OB.UTIL.localStorage.getItem('businessdate')).format('YYYY-MM-DD') + "') order by O.name";
                                OB.Dal.queryUsingCache(OB.Model.Discount, query, [], function (offers) {
                                  if (offers.length === 0) {
                                    if (!OB.UTIL.isNullOrUndefined(product.custdisOffer)) {
                                      args.receipt.deleteLine(orderline);
                                      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), 'Applied promotion in original order is invalid');
                                    }
                                    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                                  } else if (args.qtyToAdd === 1 && OB.UTIL.isNullOrUndefined(args.productToAdd.get('custdisOffer'))) {
                                    for (var i = 0; i < offers.models.length; i++) {
                                      if (offers.models[i].get('id') === product.custdisOffer) {
                                        promoValid = true;
                                        promotion = offers.models[i];
                                        OB.UTIL.EPAY.promotionQtyLimit(promotion, args, callbacks, function () {
                                          orderline.set('custdisAddByPromo', true);
                                          if (!OB.UTIL.isNullOrUndefined(promotion)) {
                                            OB.CUSTDIS.Utils.findPromotionProduct(orderline, {
                                              rule: promotion
                                            }, function (product) {
                                              if (product) {
                                                OB.info('The previous promotion has the product to be added ' + product.get('_identifier'));
                                                product.set('custdisPrimaryNewPrice', promotion.get('custdisPrimaryNewprice'));
                                                product.set('custdisSecondaryNewPrice', promotion.get('custdisSecondaryNewprice'));
                                                OB.CUSTDIS.Utils.setLineDiscount(orderline, product);
                                                orderline.set('custdisAddByPromo', true);
                                                for (var k = 0; k < args.attrs.receiptLines.length; k++) {
                                                  if (!OB.UTIL.isNullOrUndefined(args.attrs.receiptLines[k].custdisOrderline)) {
                                                    if (orderline.get('returnLineId') === args.attrs.receiptLines[k].custdisOrderline) {
                                                      returnOrderline = args.attrs.receiptLines[k];
                                                      if (returnOrderline) {
                                                        if (promotion.get('discountType') === 'CA5491E6000647BD889B8D7CDF680795') {
                                                          OB.Model.Discounts.addManualPromotion(args.receipt, [orderline], {
                                                            rule: promotion,
                                                            definition: {}
                                                          });
                                                        }
                                                        if (promotion.get('discountType') === '6A3C2313136147A6B2D1B8D2F5F768E6' || promotion.get('discountType') === '1424B47A341E4D38B0F61CCF26450AF3') {
                                                          OB.info('The previous product will be added to the ticket shortly');
                                                          OB.CUSTDIS.Utils.addPromotionProducts(promotion, args.receipt, orderline);
                                                        }
                                                      }
                                                    }
                                                  } else {
                                                    if (promotion.get('discountType') === 'CA5491E6000647BD889B8D7CDF680795') {
                                                      if (orderline.get('returnLineId') === args.attrs.receiptLines[k].lineId) {
                                                        returnOrderline = args.attrs.receiptLines[k];
                                                        if (returnOrderline) {
                                                          if (promotion.get('discountType') === 'CA5491E6000647BD889B8D7CDF680795') {
                                                            OB.Model.Discounts.addManualPromotion(args.receipt, [orderline], {
                                                              rule: promotion,
                                                              definition: {}
                                                            });
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                }
                                              }
                                            });
                                          } else {
                                            OB.UTIL.EPAY.addPromotionProductForReInvoice(returnOrderline, promotion, args, callbacks);
                                          }
                                        });
                                      }
                                    }
                                    if (!promoValid) {
                                      if (orderline.get('custdisAddByPromo')) {
                                        args.receipt.deleteLine(orderline);
                                        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), 'Applied promotion in original order is invalid');
                                      } else {
                                        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                                      }
                                    }
                                  } else {
                                    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                                  }
                                }, function () {
                                  //not to show promotion popup while increasing the primary prod qty if promo is not selected first time
                                  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                                });
                              }
                            });
                          }
                        }
                      });
                    } else {
                      enyo.$.scrim.hide();
                      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                    }
                  }
                }
              });
            } else {
              enyo.$.scrim.hide();
              OB.UTIL.HookManager.callbackExecutor(args, callbacks);
            }
          } else {
            enyo.$.scrim.hide();
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
          }
        }
      });
    });
  });
}());

OB.UTIL.EPAY.promotionQtyLimit = function (promotion, args, callbacks, callbackFound) {
  var count = 0;

  function successLocal(model) {
    if (!OB.UTIL.isNullOrUndefined(promotion) && !OB.UTIL.isNullOrUndefined(promotion.get('limitcount'))) {
      promotion.unset('limitcount');
    }
    if (!OB.UTIL.isNullOrUndefined(promotion) && !OB.UTIL.isNullOrUndefined(promotion.get('totalAvailableQty'))) {
      promotion.unset('totalAvailableQty');
    }
    if (!OB.UTIL.isNullOrUndefined(promotion) && !OB.UTIL.isNullOrUndefined(promotion.get('count'))) {
      promotion.unset('count');
    }
    if (!OB.UTIL.isNullOrUndefined(model.get('custdisSellableLimit')) && model.get('custdisSellableLimit') > 0) {
      // to fetch total promo qty added in the order ---> for promo limit check
      var map = new Map();
      var ArrayOnlyCustOffer = [];
      var ArrayPriceAdjDis = [];
      var lines = OB.MobileApp.model.receipt.get('lines').models;
      var k = 0;

      for (k = 0; k < lines.length; k++) {
        if (!OB.UTIL.isNullOrUndefined(lines[k].get('promotions'))) {
          if (!OB.UTIL.isNullOrUndefined(lines[k].get('custdisOffer')) && (OB.UTIL.isNullOrUndefined(lines[k].get('custdisOrderline')) || (!OB.UTIL.isNullOrUndefined(lines[k].get('promotions')[0].discountType) && lines[k].get('promotions')[0].discountType === 'CA5491E6000647BD889B8D7CDF680795'))) {
            ArrayOnlyCustOffer.push(lines[k]); //add only primary lines and priceAdjustment discount types
          }
        }
      }

      for (i = 0; i < ArrayOnlyCustOffer.length; i++) {
        if (!OB.UTIL.isNullOrUndefined(lines[i].get('promotions'))) {
          if (map.get(ArrayOnlyCustOffer[i].get('custdisOffer')) && (OB.MobileApp.model.receipt.get('custsdtDocumenttypeSearchKey') != 'BS' || OB.MobileApp.model.receipt.get('custsdtDocumenttypeSearchKey') != 'BR')) {
            map.set(ArrayOnlyCustOffer[i].get('custdisOffer'), map.get(ArrayOnlyCustOffer[i].get('custdisOffer')) + ArrayOnlyCustOffer[i].get('qty'));
          } else {
            map.set(ArrayOnlyCustOffer[i].get('custdisOffer'), ArrayOnlyCustOffer[i].get('qty'));
          }
        }
      }

      var keys = map.keys();
      var values = map.values();
      map.forEach(function (eachMap) {
        if (keys.next().value === promotion.get('id')) {
          count = count + values.next().value;
        }
      });

      promotion.set('count', count);

      new OB.DS.Process('com.openbravo.sharaf.retail.discounts.service.PromotionLimitService').exec({
        mofferid: promotion.get('id'),
        client: OB.MobileApp.model.attributes.terminal.client
      }, function (data) {
        if (data && data.exception) {
          promotion.set('limitcount', -1);
          if (!OB.UTIL.isNullOrUndefined(data.exception.message) && data.exception.message.indexOf('Application server is not') >= 0) {
            promotion.set('limitcount', 'offline');
          } else if (!OB.UTIL.isNullOrUndefined(data.exception) && !OB.UTIL.isNullOrUndefined(data.exception.status) && !OB.UTIL.isNullOrUndefined(data.exception.status.message)) {
            promotion.set('limitcount', 'error');
            promotion.set('limitErrorMessage', data.exception.status.message);
          }
          if ((!OB.UTIL.isNullOrUndefined(promotion)) && !OB.UTIL.isNullOrUndefined(promotion.get('limitcount')) && promotion.get('limitcount') > 0) {
            callbackFound();
          } else {
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
            if (!OB.UTIL.isNullOrUndefined(promotion) && !OB.UTIL.isNullOrUndefined(promotion.get('limitcount')) && promotion.get('limitcount') === 'offline') {
              OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBPOS_MsgApplicationServerNotAvailable'));
            } else if (!OB.UTIL.isNullOrUndefined(promotion) && !OB.UTIL.isNullOrUndefined(promotion.get('limitcount')) && promotion.get('limitcount') === 'error' && !OB.UTIL.isNullOrUndefined(promotion.get('limitErrorMessage'))) {
              OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), promotion.get('limitErrorMessage'));
            } else if (!OB.UTIL.isNullOrUndefined(promotion)) {
              OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTDIS_PromotionLimit', [promotion.get('totalAvailableQty')]));
            }
          }
        } else {
          if (!OB.UTIL.isNullOrUndefined(data) && !OB.UTIL.isNullOrUndefined(data.remainingqty)) {
            if (promotion.get('count') >= data.remainingqty) {
              promotion.set('limitcount', -1);
              promotion.set('totalAvailableQty', data.remainingqty);
            } else {
              promotion.set('limitcount', data.remainingqty);
              promotion.set('totalAvailableQty', data.remainingqty);
            }
          }
          if ((!OB.UTIL.isNullOrUndefined(promotion)) && !OB.UTIL.isNullOrUndefined(promotion.get('limitcount')) && promotion.get('limitcount') > 0) {
            //     me.args.callback(promotion);
          } else {
            //     OB.UTIL.HookManager.callbackExecutor(args, callbacks);
            if (!OB.UTIL.isNullOrUndefined(promotion) && !OB.UTIL.isNullOrUndefined(promotion.get('limitcount')) && promotion.get('limitcount') === 'offline') {
              OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBPOS_MsgApplicationServerNotAvailable'));
            } else if (!OB.UTIL.isNullOrUndefined(promotion) && !OB.UTIL.isNullOrUndefined(promotion.get('limitcount')) && promotion.get('limitcount') === 'error' && !OB.UTIL.isNullOrUndefined(promotion.get('limitErrorMessage'))) {
              OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), promotion.get('limitErrorMessage'));
            } else if (!OB.UTIL.isNullOrUndefined(promotion)) {
              OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTDIS_PromotionLimit', [promotion.get('totalAvailableQty')]));
            }
          }
          enyo.$.scrim.hide();
          callbackFound();
        }
      });
    } else {
      if ((!OB.UTIL.isNullOrUndefined(promotion)) && OB.UTIL.isNullOrUndefined(promotion.get('limitcount'))) {
        callbackFound(); // To add non PQL promo products avoiding PQL check
      }
    }
  }

  function errorLocal(tx) {
    console.log(tx);
    enyo.$.scrim.hide();
  }

  if (!OB.UTIL.isNullOrUndefined(promotion)) {
    OB.Dal.get(OB.Model.Discount, promotion.get('id'), successLocal, errorLocal);
  } else {
    enyo.$.scrim.hide();
    callbackFound();
  }
};

OB.UTIL.EPAY.promotionLine = function (args, product, promotion, callbacks, callbackFound) {
  var orderline;
  if (args.receipt.get('lines').models.length > 0) {
    for (var j = args.receipt.get('lines').models.length - 1; j >= 0; j--) {
      orderline = args.receipt.get('lines').models[j];
      break;
    }
  } else {
	  OB.UTIL.EPAY.promotionLine(args, product, promotion, callbacks, callbackFound);
  }
  if (!OB.UTIL.isNullOrUndefined(orderline)) {
    callbackFound(orderline);
  }
};

OB.UTIL.EPAY.EPAYInvoiceType = function (args, callbacks) {

  //-------------PIN print flow
  if (!OB.UTIL.isNullOrUndefined(args.productToAdd)) {
    if (args.productToAdd.get('digiProductType') === 'P' || args.productToAdd.get('digiProductType') === 'E' || args.productToAdd.get('digiProductType') === 'SP') {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      //------------------------activation flow	
    }else if (args.productToAdd.get('digiProductType') === 'A' || args.productToAdd.get('digiProductType') === 'SA') {
      OB.UTIL.EPAY.showEPAYSerailNoPopup(args, callbacks);
    } else {
      enyo.$.scrim.hide();
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
  } else {
    if (!OB.UTIL.isNullOrUndefined(args.get('digiProductType'))) {
      if (args.get('digiProductType') === 'P' || args.get('digiProductType') === 'E'|| args.get('digiProductType') === 'SP') {
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        //------------------------activation flow	
      } else if (args.get('digiProductType') === 'A'|| args.get('digiProductType') === 'SA') {
        OB.UTIL.EPAY.showEPAYSerailNoPopup(args, callbacks);
      } else {
        enyo.$.scrim.hide();
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      }
    }
  }
};

OB.UTIL.EPAY = OB.UTIL.EPAY || {};
OB.UTIL.EPAY.showEPAYSerailNoPopup = function (args, callbacks) {
  OB.MobileApp.view.waterfallDown('onShowPopup', {
    popup: 'CPSE.UI.epayActivationSerialNo',
    args: {
      data: args,
      callback: function () {
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      }
    }
  });
};

OB.UTIL.EPAY.call_cancel_API = function (args, txn_id, pos_date, line, callback) {
  var processEPAYPINCancel = new OB.DS.Process('com.promantia.sharaf.epay.process.PINPrintCancellation');
  OB.UTIL.showLoading(true);
  var is_api_cancel_success = false,
      upc, amount, serialNo, salesRep, docNo, prodId,esdResponse;
  if (!OB.UTIL.isNullOrUndefined(args) && !OB.UTIL.isNullOrUndefined(line)) {
    upc = line.get('product').get('uPCEAN');
    amount = line.get('product').get('standardPrice');
    serialNo = line.get('product').get('digiSerialNo');
    salesRep = (!OB.UTIL.isNullOrUndefined(args.context.model)) ? args.context.model.get('order').get('salesRepresentative') : args.context.get('order').get('salesRepresentative');
    docNo = (!OB.UTIL.isNullOrUndefined(args.context.model)) ? args.context.model.get('order').get('documentNo') : args.context.get('order').get('documentNo');
    prodId = line.get('product').get('id');
    esdResponse= !OB.UTIL.isNullOrUndefined(line.get('cpseEpayresponse'))? JSON.parse(line.get('cpseEpayresponse')):null;
  }
  processEPAYPINCancel.exec({
    upc: upc,
    txnRef: txn_id,
    amount: amount,
    serialNo: serialNo,
    currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
    salesRep: salesRep,
    docNo: docNo,
    prodId: prodId,
    date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
    lineNo: line.get('id'),
    isPinCancel: true,
    digitalProductType: line.get('product').get('digiProductType'), 
    txnId: Math.floor((Math.random() * 1000000000000000)),
    esdResponse: esdResponse
  }, function (data) {
    if (data && !data.exception && data.eResponseCode === '0') {
      if (!OB.UTIL.isNullOrUndefined(args)) {
        args.is_cancel_api_success = true;
      }
      callback();
    } else if (data.exception) {
      if (!OB.UTIL.isNullOrUndefined(args)) {
        args.is_cancel_api_success = true;
      }
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), 'Time Out');
      callback();
    }

  }, function (error) {
    if (!OB.UTIL.isNullOrUndefined(args)) {
      args.is_cancel_api_success = false;
    }
  });
};


OB.UTIL.EPAY.call_cancel_API_Deactivation = function (args, txn_id, pos_date, line, callback) {
  var processEPAYPOSDeactivation = new OB.DS.Process('com.promantia.sharaf.epay.process.POSDeactivation');
  OB.UTIL.showLoading(true);
  var is_api_cancel_success = false,
      upc, amount, serialNo, salesRep, docNo, prodId;
  if (!OB.UTIL.isNullOrUndefined(args) && !OB.UTIL.isNullOrUndefined(line)) {
    upc = line.get('product').get('uPCEAN');
    amount = line.get('product').get('standardPrice');
    serialNo = line.get('product').get('digiSerialNo');
    salesRep = (!OB.UTIL.isNullOrUndefined(args.context.model)) ? args.context.model.get('order').get('salesRepresentative') : args.context.get('order').get('salesRepresentative');
    docNo = (!OB.UTIL.isNullOrUndefined(args.context.model)) ? args.context.model.get('order').get('documentNo') : args.context.get('order').get('documentNo');
    prodId = line.get('product').get('id');
  }
  processEPAYPOSDeactivation.exec({
    upc: upc,
    txnRef: txn_id,
    amount: amount,
    serialNo: serialNo,
    currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
    salesRep: salesRep,
    docNo: docNo,
    prodId: prodId,
    date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
    lineNo: line.get('id'),
    isDeactivation: true,
    txnId: Math.floor((Math.random() * 1000000000000000))
  }, function (data) {
    if (data && !data.exception && data.eResponseCode === '0') {
      if (!OB.UTIL.isNullOrUndefined(args)) {
        args.is_cancel_api_success = true;
      }
      callback();
    } else if (data.exception) {
      if (!OB.UTIL.isNullOrUndefined(args)) {
        args.is_cancel_api_success = true;
      }
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), 'Time Out');
      callback();
    }

  }, function (error) {
    if (!OB.UTIL.isNullOrUndefined(args)) {
      args.is_cancel_api_success = false;
    }
  });
};

OB.UTIL.EPAY.call_cancelAPI_SubscribePINPRINTCancel_for_return = function (args, txn_id, pos_date, line, callback) {
  var processSubscribePINPRINTCancel = new OB.DS.Process('com.promantia.sharaf.epay.process.SubscribePINPRINTCancellation');
  OB.UTIL.showLoading(true);
  var is_api_cancel_success = false,
      upc, amount, salesRep, docNo, prodId;
  if (!OB.UTIL.isNullOrUndefined(args) && !OB.UTIL.isNullOrUndefined(line)) {
    upc = line.get('product').get('uPCEAN');
    amount = line.get('product').get('standardPrice');
    salesRep = (!OB.UTIL.isNullOrUndefined(args.context.model)) ? args.context.model.get('order').get('salesRepresentative') : args.context.get('order').get('salesRepresentative');
    docNo = (!OB.UTIL.isNullOrUndefined(args.context.model)) ? args.context.model.get('order').get('documentNo') : args.context.get('order').get('documentNo');
    prodId = line.get('product').get('id');
  }
  processSubscribePINPRINTCancel.exec({
    upc: upc,
    txnRef: txn_id,
    amount: amount,
    currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
    salesRep: salesRep,
    docNo: docNo,
    prodId: prodId,
    date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
    lineNo: line.get('id'),
    isSubscribePINPRINTCancel: true,
    digitalProductType: line.get('product').get('digiProductType'), 
    txnId: Math.floor((Math.random() * 1000000000000000)),
  }, function (data) {
    if (data && !data.exception && data.eResponseCode === '0') {
      if (!OB.UTIL.isNullOrUndefined(args)) {
        args.is_cancel_api_success = true;
      }
      callback();
    } else if (data.exception) {
      if (!OB.UTIL.isNullOrUndefined(args)) {
        args.is_cancel_api_success = false;
      }
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), 'Time Out');
      callback();
    }

  }, function (error) {
    if (!OB.UTIL.isNullOrUndefined(args)) {
      args.is_cancel_api_success = false;
    }
  });
};

OB.UTIL.EPAY.call_cancelAPI_SubscribeActivationCancel_for_return = function (args, txn_id, pos_date, line, callback) {
  var processSubscribeActivationCancel = new OB.DS.Process('com.promantia.sharaf.epay.process.SubscribeActivationCancellation');
  OB.UTIL.showLoading(true);
  var is_api_cancel_success = false,
      upc, amount, serialNo, salesRep, docNo, prodId;
  if (!OB.UTIL.isNullOrUndefined(args) && !OB.UTIL.isNullOrUndefined(line)) {
    upc = line.get('product').get('uPCEAN');
    amount = line.get('product').get('standardPrice');
    serialNo = line.get('product').get('digiSerialNo');
    salesRep = (!OB.UTIL.isNullOrUndefined(args.context.model)) ? args.context.model.get('order').get('salesRepresentative') : args.context.get('order').get('salesRepresentative');
    docNo = (!OB.UTIL.isNullOrUndefined(args.context.model)) ? args.context.model.get('order').get('documentNo') : args.context.get('order').get('documentNo');
    prodId = line.get('product').get('id');
  }
  processSubscribeActivationCancel.exec({
    upc: upc,
    txnRef: txn_id,
    amount: amount,
    serialNo: serialNo,
    currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
    salesRep: salesRep,
    docNo: docNo,
    prodId: prodId,
    date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
    lineNo: line.get('id'),
    isSubscribeActivationCancel: true,
    txnId: Math.floor((Math.random() * 1000000000000000))
  }, function (data) {
    if (data && !data.exception && data.eResponseCode === '0') {
      if (!OB.UTIL.isNullOrUndefined(args)) {
        args.is_cancel_api_success = true;
      }
      callback();
    } else if (data.exception) {
      if (!OB.UTIL.isNullOrUndefined(args)) {
        args.is_cancel_api_success = false;
      }
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), 'Time Out');
      callback();
    }

  }, function (error) {
    if (!OB.UTIL.isNullOrUndefined(args)) {
      args.is_cancel_api_success = false;
    }
  });
};