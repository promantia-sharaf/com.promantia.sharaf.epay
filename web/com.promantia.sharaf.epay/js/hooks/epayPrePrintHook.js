/*global OB, enyo, _ */

(function () {
	OB.UTIL.EPAY = OB.UTIL.EPAY || {};
	//OB.UTIL.EPAY.printEpayTicket = OB.UTIL.EPAY.printEpayTicket || {};
	OB.UTIL.HookManager.registerHook('OBPRINT_PrePrint', function (args, callbacks) {

		if(args.order.get('custsdtDocumenttypeSearchKey') !== 'BS' && args.order.get('hasbeenpaid') === 'Y' && (!OB.MobileApp.model.get('terminal').shfptDisbalePrint ||  args.order.get('shfptIsDuplicatePrint'))){
			 //If EPAY Ticket configuration is present  
			  if (!OB.UTIL.isNullOrUndefined(OB.POS.modelterminal.get('terminal').printESDTicketTemplate) || !OB.UTIL.isNullOrUndefined(OB.POS.modelterminal.get('terminal').printEpayTicketTemplate)){
				  OB.UTIL.EPAY.printEpayTicket(args, function(){
					  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
				  });
			  	} else{
				  //EPAY template configuration is not present... Make a Callback
				  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
			  }
		}else if(args.order.get('custsdtDocumenttypeSearchKey') !== 'BS' && args.order.get('hasbeenpaid') === 'N' && (!OB.MobileApp.model.get('terminal').shfptDisbalePrint ||  args.order.get('shfptIsDuplicatePrint'))) {
			//If EPAY Ticket configuration is present  
			  if (!OB.UTIL.isNullOrUndefined(OB.POS.modelterminal.get('terminal').printEpayClosedTemplate) || !OB.UTIL.isNullOrUndefined(OB.POS.modelterminal.get('terminal').printESDClosedTemplate)){
				  OB.UTIL.EPAY.printPaidEpayTicket(args, function(){
					  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
				  });
			  	} else{
				  //EPAY template configuration is not present... Make a Callback
				  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
			  }
		}else{
			 OB.UTIL.HookManager.callbackExecutor(args, callbacks);
		}

	  });
}());

//For EPAY template print
OB.UTIL.EPAY.printEpayTicket = function(args, callback) {
	var epayTemplateResource, count=0;
    var isVerifiedReturn = args.order.get('isVerifiedReturn');
	epayTemplateResource = new OB.DS.HWResource(OB.POS.modelterminal.get('terminal').printEpayTicketTemplate);
    var esdTempateResource = new OB.DS.HWResource(OB.POS.modelterminal.get('terminal').printESDTicketTemplate);
	args.order.get('lines').models.forEach(function(line){
		if(!OB.UTIL.isNullOrUndefined(line.get('product').get('epayResponse')) && ((!OB.UTIL.isNullOrUndefined(line.get('product').get('digiProductType')) 
         && line.get('product').get('digiProductType') !== 'E' ) || ((!OB.UTIL.isNullOrUndefined(line.get('product').get('digiProductType')) && line.get('product').get('digiProductType') == 'E' ) && !isVerifiedReturn))){
			
			var epayResponse = line.get('product').get('epayResponse');
			if (OB.UTIL.isNullOrUndefined(epayResponse.Header) && OB.UTIL.isNullOrUndefined(epayResponse.TokenReturnResponse)) {
                //to handle data which is not of type string- which doesn't come up in print
    			for(var i=0; i<epayResponse.RECEIPT.LINE.length; i++){
    				epayResponse.RECEIPT.LINE[i] = epayResponse.RECEIPT.LINE[i].toString();
    			}
    
    			OB.POS.hwserver.print(epayTemplateResource, {
    				epayData: epayResponse.RECEIPT
    			  }, function(result) {
    				 if (result && result.exception) {
    					  OB.UTIL.showWarning('Error while printing EPAY receipt');
    				  }
    				});
            } else {
                if (!OB.UTIL.isNullOrUndefined(epayResponse.TokenReturnResponse)) {
                    OB.UTIL.showWarning('Cannot print ESD return receipt');                    
                } else {
                    OB.POS.hwserver.print(esdTempateResource, {
                        epayData: epayResponse.DetailList[0].SubsList[0]
                      }, function(result) {
                         if (result && result.exception) {
                              OB.UTIL.showWarning('Error while printing ESD receipt');
                          }
                        });
                }
            }
            count++;
		  }else {
			  count++;
		  }
	  });

	if(count === args.order.get('lines').models.length){
		callback();
	}
};

//For paid receipt EPAY template print
OB.UTIL.EPAY.printPaidEpayTicket = function(args, callback) {
	var epayTemplateResource, count=0;

	epayTemplateResource = new OB.DS.HWResource(OB.POS.modelterminal.get('terminal').printEpayTicketTemplate);
    var esdTempateResource = new OB.DS.HWResource(OB.POS.modelterminal.get('terminal').printESDTicketTemplate);
	args.order.get('lines').models.forEach(function(line){
		if(!OB.UTIL.isNullOrUndefined(line.get('cpseEpayresponse'))){
			var epayResponse = line.get('cpseEpayresponse');
			epayResponseJSONObj = JSON.parse(epayResponse);
    		
            if (OB.UTIL.isNullOrUndefined(epayResponseJSONObj.Header)) {
            	//to handle data which is not of type string- which doesn't come up in print
    			for(var i=0; i<epayResponseJSONObj.RECEIPT.LINE.length; i++){
    				epayResponseJSONObj.RECEIPT.LINE[i] = epayResponseJSONObj.RECEIPT.LINE[i].toString();
    			}
    
    			OB.POS.hwserver.print(epayTemplateResource, {
    				epayData: epayResponseJSONObj.RECEIPT
    			  }, function(result) {
    				 if (result && result.exception) {
    					  OB.UTIL.showWarning('Error while printing EPAY receipt');
    				  }
    				});
            } else {
                OB.POS.hwserver.print(esdTempateResource, {
                    epayData: epayResponseJSONObj.DetailList[0].SubsList[0]
                  }, function(result) {
                     if (result && result.exception) {
                          OB.UTIL.showWarning('Error while printing ESD receipt');
                      }
                    });
            } 
            count++;            
		  }else {
			  count++;
		  }
	  });

	if(count === args.order.get('lines').models.length){
		callback();
	}
};