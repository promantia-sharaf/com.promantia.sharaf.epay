/**
 * Promantia development
 */

OB.UTIL.HookManager.registerHook('OBRETUR_postAddVerifiedReturnLines', function (args, callbacks) {
  var errorLines = [];
  _.each(args.receipt.get('lines').models, function (line) {
    if (!OB.UTIL.isNullOrUndefined(line.get('product').get('digiProductType')) && (line.get('product').get('digiProductType') === 'P' || line.get('product').get('digiProductType') === 'E' || line.get('product').get('digiProductType') === 'A' || line.get('product').get('digiProductType') === 'SA' || line.get('product').get('digiProductType') === 'SP')) {
      if (OB.UTIL.isNullOrUndefined(line.get('cpseEpayresponse'))) {
        line.set('invoice_type', 'Re Invoice');
        line.get('product').set('invoice_type', 'Re Invoice');
        errorLines.push(line.get('product').get('_identifier'));
      }
    }
  });
  if (errorLines.length > 0) {
    OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CPSE_LblImportantNote'), OB.I18N.getLabel('CPSE_ForbidCallingEpayInReturnForOldInvoices') + ' for article(s) : ' + errorLines);
  }
  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
});
