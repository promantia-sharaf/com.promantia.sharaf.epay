(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_PrePaymentHook', function (args, callbacks) {
    if(!args.context.get('order').get('isEditable')) {
       args.cancelOperation = true;
       OB.UTIL.HookManager.callbackExecutor(args, callbacks); 
    } else {
    var order = args.context.get('order'),
        isError = false,
        errorCards = [],
        lines = order.get('lines'),
        lineCount = 0,
        errors = [],
        newInvoiceFlag = false;
   var digitalProductPresent = false;
      _.each(lines.models, function (line) {
           if(!OB.UTIL.isNullOrUndefined(line.get('product').get('digiProductType')) && (line.get('product').get('digiProductType') === 'P' || 
               line.get('product').get('digiProductType') === 'A' || line.get('product').get('digiProductType') === 'E' || line.get('product').get('digiProductType') === 'SP' 
               || line.get('product').get('digiProductType') === 'SA') && (!order.get('isVerifiedReturn') || !order.get('isQuotation')) 
               && line.get('product').get('custqcDigiApiResponse') === false) {
              OB.UTIL.showLoading(true);
           }
        });
        _.each(lines.models, function(line) {
            if (!OB.UTIL.isNullOrUndefined(line.get('product').get('digiProductType')) && (line.get('product').get('digiProductType') === 'P' ||
                line.get('product').get('digiProductType') === 'A' || line.get('product').get('digiProductType') === 'E' || line.get('product').get('digiProductType') === 'SP'
                || line.get('product').get('digiProductType') === 'SA') && (!order.get('isVerifiedReturn') || !order.get('isQuotation'))) {
                digitalProductPresent = true;
            }
        });
    var finalcallback = function () {
        if (
            order.get('custsdtDocumenttype') &&
            OB.COSRD.getSearchKeyFromDocumentType(
                order.get('custsdtDocumenttype')
            ) &&
            OB.COSRD.getSearchKeyFromDocumentType(order.get('custsdtDocumenttype'))
                .ticketSequence &&
            !order.get('isVerifiedReturn') && digitalProductPresent
        ) {
            if (args.context.get('order').get('custshaCustomerEmail')) {
                OB.MobileApp.view.waterfallDown('onShowPopup', {
                    popup: 'OB_UI_ModalEmailInformation',
                    args: {
                        emaildata: args.context.get('order').get('custshaCustomerEmail'),
                        args: args,
                        callbacks: callbacks,
                    }
                });
            } else {
                OB.MobileApp.view.waterfallDown('onShowPopup', {
                    popup: 'OB_UI_ModalEmailInformation',
                    args: {
                        emaildata: args.context.get('order').get('bp').get('email'),
                        args: args,
                        callbacks: callbacks,
                    }
                });
            }
        } else {
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        }
        };

    var checkActivationSuccessHook = function (line, lineCount) {
        if (line.get('product').get('custqcDigiApiResponse') === false) {
          isError = true;
          errorCards.push(line.get('product').get('description') + ' with error ' + line.get('epayError'));
        }

        if (lineCount === order.attributes.lines.models.length) {
          if (isError) {
            OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), errorCards);
          } else {
            finalcallback();
          }
        }
        };

    var checkActivationSuccessHookForSales = function (line, lineCount) {

        if (OB.UTIL.isNullOrUndefined(line)) {
          return;
        } else {
          if (line.get('product').get('custqcDigiApiResponse') === false) {
            isError = true;
          }

          if (lineCount === order.attributes.lines.models.length) {
            if (!isError) {
              finalcallback();
            }
          }
        }
        };


    var isAPICalledOnce = function () {
        //if API is already called once... dont call again
        var errorProd = [],
            lineNum = 1,
            epayAPIError = false;
        _.each(lines.models, function (line) {
          if (line.get('product').get('custqcDigiApiResponse') === true || line.get('product').get('custqcDigiApiResponse') === false) {
            if (line.get('product').get('custqcDigiApiResponse') === false) {
              errorProd.push('Article ' + line.get('product').get('description') + ' in line no ' + lineNum + ' ');
              lineNum++;
              epayAPIError = true
            } else {
              lineNum++;
            }

          } else {
            lineNum++;
          }
        });
        if (epayAPIError) {
          OB.UTIL.showConfirmation.display('Delete ' + [errorProd] + ' and proceed');
          return;
        } else {
          callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
        }
        //-------------------
        };

    var callAPI = function (args, order, lines, lineCount, checkActivationSuccessHook, callbacks) {
        var newInvoiceCount = 0;
        if (lineCount < lines.models.length) {
        var line = lines.models[lineCount];
          //if API is already called once... dont call again
          if (order.get('paymentButtonClicked') === true && line.get('product').get('custqcDigiApiResponse') === true) {
            lineCount++;
            checkActivationSuccessHook(line, lineCount);
            callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
          } else if (order.get('paymentButtonClicked') === true && !line.get('product').get('custqcDigiApiResponse') && line.get('product').get('invoice_type') === 'New Invoice') {
            if (!OB.UTIL.isNullOrUndefined(line.get('cpseEpayresponse'))) {
              var epayResponse = JSON.parse(line.get('cpseEpayresponse'));
              if (epayResponse.TYPE === 'PINPRINTING') {
                var processEPAYPINCancel = new OB.DS.Process('com.promantia.sharaf.epay.process.PINPrintCancellation');
                OB.UTIL.showLoading(true);
                var pos_date = new Date();
                var txn_id = line.get('id') + '_' + Math.floor((Math.random() * 1000000));
                var start_date = new Date();
                processEPAYPINCancel.exec({
                  upc: line.get('product').get('uPCEAN'),
                  txnRef: epayResponse.TXID,
                  orignalTerminalId: epayResponse.TERMINALID,
                  amount: line.get('price'),
                  currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
                  salesRep: order.get('salesRepresentative'),
                  docNo: order.get('documentNo'),
                  prodId: line.get('product').get('id'),
                  isPinCancel: true,
                  digitalProductType: line.get('product').get('digiProductType'),
                  date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
                  lineNo: line.get('id'),
                  txnId: txn_id,
                  esdResponse: JSON.parse(line.get('cpseEpayresponse'))
                }, function (data) {
                  OB.UTIL.showLoading(false);
                  if (data && !data.exception && data.eResponseCode === '0' && data.Status != 'failure') {
                    line.get('product').set('custqcDigiApiResponse', true);
                    //success response from EPAY
                    if (line.get('product').get('digiProductType') === 'E') {
                       data.eResponse = JSON.parse(data.eResponse);
                    }
                    line.get('product').set('epayResponse', data.eResponse);
                    line.get('product').set('HTTPRequest', data.HTTPRequest);
                    line.get('product').set('HTTPResponse', data.HTTPResponse);
                    order.save();
                    lineCount++;
                    checkActivationSuccessHook(line, lineCount);
                    callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
                  } else if (data.exception) {
                    if (data && data.exception && data.exception.status && data.exception.status.data && data.exception.status.data.eResponseCode === '5') {
                      line.get('product').set('custqcDigiApiResponse', true);
                    } else {
                      line.get('product').set('custqcDigiApiResponse', false);
                    }
                    order.save();
                    line.set('epayError', data.exception.status.errorMessage);
                    lineCount++;
                    checkActivationSuccessHook(line, lineCount);
                    callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
                  } else if (data.Status == 'failure') {
                    line.get('product').set('custqcDigiApiResponse', false);
                    lineCount++;
                    if(data.Message != null) {
                     OB.UTIL.showConfirmation.display(data.Message);
                    }
                    callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
                   }
                }, function (error) {
                if(line.get('product').get('digiProductType') === 'P' || line.get('product').get('digiProductType') === 'A') {   
                  pos_date = new Date();
                  var is_cancel_success = false;
                  OB.UTIL.EPAY.call_cancelAPI_for_return(args, txn_id, pos_date, order, line, epayResponse, function () {
                    if (args.is_cancel_api_success) {
                      line.get('product').set('custqcDigiApiResponse', true);
                      order.save();
                      lineCount++;
                      checkActivationSuccessHook(line, lineCount);
                      callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
                    } else {
                      var is_cancel_success = false;
                      OB.UTIL.EPAY.call_cancelAPI_for_return(args, txn_id, pos_date, order, line, epayResponse, function () {
                        if (args.is_cancel_api_success) {
                          line.get('product').set('custqcDigiApiResponse', true);
                          order.save();
                          lineCount++;
                          checkActivationSuccessHook(line, lineCount);
                          callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
                        } else {
                          var is_cancel_success = false;
                          OB.UTIL.EPAY.call_cancelAPI_for_return(args, txn_id, pos_date, order, line, epayResponse, function () {
                            if (args.is_cancel_api_success) {
                              line.get('product').set('custqcDigiApiResponse', true);
                              order.save();
                              lineCount++;
                              checkActivationSuccessHook(line, lineCount);
                              callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
                            } else {
                              line.get('product').set('custqcDigiApiResponse', false);
                              order.save();
                              lineCount++;
                              if (!OB.UTIL.isNullOrUndefined(args.recursiveError)) {
                                line.set('epayError', args.recursiveError);
                              } else {
                                line.set('epayError', 'Time Out');
                              }
                              checkActivationSuccessHook(line, lineCount);
                              callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
                            }
                          });
                        }
                      });
                    }
                   });
                  }
                });
                //de-activation flow
              } else if (epayResponse.TYPE === 'ACTIVATE') {
                var processPOSDeactivate = new OB.DS.Process('com.promantia.sharaf.epay.process.POSDeactivation');
                OB.UTIL.showLoading(true);
                var pos_date = new Date();
                var txn_id = line.get('id') + '_' + Math.floor((Math.random() * 1000000));
                processPOSDeactivate.exec({
                  upc: line.get('product').get('uPCEAN'),
                  txnRef: epayResponse.TXID,
                  orignalTerminalId: epayResponse.TERMINALID,
                  amount: line.get('price'),
                  serialNo: line.get('cpseEpaySerialno'),
                  currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
                  salesRep: order.get('salesRepresentative'),
                  docNo: order.get('documentNo'),
                  prodId: line.get('product').get('id'),
                  isDeactivation: true,
                  date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
                  lineNo: line.get('id'),
                  txnId: txn_id
                }, function (data) {
                  OB.UTIL.showLoading(false);
                  if (data && !data.exception && data.eResponseCode === '0') {
                    line.get('product').set('custqcDigiApiResponse', true);
                    //success response from EPAY
                    line.get('product').set('epayResponse', data.eResponse);
                    line.get('product').set('HTTPRequest', data.HTTPRequest);
                    line.get('product').set('HTTPResponse', data.HTTPResponse);
                    order.save();
                    lineCount++;
                    checkActivationSuccessHook(line, lineCount);
                    callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
                  } else if (data.exception) {
                    if (data.exception.status.data.eResponseCode === '5') {
                      line.get('product').set('custqcDigiApiResponse', true);
                    } else {
                      line.get('product').set('custqcDigiApiResponse', false);
                    }
                    order.save();
                    line.set('epayError', data.exception.status.errorMessage);
                    lineCount++;
                    checkActivationSuccessHook(line, lineCount);
                    callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
                  }
                }, function (error) {

                  pos_date = new Date();
                  var is_cancel_success = false;
                  OB.UTIL.EPAY.call_cancelAPI_Deactivation_for_return(args, txn_id, pos_date, order, line, epayResponse, function () {
                    if (args.is_cancel_api_success) {
                      line.get('product').set('custqcDigiApiResponse', true);
                      order.save();
                      lineCount++;
                      checkActivationSuccessHook(line, lineCount);
                      callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
                    } else {
                      var is_cancel_success = false;
                      OB.UTIL.EPAY.call_cancelAPI_Deactivation_for_return(args, txn_id, pos_date, order, line, epayResponse, function () {
                        if (args.is_cancel_api_success) {
                          line.get('product').set('custqcDigiApiResponse', true);
                          order.save();
                          lineCount++;
                          checkActivationSuccessHook(line, lineCount);
                          callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
                        } else {
                          var is_cancel_success = false;
                          OB.UTIL.EPAY.call_cancelAPI_Deactivation_for_return(args, txn_id, pos_date, order, line, epayResponse, function () {
                            if (args.is_cancel_api_success) {
                              line.get('product').set('custqcDigiApiResponse', true);
                              order.save();
                              lineCount++;
                              checkActivationSuccessHook(line, lineCount);
                              callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
                            } else {
                              line.get('product').set('custqcDigiApiResponse', false);
                              order.save();
                              lineCount++;
                              if (!OB.UTIL.isNullOrUndefined(args.recursiveError)) {
                                line.set('epayError', args.recursiveError);
                              } else {
                                line.set('epayError', 'Time Out');
                              }
                              checkActivationSuccessHook(line, lineCount);
                              callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
                            }
                          });
                        }
                      });
                    }
                  });
                });
              } else if (epayResponse.TYPE === 'SALE') {
                if((line.get('product').get('digiProductType') === 'SA')) {
                var processSubscribeActivationCancel = new OB.DS.Process('com.promantia.sharaf.epay.process.SubscribeActivationCancellation');
                OB.UTIL.showLoading(true);
                var pos_date = new Date();
                var txn_id = line.get('id') + '_' + Math.floor((Math.random() * 1000000));
                processSubscribeActivationCancel.exec({
                  upc: line.get('product').get('uPCEAN'),
                  txnRef: epayResponse.TXID,
                  orignalTerminalId: epayResponse.TERMINALID,
                  amount: line.get('price'),
                  serialNo: line.get('cpseEpaySerialno'),
                  currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
                  salesRep: order.get('salesRepresentative'),
                  docNo: order.get('documentNo'),
                  prodId: line.get('product').get('id'),
                  isSubscribeActivationCancel: true,
                  digitalProductType: line.get('product').get('digiProductType'),
                  date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
                  lineNo: line.get('id'),
                  txnId: txn_id
                }, function (data) {
                  OB.UTIL.showLoading(false);
                  if (data && !data.exception && data.eResponseCode === '0') {
                    line.get('product').set('custqcDigiApiResponse', true);
                    //success response from EPAY
                    line.get('product').set('epayResponse', data.eResponse);
                    line.get('product').set('HTTPRequest', data.HTTPRequest);
                    line.get('product').set('HTTPResponse', data.HTTPResponse);
                    order.save();
                    lineCount++;
                    checkActivationSuccessHook(line, lineCount);
                    callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
                  } else if (data.exception) {
                    if (data.exception.status.data.eResponseCode === '5') {
                      line.get('product').set('custqcDigiApiResponse', true);
                    } else {
                      line.get('product').set('custqcDigiApiResponse', false);
                    }
                    order.save();
                    line.set('epayError', data.exception.status.errorMessage);
                    lineCount++;
                    checkActivationSuccessHook(line, lineCount);
                    callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
                  }
                }, function (error) {

                  pos_date = new Date();
                  var is_cancel_success = false;
                  OB.UTIL.EPAY.call_cancelAPI_SubscribeActivationCancel_for_return(args, txn_id, pos_date, order, line, epayResponse, function () {
                    if (args.is_cancel_api_success) {
                      line.get('product').set('custqcDigiApiResponse', true);
                      order.save();
                      lineCount++;
                      checkActivationSuccessHook(line, lineCount);
                      callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
                    } else {
                      var is_cancel_success = false;
                      OB.UTIL.EPAY.call_cancelAPI_SubscribeActivationCancel_for_return(args, txn_id, pos_date, order, line, epayResponse, function () {
                        if (args.is_cancel_api_success) {
                          line.get('product').set('custqcDigiApiResponse', true);
                          order.save();
                          lineCount++;
                          checkActivationSuccessHook(line, lineCount);
                          callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
                        } else {
                          var is_cancel_success = false;
                          OB.UTIL.EPAY.call_cancelAPI_SubscribeActivationCancel_for_return(args, txn_id, pos_date, order, line, epayResponse, function () {
                            if (args.is_cancel_api_success) {
                              line.get('product').set('custqcDigiApiResponse', true);
                              order.save();
                              lineCount++;
                              checkActivationSuccessHook(line, lineCount);
                              callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
                            } else {
                              line.get('product').set('custqcDigiApiResponse', false);
                              order.save();
                              lineCount++;
                              if (!OB.UTIL.isNullOrUndefined(args.recursiveError)) {
                                line.set('epayError', args.recursiveError);
                              } else {
                                line.set('epayError', 'Time Out');
                              }
                              checkActivationSuccessHook(line, lineCount);
                              callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
                            }
                          });
                        }
                      });
                    }
                  });
                });
               }
               if((line.get('product').get('digiProductType') === 'SP')) {
                var processSubscribePINPRINTCancel = new OB.DS.Process('com.promantia.sharaf.epay.process.SubscribePINPRINTCancellation');
                OB.UTIL.showLoading(true);
                var pos_date = new Date();
                var txn_id = line.get('id') + '_' + Math.floor((Math.random() * 1000000));
                processSubscribePINPRINTCancel.exec({
                  upc: line.get('product').get('uPCEAN'),
                  txnRef: epayResponse.TXID,
                  orignalTerminalId: epayResponse.TERMINALID,
                  amount: line.get('price'),
                  currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
                  salesRep: order.get('salesRepresentative'),
                  docNo: order.get('documentNo'),
                  prodId: line.get('product').get('id'),
                  isSubscribePINPRINTCancel: true,
                  digitalProductType: line.get('product').get('digiProductType'),
                  date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
                  lineNo: line.get('id'),
                  txnId: txn_id
                }, function (data) {
                  OB.UTIL.showLoading(false);
                  if (data && !data.exception && data.eResponseCode === '0') {
                    line.get('product').set('custqcDigiApiResponse', true);
                    //success response from EPAY
                    line.get('product').set('epayResponse', data.eResponse);
                    line.get('product').set('HTTPRequest', data.HTTPRequest);
                    line.get('product').set('HTTPResponse', data.HTTPResponse);
                    order.save();
                    lineCount++;
                    checkActivationSuccessHook(line, lineCount);
                    callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
                  } else if (data.exception) {
                    if (data.exception.status.data.eResponseCode === '5') {
                      line.get('product').set('custqcDigiApiResponse', true);
                    } else {
                      line.get('product').set('custqcDigiApiResponse', false);
                    }
                    order.save();
                    line.set('epayError', data.exception.status.errorMessage);
                    lineCount++;
                    checkActivationSuccessHook(line, lineCount);
                    callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
                  }
                }, function (error) {

                  pos_date = new Date();
                  var is_cancel_success = false;
                  OB.UTIL.EPAY.call_cancelAPI_SubscribePINPRINTCancel_for_return(args, txn_id, pos_date, order, line, epayResponse, function () {
                    if (args.is_cancel_api_success) {
                      line.get('product').set('custqcDigiApiResponse', true);
                      order.save();
                      lineCount++;
                      checkActivationSuccessHook(line, lineCount);
                      callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
                    } else {
                      var is_cancel_success = false;
                      OB.UTIL.EPAY.call_cancelAPI_Deactivation_for_return(args, txn_id, pos_date, order, line, epayResponse, function () {
                        if (args.is_cancel_api_success) {
                          line.get('product').set('custqcDigiApiResponse', true);
                          order.save();
                          lineCount++;
                          checkActivationSuccessHook(line, lineCount);
                          callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
                        } else {
                          var is_cancel_success = false;
                          OB.UTIL.EPAY.call_cancelAPI_Deactivation_for_return(args, txn_id, pos_date, order, line, epayResponse, function () {
                            if (args.is_cancel_api_success) {
                              line.get('product').set('custqcDigiApiResponse', true);
                              order.save();
                              lineCount++;
                              checkActivationSuccessHook(line, lineCount);
                              callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
                            } else {
                              line.get('product').set('custqcDigiApiResponse', false);
                              order.save();
                              lineCount++;
                              if (!OB.UTIL.isNullOrUndefined(args.recursiveError)) {
                                line.set('epayError', args.recursiveError);
                              } else {
                                line.set('epayError', 'Time Out');
                              }
                              checkActivationSuccessHook(line, lineCount);
                              callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
                            }
                          });
                        }
                      });
                    }
                  });
                 });
                 } 
                } else {
                //unknown response type
                lineCount++;
                checkActivationSuccessHook(line, lineCount);
                callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
              }
            } else {
              // New digital product added into Verified return
              if (line.get('product').get('invoice_type') === 'New Invoice' && (line.get('product').get('digiProductType') === 'P' || line.get('product').get('digiProductType') === 'E' || line.get('product').get('digiProductType') === 'A'|| line.get('product').get('digiProductType') === 'SA'|| line.get('product').get('digiProductType') === 'SP')) {

                newInvoiceCount++;
              } else {
                //non-digital products
                //do nothing
                lineCount++;
                checkActivationSuccessHook(line, lineCount);
                callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
              }
            }
          } else {
            if (line.get('product').get('invoice_type') === 'New Invoice') {
              isAPICalledOnceForSales();
            } else {
              lineCount++;
              checkActivationSuccessHook(line, lineCount);
              callAPI(args, order, lines, lineCount, checkActivationSuccessHook, callbacks);
            }
          }
         } else {
            checkActivationSuccessHook(lines.models[lineCount-1], lineCount);
         }
        if (newInvoiceCount > 0) {
          isAPICalledOnceForSales();
        }
        };

    var isAPICalledOnceForSales = function () {
        //if API is already called once... dont call again
        var errorProd = [],
            lineNum = 1,
            epayAPIError = false;
        _.each(lines.models, function (line) {
          if (line.get('product').get('custqcDigiApiResponse') === true || line.get('product').get('custqcDigiApiResponse') === false) {
            if (line.get('product').get('custqcDigiApiResponse') === false) {
              errorProd.push('Article ' + line.get('product').get('description') + ' in line no ' + lineNum + ' ');
              lineNum++;
              epayAPIError = true
            } else {
              lineNum++;
            }

          } else {
            lineNum++;
          }
        });
        if (epayAPIError) {
          OB.UTIL.showConfirmation.display('Delete ' + [errorProd] + ' and proceed');
          return;
        } else {
          if (order.get('custsdtDocumenttypeSearchKey') !== 'BS') {
            enyo.$.scrim.show();
            callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
          } else {
            finalcallback();
          }
        }
        };

    if (order.get('isQuotation')) {
      finalcallback();
    } else if (order.get('isVerifiedReturn')) {
      _.each(lines.models, function (line) {
        if (!OB.UTIL.isNullOrUndefined(line.get('product').get('digiProductType'))) {
          order.set('digiProductPresent', true);
        } else if (!OB.UTIL.isNullOrUndefined(line.get('cpseEpayresponse'))) {
          var epayResponseCheck = JSON.parse(line.get('cpseEpayresponse'));
          if (!OB.UTIL.isNullOrUndefined(epayResponseCheck) && !OB.UTIL.isNullOrUndefined(epayResponseCheck.TYPE) && 
               epayResponseCheck.TYPE === 'PINPRINTING' && OB.UTIL.isNullOrUndefined(epayResponseCheck.Header)) {
             order.set('digiProductPresent', true);
             line.get('product').set('digiProductType', 'P')             
          } else if (!OB.UTIL.isNullOrUndefined(epayResponseCheck) && !OB.UTIL.isNullOrUndefined(epayResponseCheck.TYPE) && 
               epayResponseCheck.TYPE === 'PINPRINTING' && !OB.UTIL.isNullOrUndefined(epayResponseCheck.Header)) {
            order.set('digiProductPresent', true);
            line.get('product').set('digiProductType', 'E')
          }else if (!OB.UTIL.isNullOrUndefined(epayResponseCheck) && !OB.UTIL.isNullOrUndefined(epayResponseCheck.TYPE)
                 &&  epayResponseCheck.TYPE === 'ACTIVATE') {
             order.set('digiProductPresent', true);
             line.get('product').set('digiProductType', 'A')
          }else if (!OB.UTIL.isNullOrUndefined(epayResponseCheck) && !OB.UTIL.isNullOrUndefined(epayResponseCheck.TYPE)
                 &&  epayResponseCheck.TYPE === 'SALE' 
                 &&  !OB.UTIL.isNullOrUndefined(line.get('cpseEpayrequest')) 
                 &&  ! line.get('cpseEpayrequest').includes("PAN")) {
             order.set('digiProductPresent', true);
             line.get('product').set('digiProductType', 'SP')
          }else if (!OB.UTIL.isNullOrUndefined(epayResponseCheck) && !OB.UTIL.isNullOrUndefined(epayResponseCheck.TYPE)
                 &&  epayResponseCheck.TYPE === 'SALE' 
                 && !OB.UTIL.isNullOrUndefined(line.get('cpseEpayrequest')) 
                 && line.get('cpseEpayrequest').includes("PAN")) {
             order.set('digiProductPresent', true);
             line.get('product').set('digiProductType', 'SA')
          }  
        }
      });

      //check whether all digital products are having invoice type 
      var invoiceTypeNotSelected = false;
      if (order.get('digiProductPresent')) {
        _.each(lines.models, function (line) {
          if ((!OB.UTIL.isNullOrUndefined(line.get('product').get('digiProductType')) && OB.UTIL.isNullOrUndefined(line.get('product').get('invoice_type'))) || (!OB.UTIL.isNullOrUndefined(line.get('custdisOffer')) && OB.UTIL.isNullOrUndefined(line.get('product').get('invoice_type')) && !OB.UTIL.isNullOrUndefined(line.get('custdisOrderline')))) {
            invoiceTypeNotSelected = true;
          }
        });
      }

      if (order.get('custsdtDocumenttypeSearchKey') !== 'BR') {
        if (order.get('digiProductPresent') && invoiceTypeNotSelected) {
          OB.MobileApp.view.waterfall('onShowPopup', {
            popup: 'CPSE.UI.epayInvoiceType',
            args: {
              data: args,
              callback: function () {
                if (args.invoice_type === 'new_invoice') {
                  //if new invoice call API to de-activate
                  isAPICalledOnce();
                } else {
                  finalcallback();
                }
              }
            }
          });

          //set the payment button clicked status to true
          if (OB.UTIL.isNullOrUndefined(order.get('paymentButtonClicked'))) {
            order.set('paymentButtonClicked', true);
            order.save();
          }
        } else if (order.get('digiProductPresent')) {
          _.each(lines.models, function (line) {
            if (line.get('product').get('invoice_type') === 'New Invoice') {
              newInvoiceFlag = true;
            }
          });

          //set the payment button clicked status to true
          if (OB.UTIL.isNullOrUndefined(order.get('paymentButtonClicked'))) {
            order.set('paymentButtonClicked', true);
            order.save();
          }

          if (newInvoiceFlag) {
            //if new invoice call API to de-activate
            isAPICalledOnce();
          } else {
            finalcallback();
          }
        } else {
          finalcallback();
        }
      } else {
        finalcallback();
      }

    } else {
      _.each(lines.models, function (line) {
        if (!OB.UTIL.isNullOrUndefined(line.get('product').get('digiProductType'))) {
          order.set('digiProductPresent', true);
        }
        if (order.get('digiProductPresent')) {
          if (line.get('product').get('invoice_type') !== 'Re Invoice') {
            line.get('product').set('invoice_type', 'new_invoice');
            newInvoiceFlag = true;
          }
        }
      });

      if (order.get('digiProductPresent')) {
        if (newInvoiceFlag) {
          //if new invoice call API to activate
          isAPICalledOnceForSales();
        } else {
          finalcallback();
        }

        //set the payment button clicked status to true
        if (OB.UTIL.isNullOrUndefined(order.get('paymentButtonClicked'))) {
          order.set('paymentButtonClicked', true);
          order.save();
        }
      } else {
        finalcallback();
      }
    }
    }
  });
}());

var callAPIForSales = function (args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks) {
    if (lineCount < lines.models.length) {
      var line = lines.models[lineCount];
      //if API is already called once, no need to call again
      if (order.get('paymentButtonClicked') === true && line.get('product').get('custqcDigiApiResponse') === true) {
        lineCount++;
        checkActivationSuccessHookForSales(line, lineCount);
        callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
      } else if (!line.get('product').get('custqcDigiApiResponse') && (line.get('product').get('invoice_type') === 'new_invoice' || line.get('product').get('invoice_type') === 'New Invoice')) {

        //-------------PIN print flow
        if (line.get('product').get('digiProductType') === 'P' || line.get('product').get('digiProductType') === 'E') {
          var processEPAYPinPrint = new OB.DS.Process('com.promantia.sharaf.epay.process.PINPrintActivation');
          OB.UTIL.showLoading(true);
          var pos_date = new Date();
          var txn_id = line.get('id') + '_' + Math.floor((Math.random() * 1000000));
          var start_date = new Date();
          processEPAYPinPrint.exec({
            upc: line.get('product').get('uPCEAN'),
            amount: line.get('product').get('standardPrice'),
            currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
            salesRep: order.get('salesRepresentative'),
            docNo: order.get('documentNo'),
            prodId: line.get('product').get('id'),
            isPinPrint: true,
            digitalProductType: line.get('product').get('digiProductType'),
            date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
            lineNo: line.get('id'),
            txnId: txn_id
          }, function (data) {
            enyo.$.scrim.hide();
            if (data && !data.exception && data.eResponseCode === '0') {
              line.get('product').set('custqcDigiApiResponse', true);
              OB.UTIL.showLoading(false);
              //success response from EPAY
              if (line.get('product').get('digiProductType') === 'E') {
                data.eResponse = JSON.parse(data.eResponse);
              }
              line.get('product').set('epayResponse', data.eResponse);
              line.get('product').set('HTTPRequest', data.HTTPRequest);
              line.get('product').set('HTTPResponse', data.HTTPResponse);
              order.save();
              lineCount++;
              checkActivationSuccessHookForSales(line, lineCount);
              callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
            } else if (data.exception) {
              args.cancelOperation = true;
              if (data.exception.status && data.exception.status.message) {
                OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), data.exception.status.data.Message);
              } else if (data.exception.status.data && data.exception.status.data.eResponseMessage) {
                OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), data.exception.status.data.eResponseMessage);
              }
              line.get('product').set('custqcDigiApiResponse', false);
              order.save();
              line.set('epayError', data.exception.message);
              lineCount++;
              checkActivationSuccessHookForSales(line, lineCount);
              callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
            }

          }, function (error) {
            pos_date = new Date();
            var is_cancel_success = false;
            OB.UTIL.EPAY.call_cancel_API(args, txn_id, pos_date, line, function () {
              if (args.is_cancel_api_success) {
                enyo.$.scrim.hide();
                args.cancelOperation = true;
                line.get('product').set('custqcDigiApiResponse', false);
                order.save();
                lineCount++;
                checkActivationSuccessHookForSales(line, lineCount);
                callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
              } else {
                var is_cancel_success = false;
                OB.UTIL.EPAY.call_cancel_API(args, txn_id, pos_date, line, function () {
                  if (args.is_cancel_api_success) {
                    enyo.$.scrim.hide();
                    args.cancelOperation = true;
                    line.get('product').set('custqcDigiApiResponse', false);
                    order.save();
                    lineCount++;
                    checkActivationSuccessHookForSales(line, lineCount);
                    callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
                  } else {
                    var is_cancel_success = false;
                    OB.UTIL.EPAY.call_cancel_API(args, txn_id, pos_date, line, function () {
                      if (args.is_cancel_api_success) {
                        enyo.$.scrim.hide();
                        args.cancelOperation = true;
                        line.get('product').set('custqcDigiApiResponse', false);
                        order.save();
                        lineCount++;
                        checkActivationSuccessHookForSales(line, lineCount);
                        callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
                      } else {
                        enyo.$.scrim.hide();
                        line.get('product').set('custqcDigiApiResponse', false);
                        order.save();
                        lineCount++;
                        if (!OB.UTIL.isNullOrUndefined(args.recursiveError)) {
                          line.set('epayError', args.recursiveError);
                        } else {
                          line.set('epayError', 'Time Out');
                        }
                        checkActivationSuccessHookForSales(line, lineCount);
                        callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
                      }
                    });
                  }
                });
              }
            });
          });
          //------------------------activation flow	
        } else if (line.get('product').get('digiProductType') === 'SA') {
          var processEPAYPinPrint = new OB.DS.Process('com.promantia.sharaf.epay.process.SubscriptionActivation');
          OB.UTIL.showLoading(true);
          var pos_date = new Date();
          var txn_id = line.get('id') + '_' + Math.floor((Math.random() * 1000000));
          var start_date = new Date();
          processEPAYPinPrint.exec({
            upc: line.get('product').get('uPCEAN'),
            amount: line.get('product').get('standardPrice'),
            serialNo: line.get('product').get('digiSerialNo'),
            currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
            salesRep: order.get('salesRepresentative'),
            docNo: order.get('documentNo'),
            prodId: line.get('product').get('id'),
            isSubscriptionActivation: true,
            digitalProductType: line.get('product').get('digiProductType'),
            date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
            lineNo: line.get('id'),
            txnId: txn_id,
            contractId: line.get('id'),
            redirectionDivisionId: line.get('product').get('redirectionDivisionId')
          }, function (data) {
            enyo.$.scrim.hide();
            if (data && !data.exception && data.eResponseCode === '0') {
              line.get('product').set('custqcDigiApiResponse', true);
              OB.UTIL.showLoading(false);
              //success response from EPAY
              if (line.get('product').get('digiProductType') === 'E') {
                data.eResponse = JSON.parse(data.eResponse);
              }
              line.get('product').set('epayResponse', data.eResponse);
              line.get('product').set('HTTPRequest', data.HTTPRequest);
              line.get('product').set('HTTPResponse', data.HTTPResponse);
              order.save();
              lineCount++;
              checkActivationSuccessHookForSales(line, lineCount);
              callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
            } else if (data.exception) {
              args.cancelOperation = true;
              if (data.exception.status && data.exception.status.message) {
                OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), data.exception.status.message && line.get('product').get('digiProductType') === 'E');
              } else {
                OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), data.exception.message);
              }
              line.get('product').set('custqcDigiApiResponse', false);
              order.save();
              line.set('epayError', data.exception.message);
              lineCount++;
              checkActivationSuccessHookForSales(line, lineCount);
              callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
            }

          }, function (error) {
            pos_date = new Date();
            var is_cancel_success = false;
            OB.UTIL.EPAY.call_cancel_API(args, txn_id, pos_date, line, function () {
              if (args.is_cancel_api_success) {
                enyo.$.scrim.hide();
                args.cancelOperation = true;
                line.get('product').set('custqcDigiApiResponse', false);
                order.save();
                lineCount++;
                checkActivationSuccessHookForSales(line, lineCount);
                callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
              } else {
                var is_cancel_success = false;
                OB.UTIL.EPAY.call_cancel_API(args, txn_id, pos_date, line, function () {
                  if (args.is_cancel_api_success) {
                    enyo.$.scrim.hide();
                    args.cancelOperation = true;
                    line.get('product').set('custqcDigiApiResponse', false);
                    order.save();
                    lineCount++;
                    checkActivationSuccessHookForSales(line, lineCount);
                    callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
                  } else {
                    var is_cancel_success = false;
                    OB.UTIL.EPAY.call_cancel_API(args, txn_id, pos_date, line, function () {
                      if (args.is_cancel_api_success) {
                        enyo.$.scrim.hide();
                        args.cancelOperation = true;
                        line.get('product').set('custqcDigiApiResponse', false);
                        order.save();
                        lineCount++;
                        checkActivationSuccessHookForSales(line, lineCount);
                        callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
                      } else {
                        enyo.$.scrim.hide();
                        line.get('product').set('custqcDigiApiResponse', false);
                        order.save();
                        lineCount++;
                        if (!OB.UTIL.isNullOrUndefined(args.recursiveError)) {
                          line.set('epayError', args.recursiveError);
                        } else {
                          line.set('epayError', 'Time Out');
                        }
                        checkActivationSuccessHookForSales(line, lineCount);
                        callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
                      }
                    });
                  }
                });
              }
            });
          });
          //------------------------activation flow 
        }else if (line.get('product').get('digiProductType') === 'SP') {
          var processEPAYPinPrint = new OB.DS.Process('com.promantia.sharaf.epay.process.SubscriptionPINPRINT');
          OB.UTIL.showLoading(true);
          var pos_date = new Date();
          var txn_id = line.get('id') + '_' + Math.floor((Math.random() * 1000000));
          var start_date = new Date();
          processEPAYPinPrint.exec({
            upc: line.get('product').get('uPCEAN'),
            amount: line.get('product').get('standardPrice'),
            currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
            salesRep: order.get('salesRepresentative'),
            docNo: order.get('documentNo'),
            prodId: line.get('product').get('id'),
            isSubscriptionPINPRINT: true,
            digitalProductType: line.get('product').get('digiProductType'),
            date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
            lineNo: line.get('id'),
            txnId: txn_id,
            contractId: line.get('id'),
            redirectionDivisionId: line.get('product').get('redirectionDivisionId')
          }, function (data) {
            enyo.$.scrim.hide();
            if (data && !data.exception && data.eResponseCode === '0') {
              line.get('product').set('custqcDigiApiResponse', true);
              OB.UTIL.showLoading(false);
              //success response from EPAY
              if (line.get('product').get('digiProductType') === 'E') {
                data.eResponse = JSON.parse(data.eResponse);
              }
              line.get('product').set('epayResponse', data.eResponse);
              line.get('product').set('HTTPRequest', data.HTTPRequest);
              line.get('product').set('HTTPResponse', data.HTTPResponse);
              order.save();
              lineCount++;
              checkActivationSuccessHookForSales(line, lineCount);
              callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
            } else if (data.exception) {
              args.cancelOperation = true;
              if (data.exception.status && data.exception.status.message) {
                OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), data.exception.status.message && line.get('product').get('digiProductType') === 'E');
              } else {
                OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), data.exception.message);
              }
              line.get('product').set('custqcDigiApiResponse', false);
              order.save();
              line.set('epayError', data.exception.message);
              lineCount++;
              checkActivationSuccessHookForSales(line, lineCount);
              callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
            }

          }, function (error) {
            pos_date = new Date();
            var is_cancel_success = false;
            OB.UTIL.EPAY.call_cancel_API(args, txn_id, pos_date, line, function () {
              if (args.is_cancel_api_success) {
                enyo.$.scrim.hide();
                args.cancelOperation = true;
                line.get('product').set('custqcDigiApiResponse', false);
                order.save();
                lineCount++;
                checkActivationSuccessHookForSales(line, lineCount);
                callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
              } else {
                var is_cancel_success = false;
                OB.UTIL.EPAY.call_cancel_API(args, txn_id, pos_date, line, function () {
                  if (args.is_cancel_api_success) {
                    enyo.$.scrim.hide();
                    args.cancelOperation = true;
                    line.get('product').set('custqcDigiApiResponse', false);
                    order.save();
                    lineCount++;
                    checkActivationSuccessHookForSales(line, lineCount);
                    callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
                  } else {
                    var is_cancel_success = false;
                    OB.UTIL.EPAY.call_cancel_API(args, txn_id, pos_date, line, function () {
                      if (args.is_cancel_api_success) {
                        enyo.$.scrim.hide();
                        args.cancelOperation = true;
                        line.get('product').set('custqcDigiApiResponse', false);
                        order.save();
                        lineCount++;
                        checkActivationSuccessHookForSales(line, lineCount);
                        callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
                      } else {
                        enyo.$.scrim.hide();
                        line.get('product').set('custqcDigiApiResponse', false);
                        order.save();
                        lineCount++;
                        if (!OB.UTIL.isNullOrUndefined(args.recursiveError)) {
                          line.set('epayError', args.recursiveError);
                        } else {
                          line.set('epayError', 'Time Out');
                        }
                        checkActivationSuccessHookForSales(line, lineCount);
                        callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
                      }
                    });
                  }
                });
              }
            });
          });
          //------------------------activation flow 
        } else if (line.get('product').get('digiProductType') === 'A') {
          OB.UTIL.EPAY.callEPAYActivation(args, order, lines, line, lineCount, checkActivationSuccessHookForSales, callAPIForSales, callbacks);
        } else {
          lineCount++;
          checkActivationSuccessHookForSales(line, lineCount);
          callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
        }

      } else {
        lineCount++;
        checkActivationSuccessHookForSales(line, lineCount);
        callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
      }
    } else {
      checkActivationSuccessHookForSales(line, lineCount);
    }
    };

OB.UTIL.EPAY.callEPAYActivation = function (args, order, lines, line, lineCount, checkActivationSuccessHookForSales, callAPIForSales, callbacks) {
  var processEPAYActivation = new OB.DS.Process('com.promantia.sharaf.epay.process.EPAYActivation');
  OB.UTIL.showLoading(true);
  var pos_date = new Date();
  var txn_id = line.get('id') + '_' + Math.floor((Math.random() * 1000000));
  var start_date = new Date();
  processEPAYActivation.exec({
    upc: line.get('product').get('uPCEAN'),
    amount: line.get('product').get('standardPrice'),
    serialNo: line.get('product').get('digiSerialNo'),
    currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
    salesRep: order.get('salesRepresentative'),
    docNo: order.get('documentNo'),
    prodId: line.get('product').get('id'),
    isActivation: true,
    date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
    lineNo: line.get('id'),
    txnId: txn_id
  }, function (data) {
    OB.UTIL.showLoading(false);
    if (data && !data.exception && data.eResponseCode === '0') {
      line.get('product').set('custqcDigiApiResponse', true);
      //success response from EPAY
      line.get('product').set('epayResponse', data.eResponse);
      line.get('product').set('HTTPRequest', data.HTTPRequest);
      line.get('product').set('HTTPResponse', data.HTTPResponse);
      order.save();
      lineCount++;
      checkActivationSuccessHookForSales(line, lineCount);
      callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
    } else if (data.exception) {
      args.cancelOperation = true;
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), data.exception.message);
      line.get('product').set('custqcDigiApiResponse', false);
      order.save();
      line.set('epayError', data.exception.message);
      lineCount++;
      checkActivationSuccessHookForSales(line, lineCount);
      callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
    }
  }, function (error) {
    pos_date = new Date();
    var is_cancel_success = false;
    OB.UTIL.EPAY.call_cancel_API_Deactivation(args, txn_id, pos_date, line, function () {
      if (args.is_cancel_api_success) {
        enyo.$.scrim.hide();
        args.cancelOperation = true;
        line.get('product').set('custqcDigiApiResponse', false);
        order.save();
        lineCount++;
        checkActivationSuccessHookForSales(line, lineCount);
        callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
      } else {
        var is_cancel_success = false;
        OB.UTIL.EPAY.call_cancel_API_Deactivation(args, txn_id, pos_date, line, function () {
          if (args.is_cancel_api_success) {
            enyo.$.scrim.hide();
            args.cancelOperation = true;
            line.get('product').set('custqcDigiApiResponse', false);
            order.save();
            lineCount++;
            checkActivationSuccessHookForSales(line, lineCount);
            callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
          } else {
            var is_cancel_success = false;
            OB.UTIL.EPAY.call_cancel_API_Deactivation(args, txn_id, pos_date, line, function () {
              if (args.is_cancel_api_success) {
                enyo.$.scrim.hide();
                args.cancelOperation = true;
                line.get('product').set('custqcDigiApiResponse', false);
                order.save();
                lineCount++;
                checkActivationSuccessHookForSales(line, lineCount);
                callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
              } else {
                enyo.$.scrim.hide();
                args.cancelOperation = true;
                line.get('product').set('custqcDigiApiResponse', false);
                order.save();
                lineCount++;
                if (!OB.UTIL.isNullOrUndefined(args.recursiveError)) {
                  line.set('epayError', args.recursiveError);
                } else {
                  line.set('epayError', 'Time Out');
                }
                checkActivationSuccessHookForSales(line, lineCount);
                callAPIForSales(args, order, lines, lineCount, checkActivationSuccessHookForSales, callbacks);
              }
            });
          }
        });
      }
    });
  });
};

OB.UTIL.EPAY.call_cancelAPI_for_return = function (args, txn_id, pos_date, order, line, epayResponse, callback) {
  var processEPAYPINCancel = new OB.DS.Process('com.promantia.sharaf.epay.process.PINPrintCancellation');
  OB.UTIL.showLoading(true);
  var is_api_cancel_success = false;
  processEPAYPINCancel.exec({

    upc: line.get('product').get('uPCEAN'),
    txnRef: epayResponse.TXID,
    amount: line.get('price'),
    currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
    salesRep: order.get('salesRepresentative'),
    docNo: order.get('documentNo'),
    prodId: line.get('product').get('id'),
    isPinCancel: true,
    digitalProductType: line.get('product').get('digiProductType'),
    date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
    lineNo: line.get('id'),
    txnId: txn_id,
    esdResponse: JSON.parse(line.get('cpseEpayresponse'))
  }, function (data) {
    OB.UTIL.showLoading(false);
    if (data && !data.exception && data.eResponseCode === '0') {
      args.is_cancel_api_success = true;
      callback();
    } else if (data.exception) {
      args.is_cancel_api_success = false;
      //OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), 'Time Out');
      callback();
    }
  }, function (error) {
    args.is_cancel_api_success = false;
    args.recursiveError = error.exception.message;
    callback();
  });
};

OB.UTIL.EPAY.call_cancelAPI_Deactivation_for_return = function (args, txn_id, pos_date, order, line, epayResponse, callback) {
  var processEPAYPOSDeactivation = new OB.DS.Process('com.promantia.sharaf.epay.process.POSDeactivation');
  OB.UTIL.showLoading(true);
  var is_api_cancel_success = false;
  processEPAYPOSDeactivation.exec({

    upc: line.get('product').get('uPCEAN'),
    txnRef: epayResponse.TXID,
    amount: line.get('price'),
    serialNo: line.get('cpseEpaySerialno'),
    currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
    salesRep: order.get('salesRepresentative'),
    docNo: order.get('documentNo'),
    prodId: line.get('product').get('id'),
    isDeactivation: true,
    date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
    lineNo: line.get('id'),
    txnId: txn_id

  }, function (data) {
    OB.UTIL.showLoading(false);
    if (data && !data.exception && data.eResponseCode === '0') {
      args.is_cancel_api_success = true;
      callback();
    } else if (data.exception) {
      args.is_cancel_api_success = false;
      //OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), 'Time Out');
      callback();
    }

  }, function (error) {
    args.is_cancel_api_success = false;
    args.recursiveError = error.exception.message;
    callback();
  });
};

OB.UTIL.EPAY.call_cancelAPI_SubscribeActivationCancel_for_return = function (args, txn_id, pos_date, order, line, epayResponse, callback) {
  var processSubscribeActivationCancel = new OB.DS.Process('com.promantia.sharaf.epay.process.SubscribeActivationCancellation');
  OB.UTIL.showLoading(true);
  var is_api_cancel_success = false;
  processSubscribeActivationCancel.exec({

     upc: line.get('product').get('uPCEAN'),
     txnRef: epayResponse.TXID,
     amount: line.get('price'),
     serialNo: line.get('cpseEpaySerialno'),
     currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
     salesRep: order.get('salesRepresentative'),
     docNo: order.get('documentNo'),
     prodId: line.get('product').get('id'),
     isSubscribeActivationCancel: true,
     digitalProductType: line.get('product').get('digiProductType'),
     date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
     lineNo: line.get('id'),
     txnId: txn_id

  }, function (data) {
    OB.UTIL.showLoading(false);
    if (data && !data.exception && data.eResponseCode === '0') {
      args.is_cancel_api_success = true;
      callback();
    } else if (data.exception) {
      args.is_cancel_api_success = false;
      //OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), 'Time Out');
      callback();
    }

  }, function (error) {
    args.is_cancel_api_success = false;
    args.recursiveError = error.exception.message;
    callback();
  });
};

OB.UTIL.EPAY.call_cancelAPI_SubscribePINPRINTCancel_for_return = function (args, txn_id, pos_date, order, line, epayResponse, callback) {
  var processSubscribePINPRINTCancel = new OB.DS.Process('com.promantia.sharaf.epay.process.SubscribePINPRINTCancellation');
  OB.UTIL.showLoading(true);
  var is_api_cancel_success = false;
  processSubscribePINPRINTCancel.exec({

     upc: line.get('product').get('uPCEAN'),
     txnRef: epayResponse.TXID,
     amount: line.get('price'),
     currency: OB.MobileApp.model.get('currency').cpseEpayisocode,
     salesRep: order.get('salesRepresentative'),
     docNo: order.get('documentNo'),
     prodId: line.get('product').get('id'),
     isSubscribePINPRINTCancel: true,
     digitalProductType: line.get('product').get('digiProductType'),
     date: moment(pos_date).format('YYYY-MM-DD HH:mm:ss'),
     lineNo: line.get('id'),
     txnId: txn_id

  }, function (data) {
    OB.UTIL.showLoading(false);
    if (data && !data.exception && data.eResponseCode === '0') {
      args.is_cancel_api_success = true;
      callback();
    } else if (data.exception) {
      args.is_cancel_api_success = false;
      //OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), 'Time Out');
      callback();
    }

  }, function (error) {
    args.is_cancel_api_success = false;
    args.recursiveError = error.exception.message;
    callback();
  });
};