OB.Model.Product.addProperties([
{
  name: 'digiProductType',
  column: 'digiProductType',
  primaryKey: false,
  filter: false,
  type: 'TEXT'
},
{
  name: 'redirectionDivisionId',
  column: 'redirectionDivisionId',
  primaryKey: false,
  filter: false,
  type: 'TEXT'
}
]);