/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB */

(function() {
  var digitalProduct = OB.Data.ExtensibleModel.extend({
    modelName: 'CPSE_DigitalProducts',
    tableName: 'CPSE_DigitalProducts',
    entityName: 'CPSE_DigitalProducts',
    local: true
  });
  digitalProduct.addProperties([
    {
      name: 'id',
      column: 'id',
      primaryKey: true,
      type: 'TEXT'
    },
    {
      name: 'upc',
      column: 'upc',
      type: 'TEXT'
    },
    {
      name: 'txnRef',
      column: 'txnRef',
      type: 'TEXT'
    },
    {
      name: 'amount',
      column: 'amount',
      type: 'TEXT'
    },
    {
      name: 'currency',
      column: 'currency',
      type: 'TEXT'
    },
    {
      name: 'salesRep',
      column: 'salesRep',
      type: 'TEXT'
    },
    {
      name: 'docNo',
      column: 'docNo',
      type: 'TEXT'
    },
    {
      name: 'prodId',
      column: 'prodId',
      type: 'TEXT'
    },
    {
      name: 'isPinCancel',
      column: 'isPinCancel',
      type: 'TEXT'
    },
    {
      name: 'digitalProductType',
      column: 'digitalProductType',
      type: 'TEXT'
    },
    {
      name: 'date',
      column: 'date',
      type: 'TEXT'
    },
    {
      name: 'lineNo',
      column: 'lineNo',
      type: 'TEXT'
    },
    {
      name: 'txnId',
      column: 'txnId',
      type: 'TEXT'
    },
    {
      name: 'esdResponse',
      column: 'esdResponse',
      type: 'TEXT'
    },
    {
      name: 'isDeactivation',
      column: 'isDeactivation',
      type: 'TEXT'
    },
    {
      name: 'serialNo',
      column: 'serialNo',
      type: 'TEXT'
    },
    {
      name: 'organization',
      column: 'organization',
      type: 'TEXT'
    },
    {
      name: 'pos',
      column: 'pos',
      type: 'TEXT'
    }
  ]);
  OB.Data.Registry.registerModel(digitalProduct);
  OB.MobileApp.model.get('dataSyncModels').push({
      model: OB.Model.CPSE_DigitalProducts,
      criteria: {},
      className: 'com.promantia.sharaf.epay.DigitalProductLoader'
  });
})();
