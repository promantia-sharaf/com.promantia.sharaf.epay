package com.promantia.sharaf.epay.process;

import java.util.Date;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.mobile.core.process.JSONProcessSimple;
import org.openbravo.service.json.JsonConstants;

import com.promantia.sharaf.epay.webservices.EPayIntegrationUtility;

public class SubscribeActivationCancellation extends JSONProcessSimple {

	private static Logger log = Logger.getLogger(SubscribeActivationCancellation.class);
	
  @Override
  public JSONObject exec(JSONObject jsonsent) {
    JSONObject retJSON = new JSONObject();
    JSONObject jsonFromPOS = null;
    String xml = null;
    String res = null;
    Date txnDate = new Date();
    String txnID=null;
    try {
      jsonFromPOS = new JSONObject(jsonsent.toString());
      JSONObject epayConfigError = new JSONObject();
      OBContext.setAdminMode(true);
      txnID = jsonFromPOS.getString("txnId");
      log.info("Transaction Id  "+txnID);
      
      // Fetch configurations...
      JSONObject epayConfig = EPayIntegrationUtility.fetchEpayConfigurations(jsonFromPOS);

      // ...if configurations missing throw errors
      epayConfigError = EPayIntegrationUtility.checkIfConfigMissing(epayConfig);

      if (epayConfigError.length() != 0) {
        retJSON.put(JsonConstants.RESPONSE_DATA, epayConfigError);
        retJSON.put(JsonConstants.RESPONSE_ERRORMESSAGE, epayConfigError.getString("errMsg"));
        retJSON.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_FAILURE);
        return retJSON;
      } else {
        // Invoking EPAY Server
        JSONObject epayResponse = EPayIntegrationUtility.invokeEpay(epayConfig, jsonFromPOS,
            txnDate);
        if (epayResponse.has("HTTPRequest")) {
          xml = epayResponse.getString("HTTPRequest");
        }

        if (epayResponse.has("HTTPResponse")) {
          res = epayResponse.getString("HTTPResponse");
          // save EPAY details in EPAY transaction window
          //EPayIntegrationUtility.saveEpayData(epayResponse, jsonFromPOS, txnDate);

          // Format the response back to POS
          retJSON = EPayIntegrationUtility.processResponse(epayResponse);
        }
      }

    } catch (JSONException e) {
      EPayIntegrationUtility.saveEpayErrorData(jsonFromPOS, xml, res, txnDate,txnID, e);
      throw new OBException("JSON error in Subscribe Activation Cancellation: " + e);
    } catch (Exception e) {
      EPayIntegrationUtility.saveEpayErrorData(jsonFromPOS, xml, res, txnDate, txnID,e);
      throw new OBException("Error in Subscribe Activation Cancellation" + e);
    } finally {
      OBContext.restorePreviousMode();
    }
    return retJSON;
  }

}
