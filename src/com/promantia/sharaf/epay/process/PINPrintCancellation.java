package com.promantia.sharaf.epay.process;

import java.util.Date;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.mobile.core.process.JSONProcessSimple;
import org.openbravo.service.json.JsonConstants;

import com.promantia.sharaf.epay.webservices.EPayIntegrationUtility;
import com.promantia.sharaf.epay.webservices.ESDIntegrationUtility;
import org.apache.log4j.Logger;

public class PINPrintCancellation extends JSONProcessSimple {
  
  private static Logger log = Logger.getLogger(PINPrintCancellation.class);
  
  @Override
  public JSONObject exec(JSONObject jsonsent) {
      
    JSONObject retJSON = new JSONObject();
    JSONObject jsonFromPOS = null;
    String xml = null;
    String res = null;
    Date txnDate = new Date();
    JSONObject epayConfig = new JSONObject();
    String digitalProductType = null;
    String txnID = null;
    try {
      jsonFromPOS = new JSONObject(jsonsent.toString());
      JSONObject epayConfigError = new JSONObject();
      OBContext.setAdminMode(true);
      txnID = jsonFromPOS.getString("txnId");
      log.info("Transaction Id  "+txnID);
      
      if (jsonFromPOS.has("digitalProductType")
          && jsonFromPOS.getString("digitalProductType").equals("E")) {
        // Fetch configurations...
        epayConfig = ESDIntegrationUtility.fetchESDConfigurations(jsonFromPOS);
        digitalProductType = jsonFromPOS.getString("digitalProductType");
      } else {
        epayConfig = EPayIntegrationUtility.fetchEpayConfigurations(jsonFromPOS);
        // ...if configurations missing throw errors
        epayConfigError = EPayIntegrationUtility.checkIfConfigMissing(epayConfig);
        digitalProductType = jsonFromPOS.getString("digitalProductType");
      }

      if (epayConfigError.length() != 0) {
        retJSON.put(JsonConstants.RESPONSE_DATA, epayConfigError);
        retJSON.put(JsonConstants.RESPONSE_ERRORMESSAGE, epayConfigError.getString("errMsg"));
        retJSON.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_FAILURE);
        return retJSON;
      } else {
        if (jsonFromPOS.has("digitalProductType")
            && jsonFromPOS.getString("digitalProductType").equals("P")) {
          // Invoking EPAY Server
          JSONObject epayResponse = EPayIntegrationUtility.invokeEpay(epayConfig, jsonFromPOS,
              txnDate);
          if (epayResponse.has("HTTPRequest")) {
            xml = epayResponse.getString("HTTPRequest");
          }

          if (epayResponse.has("HTTPResponse")) {
            res = epayResponse.getString("HTTPResponse");
            // save EPAY details in EPAY transaction window
            //EPayIntegrationUtility.saveEpayData(epayResponse, jsonFromPOS, txnDate);

            // Format the response back to POS
            retJSON = EPayIntegrationUtility.processResponse(epayResponse);

          }
        } else {
          epayConfig.put("url", epayConfig.getString("returnToken"));
          epayConfig.put("urlMethod", "POST");

          JSONObject epayResponse = ESDIntegrationUtility.invokeESD(epayConfig, null, jsonFromPOS,
              txnDate);
          if (epayResponse.has("HTTPRequest")) {
            xml = epayResponse.getString("HTTPRequest");
          }

          if (epayResponse != null) {
            // Format the response back to POS
            retJSON = ESDIntegrationUtility.processResponse(epayResponse);

           // if (!epayResponse.has("esdError") && xml != null)
             // ESDIntegrationUtility.saveESDData(epayResponse, jsonFromPOS, txnDate);
          }
        }
      }
    } catch (JSONException e) {
      if (digitalProductType != null && digitalProductType.equals("P")) {
        EPayIntegrationUtility.saveEpayErrorData(jsonFromPOS, xml, res, txnDate,txnID, e);
      } else if (digitalProductType != null && digitalProductType.equals("E")) {
        ESDIntegrationUtility.saveESDErrorData(jsonFromPOS, xml, res, txnDate, e);
      }
      throw new OBException("JSON error in Epay Pin Cancellation: " + e);
    } catch (Exception e) {
      if (digitalProductType != null && digitalProductType.equals("P")) {
        EPayIntegrationUtility.saveEpayErrorData(jsonFromPOS, xml, res, txnDate,txnID, e);
      } else if (digitalProductType != null && digitalProductType.equals("E")) {
        ESDIntegrationUtility.saveESDErrorData(jsonFromPOS, xml, res, txnDate, e);
      }
      throw new OBException("Error in Epay Pin Cancellation: " + e);
    } finally {
      OBContext.restorePreviousMode();
    }
    return retJSON;
  }

}
