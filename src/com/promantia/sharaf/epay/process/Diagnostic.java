package com.promantia.sharaf.epay.process;

import java.util.Date;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.mobile.core.process.JSONProcessSimple;
import org.openbravo.service.json.JsonConstants;

import com.promantia.sharaf.epay.webservices.EPayIntegrationUtility;

public class Diagnostic extends JSONProcessSimple {

  private static Logger log = Logger.getLogger(Diagnostic.class);

  @Override
  public JSONObject exec(JSONObject jsonsent) throws JSONException, ServletException {
    JSONObject retJSON = new JSONObject();
    JSONObject jsonFromPOS = new JSONObject(jsonsent.toString());
    JSONObject epayConfigError = new JSONObject();
    Date txnDate = new Date();

    try {
      OBContext.setAdminMode(true);
      // Fetch configurations...
      JSONObject epayConfig = EPayIntegrationUtility.fetchEpayConfigurations(jsonFromPOS);

      // if diagnostic then dont throw upc/ean missing error
      if (jsonFromPOS.has("isDiagnostic")) {
        epayConfig.put("isDiagnostic", true);
      }

      // ...if configurations missing throw errors
      epayConfigError = EPayIntegrationUtility.checkIfConfigMissing(epayConfig);

      if (epayConfigError.length() != 0) {
        retJSON.put(JsonConstants.RESPONSE_DATA, epayConfigError);
        retJSON.put(JsonConstants.RESPONSE_ERRORMESSAGE, epayConfigError.getString("errMsg"));
        retJSON.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_FAILURE);
        return retJSON;
      } else {
        // Invoking EPAY Server
        JSONObject epayResponse = EPayIntegrationUtility.invokeEpay(epayConfig, jsonFromPOS,
            txnDate);

        retJSON = EPayIntegrationUtility.processResponse(epayResponse);
      }

    } catch (Exception e) {
      log.error("Error in Epay Diagnostic " + e.getMessage());
      e.printStackTrace();
    } finally {
      OBContext.restorePreviousMode();
    }
    return retJSON;
  }
}
