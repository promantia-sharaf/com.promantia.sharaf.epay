package com.promantia.sharaf.epay.process;

import java.util.Date;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.mobile.core.process.JSONProcessSimple;

import com.promantia.sharaf.epay.webservices.ESDIntegrationUtility;

public class ESDValidatePartner extends JSONProcessSimple {

  private static Logger log = Logger.getLogger(ESDValidatePartner.class);

  @Override
  public JSONObject exec(JSONObject jsonsent) throws JSONException, ServletException {
    JSONObject retJSON = new JSONObject();
    JSONObject jsonFromPOS = new JSONObject(jsonsent.toString());
    Date txnDate = new Date();

    try {
      OBContext.setAdminMode(true);
      // Fetch configurations...
      JSONObject esdConfig = ESDIntegrationUtility.fetchESDConfigurations(jsonFromPOS);
      esdConfig.put("url", esdConfig.getString("validatePartner"));
      esdConfig.put("urlMethod", "POST");

      JSONObject requestBody = new JSONObject();
      requestBody.put("PartnerId", esdConfig.getString("resellerId"));
      requestBody.put("StoreId", esdConfig.getString("storeId"));
      requestBody.put("POSId", esdConfig.getString("posTermicalSearchKey"));

      JSONObject esdResponse = ESDIntegrationUtility.invokeESD(esdConfig, requestBody, jsonFromPOS,
          txnDate);

      retJSON = ESDIntegrationUtility.processResponse(esdResponse);

    } catch (Exception e) {
      log.error("Error in ESD Validate Partner ", e);
    } finally {
      OBContext.restorePreviousMode();
    }
    return retJSON;
  }
}
