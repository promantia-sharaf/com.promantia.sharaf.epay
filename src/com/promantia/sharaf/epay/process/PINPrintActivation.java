package com.promantia.sharaf.epay.process;

import java.util.Date;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.mobile.core.process.JSONProcessSimple;
import org.openbravo.service.json.JsonConstants;

import com.promantia.sharaf.epay.webservices.EPayIntegrationUtility;
import com.promantia.sharaf.epay.webservices.ESDIntegrationUtility;

public class PINPrintActivation extends JSONProcessSimple {

  private static Logger log = Logger.getLogger(PINPrintActivation.class);

  @Override
  public JSONObject exec(JSONObject jsonsent) {
    JSONObject retJSON = new JSONObject();
    JSONObject jsonFromPOS = null;
    String xml = null;
    String res = null;
    Date txnDate = new Date();
    String digitalProductType = null;
    String txnID = null;
    try {
      jsonFromPOS = new JSONObject(jsonsent.toString());
      JSONObject epayConfigError = new JSONObject();
      OBContext.setAdminMode(true);
      txnID = jsonFromPOS.getString("txnId");
      log.info("Transaction id  "+txnID);
      
      // Fetch configurations...
      JSONObject epayConfig = new JSONObject();
      if (jsonFromPOS.has("digitalProductType")
          && jsonFromPOS.getString("digitalProductType").equals("E")) {
        epayConfig = ESDIntegrationUtility.fetchESDConfigurations(jsonFromPOS);
        epayConfigError = ESDIntegrationUtility.checkIfConfigMissing(epayConfig);
        digitalProductType = jsonFromPOS.getString("digitalProductType");
      } else {
        epayConfig = EPayIntegrationUtility.fetchEpayConfigurations(jsonFromPOS);
        epayConfigError = EPayIntegrationUtility.checkIfConfigMissing(epayConfig);
        digitalProductType = jsonFromPOS.getString("digitalProductType");
      }

      // ...if configurations missing throw errors
      if (epayConfigError.length() != 0) {
        retJSON.put(JsonConstants.RESPONSE_DATA, epayConfigError);
        retJSON.put(JsonConstants.RESPONSE_ERRORMESSAGE, epayConfigError.getString("errMsg"));
        retJSON.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_FAILURE);
        return retJSON;
      } else {
        if (jsonFromPOS.has("digitalProductType")
            && jsonFromPOS.getString("digitalProductType").equals("P")) {
          // Invoking EPAY Server
          JSONObject epayResponse = EPayIntegrationUtility.invokeEpay(epayConfig, jsonFromPOS,
              txnDate);
          if (epayResponse.has("HTTPRequest")) {
            xml = epayResponse.getString("HTTPRequest");
          }

          if (epayResponse.has("HTTPResponse")) {
            res = epayResponse.getString("HTTPResponse");
            // save EPAY details in EPAY transaction window
            //EPayIntegrationUtility.saveEpayData(epayResponse, jsonFromPOS, txnDate);

            // Format the response back to POS
            retJSON = EPayIntegrationUtility.processResponse(epayResponse);
          }
        } else {
          epayConfig.put("url", epayConfig.getString("createOrder"));
          epayConfig.put("urlMethod", "POST");

          JSONObject epayResponse = ESDIntegrationUtility.invokeESD(epayConfig, null, jsonFromPOS,
              txnDate);
          if (epayResponse.has("HTTPRequest")) {
            xml = epayResponse.getString("HTTPRequest");
          }

          if (epayResponse != null) {
            // Format the response back to POS
            retJSON = ESDIntegrationUtility.processResponse(epayResponse);

            //if (!epayResponse.has("esdError") && xml != null)
            //  ESDIntegrationUtility.saveESDData(epayResponse, jsonFromPOS, txnDate);

          }
        }
      }
    } catch (JSONException e) {
      if (digitalProductType != null && digitalProductType.equals("P")) {
        EPayIntegrationUtility.saveEpayErrorData(jsonFromPOS, xml, res, txnDate,txnID, e);
      } else if (digitalProductType != null && digitalProductType.equals("E")) {
        ESDIntegrationUtility.saveESDErrorData(jsonFromPOS, xml, res, txnDate, e);
      }
      throw new OBException("JSON error in Epay/ESD Pin Print: " + e);
    } catch (Exception e) {
      if (digitalProductType != null && digitalProductType.equals("P")) {
        EPayIntegrationUtility.saveEpayErrorData(jsonFromPOS, xml, res, txnDate,txnID, e);
      } else if (digitalProductType != null && digitalProductType.equals("E")) {
        ESDIntegrationUtility.saveESDErrorData(jsonFromPOS, xml, res, txnDate, e);
      }
      throw new OBException("Error in Epay/ESD Pin Print: " + e);
    } finally {
      OBContext.restorePreviousMode();
    }
    return retJSON;
  }

}
