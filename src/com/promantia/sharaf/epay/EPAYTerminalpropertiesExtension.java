package com.promantia.sharaf.epay;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.dal.core.OBContext;
import org.openbravo.mobile.core.model.HQLProperty;
import org.openbravo.mobile.core.model.ModelExtension;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.retail.posterminal.POSUtils;
import org.openbravo.retail.posterminal.PrintTemplate;
import org.openbravo.retail.posterminal.PrintTemplateSubrep;
import org.openbravo.retail.posterminal.term.Terminal;

@Qualifier(Terminal.terminalPropertyExtension)
public class EPAYTerminalpropertiesExtension extends ModelExtension {

  private static Logger log = Logger.getLogger(EPAYTerminalpropertiesExtension.class);

  @Override
  public List<HQLProperty> getHQLProperties(Object params) {

    final ArrayList<HQLProperty> list = new ArrayList<HQLProperty>();

    addTemplateProperty(Organization.PROPERTY_CPSEEPAYINVTEMPLATE, "printEpayTicketTemplate", list);
    addTemplateProperty(Organization.PROPERTY_CPSEEPAYCLOSEDTEMPLATE, "printEpayClosedTemplate",
        list);
    addTemplateProperty(Organization.PROPERTY_CPSEESDCLOSEDTEMPLATE, "printESDClosedTemplate",
        list);
    addTemplateProperty(Organization.PROPERTY_CPSEESDINVTEMPLATE, "printESDTicketTemplate", list);
    return list;
  }

  private void addTemplateProperty(final String propertyName, final String alias,
      final List<HQLProperty> list) {
    try {
      OBContext.setAdminMode(false);
      final PrintTemplate value = (PrintTemplate) POSUtils
          .getPropertyInOrgTree(OBContext.getOBContext().getCurrentOrganization(), propertyName);
      if (value != null) {
        list.add(new HQLProperty("'" + value.getTemplatePath() + "'", alias));
        list.add(new HQLProperty("'" + value.isPdf() + "'", alias + "IsPdf"));
        if (value.isPdf()) {
          list.add(new HQLProperty("'" + value.getPrinter() + "'", alias + "Printer"));
        }
        int i = 0;
        for (final PrintTemplateSubrep subrep : value.getOBPOSPrintTemplateSubrepList()) {
          list.add(new HQLProperty("'" + subrep.getTemplatePath() + "'", alias + "Subrep" + i));
          i++;
        }
      }
    } catch (final Exception e) {
      log.error("Error getting property " + propertyName, e);
    } finally {
      OBContext.restorePreviousMode();
    }

  }

}
