package com.promantia.sharaf.epay;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.mobile.core.process.DataSynchronizationProcess.DataSynchronization;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.retail.posterminal.OBPOSApplications;
import org.openbravo.retail.posterminal.POSDataSynchronizationProcess;
import org.openbravo.service.json.JsonConstants;

import com.promantia.sharaf.epay.webservices.EPAYRequest;
import com.promantia.sharaf.epay.webservices.EPayIntegrationUtility;
import com.promantia.sharaf.epay.webservices.ESDIntegrationUtility;

@DataSynchronization(entity = "CPSE_DigitalProducts")
public class DigitalProductLoader extends POSDataSynchronizationProcess {

	private static Logger log = Logger.getLogger(DigitalProductLoader.class);
	  
  @Override
  public JSONObject saveRecord(JSONObject jsonFromPOS) throws Exception {
    String requestType = null;
    String ean = null;
    String amount = null;
    String currency = null;
    String docNo = null;
    String sapCode = null;

    Organization org = OBDal.getInstance().get(Organization.class,
        jsonFromPOS.getString("organization"));
    if (org.getSapobmpFiliale() != null) {
      sapCode = org.getSapobmpFiliale();
    }

    jsonFromPOS.put("client", OBContext.getOBContext().getCurrentClient().getId());

    if (jsonFromPOS.has("isPinCancel") && jsonFromPOS.getString("isPinCancel").equals("true")) {
      requestType = "CANCEL";
    }

    if (jsonFromPOS.has("isDeactivation")
        && jsonFromPOS.getString("isDeactivation").equals("true")) {
      requestType = "DEACTIVATE";
    }

    if (jsonFromPOS.has("upc")) {
      ean = jsonFromPOS.getString("upc");
    }

    if (jsonFromPOS.has("amount")) {
      amount = jsonFromPOS.getString("amount");
    }

    if (jsonFromPOS.has("currency")) {
      currency = jsonFromPOS.getString("currency");

      BigDecimal amt = BigDecimal.valueOf(jsonFromPOS.getDouble("amount"));
      if (currency != null && !currency.isEmpty()) {
        OBCriteria<Currency> currencyCriteria = OBDal.getInstance().createCriteria(Currency.class);
        currencyCriteria.add(Restrictions.eq(Currency.PROPERTY_CPSEEPAYISOCODE,
            BigDecimal.valueOf(Long.parseLong(currency))));

        try {
          if (currencyCriteria.list().get(0) != null) {
            Currency currencyConfig = currencyCriteria.list().get(0);
            for (long i = 0L; i < currencyConfig.getStandardPrecision(); i++) {
              amt = amt.multiply(BigDecimal.TEN);
            }
          } else {
            amt = amt.multiply(BigDecimal.valueOf(100));
          }
        } catch (Exception e2) {
          amt = amt.multiply(BigDecimal.valueOf(100));
        }
      } else {
        amt = amt.multiply(BigDecimal.valueOf(100));
      }
      amount = String.valueOf(amt);
      if (amount.contains(".")) {
        amount = amount.split("\\.")[0];
      }
    }

    if (jsonFromPOS.has("docNo")) {
      docNo = jsonFromPOS.getString("docNo");
    }

    String date = jsonFromPOS.getString("date");
    String txnID = jsonFromPOS.getString("txnId");

    OBPOSApplications pos = OBDal.getInstance().get(OBPOSApplications.class,
        jsonFromPOS.getString("pos"));

    String userName = pos.getCpseEpayusername();
    String password = pos.getCpseEpaypassword();
    String terminalID = pos.getCpseEpayterminalid();
    String charsPerLine = "42";

    if (!jsonFromPOS.getString("digitalProductType").equals("E")) {
      JSONObject xmlReqJSON = new JSONObject();
      xmlReqJSON.put("requestType", requestType);
      xmlReqJSON.put("userName", userName);
      xmlReqJSON.put("password", password);
      xmlReqJSON.put("terminalID", terminalID);
      xmlReqJSON.put("amount", amount);
      xmlReqJSON.put("date", date);
      xmlReqJSON.put("txnID", txnID);
      xmlReqJSON.put("sapCode", sapCode);
      xmlReqJSON.put("ean", ean);
      xmlReqJSON.put("currency", currency);
      xmlReqJSON.put("charsPerLine", charsPerLine);
      xmlReqJSON.put("docNo", docNo);

      String xml = new String(); // XML request based on request type if
      if (requestType.equalsIgnoreCase("CANCEL")) {
        xmlReqJSON.put("txnRef", jsonFromPOS.getString("txnRef"));
        jsonFromPOS.remove("isDeactivation");
        if (jsonFromPOS.has("orignalTerminalId")) {
          xmlReqJSON.put("terminalID", jsonFromPOS.getString("orignalTerminalId"));
        }
        xml = EPAYRequest.generatePINCancelXMLRequest(xmlReqJSON);
      } else if (requestType.equalsIgnoreCase("DEACTIVATE")) {
        xmlReqJSON.put("serialNo", jsonFromPOS.getString("serialNo"));
        jsonFromPOS.remove("isPinCancel");
        if (jsonFromPOS.has("orignalTerminalId")) {
          xmlReqJSON.put("terminalID", jsonFromPOS.getString("orignalTerminalId"));
        }
        xml = EPAYRequest.generateDeActivationXMLRequest(xmlReqJSON);
      }

      log.info("Epay Transaction Id "+txnID);
      
      EPayIntegrationUtility.saveEpayErrorData(jsonFromPOS, xml, null, new Date(),txnID,
          new Exception("Deactivated/deleted line in offline mode"));

    } else {
      JSONObject esdConfig = ESDIntegrationUtility.fetchESDConfigurations(jsonFromPOS);
      JSONObject req = ESDIntegrationUtility.createPinPrintRequestBody(esdConfig, jsonFromPOS,
          new Date());
      ESDIntegrationUtility.saveESDErrorData(jsonFromPOS, req.toString(), null, new Date(),
          new Exception("Deactivated/deleted line in offline mode"));
    }
    JSONObject jsonResponse = new JSONObject();
    jsonResponse.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_SUCCESS);
    jsonResponse.put("result", "0");
    return jsonResponse;
  }

}
