package com.promantia.sharaf.epay;

import java.util.ArrayList;
import java.util.List;

import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.mobile.core.model.HQLProperty;
import org.openbravo.mobile.core.model.ModelExtension;
import org.openbravo.retail.posterminal.PaidReceipts;

@Qualifier(PaidReceipts.paidReceiptsLinesPropertyExtension)
public class EPAYPaidReceiptLinesProperties extends ModelExtension {

  @Override
  public List<HQLProperty> getHQLProperties(Object params) {
    ArrayList<HQLProperty> list = new ArrayList<HQLProperty>() {
      private static final long serialVersionUID = 1L;
      {
        add(new HQLProperty("ordLine.cpseEpayresponse", "cpseEpayresponse"));
        add(new HQLProperty("ordLine.cpseEpaySerialno", "cpseEpaySerialno"));
        add(new HQLProperty("ordLine.cpseEpayrequest", "cpseEpayrequest"));
      }
    };
    return list;
  }

}
