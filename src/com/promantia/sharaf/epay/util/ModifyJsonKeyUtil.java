package com.promantia.sharaf.epay.util;

import java.util.Iterator;

import net.sf.json.JSONObject;

@SuppressWarnings("rawtypes")
public class ModifyJsonKeyUtil {

  public static final String STRING_KEY = "String";
  public static final String Long_KEY = "Long";
  public static final String Integer_KEY = "Integer";
  public static final String JSONOBJECT_KEY = "JSONObject";
  public static final String JSONARRAY_KEY = "JSONArray";

  public static JSONObject transferJsonKey(JSONObject jsonObject) {
    JSONObject object = new JSONObject();
    Iterator iterator = jsonObject.keys();
    while (iterator.hasNext()) {
      String jsonKey = (String) iterator.next();
      Object valueObject = jsonObject.get(jsonKey);
      jsonKey = jsonKey.toUpperCase();
      if (valueObject.getClass().toString().endsWith(STRING_KEY)
          || valueObject.getClass().toString().endsWith(Long_KEY)
          || valueObject.getClass().toString().endsWith(Integer_KEY)
          || valueObject.getClass().toString().endsWith(JSONARRAY_KEY)) {
        object.accumulate(jsonKey, valueObject);
      } else if (valueObject.getClass().toString().endsWith(JSONOBJECT_KEY)) {
        JSONObject checkObject = JSONObject.fromObject(valueObject);
        // When the value is null, the valueObject is still a JSONObject object, the null is not
        // true, to determine whether it is nullObject
        if (!checkObject.isNullObject()) {
          object.accumulate(jsonKey, transferJsonKey((JSONObject) valueObject));
        } else {
          object.accumulate(jsonKey, null);
        }
      }
    }
    return object;
  }

}