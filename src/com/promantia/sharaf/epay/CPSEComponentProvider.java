package com.promantia.sharaf.epay;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import org.openbravo.client.kernel.BaseComponentProvider;
import org.openbravo.client.kernel.BaseComponentProvider.ComponentResource.ComponentResourceType;
import org.openbravo.client.kernel.Component;
import org.openbravo.client.kernel.ComponentProvider;
import org.openbravo.retail.posterminal.POSUtils;

@ApplicationScoped
@ComponentProvider.Qualifier(CPSEComponentProvider.QUALIFIER)
public class CPSEComponentProvider extends BaseComponentProvider {

  public static final String QUALIFIER = "CPSE_Main";
  public static final String MODULE_JAVA_PACKAGE = "com.promantia.sharaf.epay";

  @Override
  public Component getComponent(String componentId, Map<String, Object> parameters) {
    throw new IllegalArgumentException("Component id " + componentId + " not supported.");
  }

  @Override
  public List<ComponentResource> getGlobalComponentResources() {
    final GlobalResourcesHelper grhelper = new GlobalResourcesHelper();
    grhelper.add("hooks/epayPreAddProductToOrder.js");
    grhelper.add("hooks/epayPreDeleteLine.js");
    grhelper.add("hooks/epayPreDeleteOrder.js");
    grhelper.add("model/epayAddProductProperties.js");
    grhelper.add("components/epayMenu.js");
    grhelper.add("hooks/epayPrePrintHook.js");
    grhelper.add("components/epaySerialNoPopup.js");
    grhelper.add("hooks/epayPrePaymentHook.js");
    grhelper.add("hooks/epayRenderOrderLineHook.js");
    grhelper.add("hooks/epayPostAddVerifiedReturnLines.js");
    grhelper.add("components/epayInvoiceType.js");
    grhelper.add("components/epayPaidReceipt.js");
    grhelper.add("components/esdMenu.js");
    grhelper.add("model/digitalProductModels.js");
    // grhelper.add("hooks/epayPrePrintPaidReceiptHook.js");

    return grhelper.getGlobalResources();
  }

  private class GlobalResourcesHelper {
    private final List<ComponentResource> globalResources = new ArrayList<ComponentResource>();
    private final String prefix = "web/" + MODULE_JAVA_PACKAGE + "/js/";

    public void add(String file) {
      globalResources.add(
          createComponentResource(ComponentResourceType.Static, prefix + file, POSUtils.APP_NAME));
    }

    public List<ComponentResource> getGlobalResources() {
      return globalResources;
    }
  }

}