package com.promantia.sharaf.epay;

import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
import org.openbravo.retail.posterminal.OrderLoaderHook;

public class EPAYOrderLoaderook implements OrderLoaderHook {
  private static final Logger log = Logger.getLogger(EPAYOrderLoaderook.class);

  @Override
  public void exec(JSONObject jsonorder, Order order, ShipmentInOut shipment, Invoice invoice)
      throws Exception {
    try {
      JSONArray orderLines = jsonorder.getJSONArray("lines");
      String orgid = jsonorder.getString("organization");
      CpseEpayTransactions epayTxn = null;
      CpseESDTransactions esdTxn = null;
      int epayAPICount = 0;
      String digiProductType = null;
      String redirectionDivisionId = null;

      log.debug("Invoked orderloaderhook for epay");

      for (int i = 0; i < orderLines.length(); i++) {
        JSONObject line = orderLines.getJSONObject(i);
        if (line.getJSONObject("product").has("digiProductType")
            && line.getJSONObject("product").get("digiProductType") != null
            && (line.getJSONObject("product").get("digiProductType").toString()
                .equalsIgnoreCase("P")
                || line.getJSONObject("product").get("digiProductType").toString()
                    .equalsIgnoreCase("A")
                || line.getJSONObject("product").get("digiProductType").toString()
                    .equalsIgnoreCase("E"))
                || line.getJSONObject("product").get("digiProductType").toString()
            .equalsIgnoreCase("SP")
                || line.getJSONObject("product").get("digiProductType").toString()
            .equalsIgnoreCase("SA")) {
          digiProductType = line.getJSONObject("product").get("digiProductType").toString();
          epayAPICount++;

        }
        if (line.getJSONObject("product").has("redirectionDivisionId")
            && line.getJSONObject("product").get("redirectionDivisionId") != null) {
          redirectionDivisionId = line.getJSONObject("product").get("redirectionDivisionId").toString();
        }
      }

      // Save order line reference in Epay Transactions window
      OBCriteria<CpseEpayTransactions> epayTxnCriteria = OBDal.getInstance()
          .createCriteria(CpseEpayTransactions.class);
      epayTxnCriteria.add(Restrictions.eq(CpseEpayTransactions.PROPERTY_ORGANIZATION, OBDal
          .getInstance().get(org.openbravo.model.common.enterprise.Organization.class, orgid)));
      epayTxnCriteria.addOrder(
          org.hibernate.criterion.Order.desc(CpseEpayTransactions.PROPERTY_TRANSACTIONDATE));
      epayTxnCriteria.setMaxResults(100);

      // Save order line reference in ESD Transactions window
      OBCriteria<CpseESDTransactions> esdTxnCriteria = OBDal.getInstance()
          .createCriteria(CpseESDTransactions.class);
      esdTxnCriteria.add(Restrictions.eq(CpseESDTransactions.PROPERTY_ORGANIZATION, OBDal
          .getInstance().get(org.openbravo.model.common.enterprise.Organization.class, orgid)));
      esdTxnCriteria.addOrder(
          org.hibernate.criterion.Order.desc(CpseESDTransactions.PROPERTY_TRANSACTIONDATE));
      esdTxnCriteria.setMaxResults(100);
      esdTxnCriteria.add(Restrictions.eq(CpseESDTransactions.PROPERTY_DOCUMENTNO,
          jsonorder.getString("documentNo")));

      for (int i = 0; i < orderLines.length(); i++) {
        JSONObject line = orderLines.getJSONObject(i);

        if (line.getJSONObject("product").has("digiProductType")
            && line.getJSONObject("product").get("digiProductType") != null
            && (line.getJSONObject("product").get("digiProductType").toString()
                .equalsIgnoreCase("P")
                || line.getJSONObject("product").get("digiProductType").toString()
                    .equalsIgnoreCase("A")
                || line.getJSONObject("product").get("digiProductType").toString()
                    .equalsIgnoreCase("E")
                || line.getJSONObject("product").get("digiProductType").toString()
                    .equalsIgnoreCase("SA")
                || line.getJSONObject("product").get("digiProductType").toString()
                    .equalsIgnoreCase("SP"))) {
          digiProductType = line.getJSONObject("product").get("digiProductType").toString();
          // For EPAY
          if (digiProductType != null && !digiProductType.equals("E")) {
            List<CpseEpayTransactions> epayTxnList = epayTxnCriteria.list();
            for (int j = epayTxnList.size() - 1; j >= 0; j--) {
              epayTxn = epayTxnList.get(j);
              if (line.get("id").toString()
                  .equalsIgnoreCase(order.getOrderLineList().get(i).getId().toString())) {
                if (epayTxn.getLineReference() == null
                    || epayTxn.getLineReference().toString().isEmpty()) {
                  if (epayTxn.getTransaction() != null) {
                    String[] str = epayTxn.getTransaction().split("_");
                    if (str.length > 1 && line.get("id").toString().equalsIgnoreCase(str[0])) {
                      epayTxn.setLineReference(order.getOrderLineList().get(i));
                      break;
                    }
                  }
                }
              }
            }
          } else {
            // For ESD
            List<CpseESDTransactions> esdTxnList = esdTxnCriteria.list();
            for (int j = esdTxnList.size() - 1; j >= 0; j--) {
              esdTxn = esdTxnList.get(j);
              if (line.get("id").toString()
                  .equalsIgnoreCase(order.getOrderLineList().get(i).getId().toString())) {
                if (esdTxn.getLineReference() == null
                    || esdTxn.getLineReference().toString().isEmpty()) {
                  if (esdTxn.getESDRequest() != null) {
                    JSONObject req = new JSONObject(esdTxn.getESDRequest());
                    JSONObject reqFromPos = new JSONObject(
                        line.getJSONObject("product").getString("HTTPRequest"));
                    if ((req.has("lpono") && reqFromPos.has("lpono")
                        && req.getString("lpono").equals(reqFromPos.getString("lpono"))) || 
                        (req.has("PurchaseOrderId") && reqFromPos.has("PurchaseOrderId")
                        && req.getString("PurchaseOrderId").equals(reqFromPos.getString("PurchaseOrderId")))) {
                      esdTxn.setLineReference(order.getOrderLineList().get(i));
                      break;
                    }
                  }
                }
              }
            }
          }
        }
      }

      for (int i = 0; i < orderLines.length(); i++) {
        JSONObject line = orderLines.getJSONObject(i);

        // Save EPAY request and Epay response in database
        if (line.getJSONObject("product").has("epayResponse")) {
          order.getOrderLineList().get(i)
              .setCpseEpayrequest(line.getJSONObject("product").getString("HTTPRequest"));
          order.getOrderLineList().get(i)
              .setCpseEpayresponse(line.getJSONObject("product").getString("epayResponse"));
          if (line.getJSONObject("product").has("digiSerialNo")) {
            order.getOrderLineList().get(i)
                .setCpseEpaySerialno(line.getJSONObject("product").getString("digiSerialNo"));
          }
          log.debug("successfully saved epayresponse in orderline table");
        }

        if (line.getJSONObject("product").has("invoice_type")) {
          order.getOrderLineList().get(i)
              .setCpseEpayInvoicetype(line.getJSONObject("product").getString("invoice_type"));
        }

        // Save goods shipment line in sales order window ... for reinvoice flow
        if (line.has("returnLineId")) {
          OrderLine returnOrderLine = OBDal.getInstance().get(OrderLine.class,
              line.getString("returnLineId"));

          OBCriteria<ShipmentInOutLine> inOutCriteria = OBDal.getInstance()
              .createCriteria(ShipmentInOutLine.class);
          inOutCriteria
              .add(Restrictions.eq(ShipmentInOutLine.PROPERTY_SALESORDERLINE, returnOrderLine));
          ShipmentInOutLine inOutLine = (ShipmentInOutLine) inOutCriteria.uniqueResult();

          order.getOrderLineList().get(i).setGoodsShipmentLine(inOutLine);
        }

        // Save Original epay request and Original epay response in database... for reinvoice flow
        if (line.getJSONObject("product").has("originalEpaySerialno")) {
          order.getOrderLineList().get(i)
              .setCpseEpaySerialno(line.getJSONObject("product").getString("originalEpaySerialno"));
        }
        if (line.getJSONObject("product").has("originalEpayRequest")) {
          order.getOrderLineList().get(i)
              .setCpseEpayrequest(line.getJSONObject("product").getString("originalEpayRequest"));
        }
        if (line.getJSONObject("product").has("originalEpayResponse")) {
          order.getOrderLineList().get(i)
              .setCpseEpayresponse(line.getJSONObject("product").getString("originalEpayResponse"));
        }

      }
    } catch (Exception e) {
      log.error(e);
    }

  }
}
