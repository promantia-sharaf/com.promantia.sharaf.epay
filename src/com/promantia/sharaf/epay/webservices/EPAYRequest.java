package com.promantia.sharaf.epay.webservices;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class EPAYRequest {
  public static String generatePINPrintXMLRequest(final JSONObject xmlReqJSON) {
    String reqXML = null;
    try {
      reqXML = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> <REQUEST type=\""
          + xmlReqJSON.getString("requestType") + "\"" + ">" + " <USERNAME>"
          + xmlReqJSON.getString("userName") + "</USERNAME>" + " <PASSWORD>"
          + xmlReqJSON.getString("password") + "</PASSWORD>" + " <TERMINALID>"
          + xmlReqJSON.getString("terminalID") + "</TERMINALID>" + " <LOCALDATETIME>"
          + xmlReqJSON.getString("date") + "</LOCALDATETIME>" + " <TXID>"
          + xmlReqJSON.getString("txnID") + "</TXID>" + " <CASHIER>"
          + xmlReqJSON.getString("sapCode") + "</CASHIER>" + " <AMOUNT>"
          + xmlReqJSON.getString("amount") + "</AMOUNT>" + " <CURRENCY>"
          + xmlReqJSON.getString("currency") + "</CURRENCY>" + " <CARD> <EAN>"
          + xmlReqJSON.getString("ean") + "</EAN> </CARD>" + " <RECEIPT> <CHARSPERLINE>"
          + xmlReqJSON.getString("charsPerLine") + "</CHARSPERLINE> </RECEIPT>" + " <COMMENT>"
          + xmlReqJSON.getString("docNo") + "</COMMENT>" + " </REQUEST>";
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return reqXML;
  }

  public static String generatePINCancelXMLRequest(final JSONObject xmlReqJSON) {
    String reqXML = null;
    try {
      reqXML = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> <REQUEST type=\""
          + xmlReqJSON.getString("requestType") + "\"" + ">" + " <USERNAME>"
          + xmlReqJSON.getString("userName") + "</USERNAME>" + " <PASSWORD>"
          + xmlReqJSON.getString("password") + "</PASSWORD>" + " <TERMINALID>"
          + xmlReqJSON.getString("terminalID") + "</TERMINALID>" + " <LOCALDATETIME>"
          + xmlReqJSON.getString("date") + "</LOCALDATETIME>" + " <TXID>"
          + xmlReqJSON.getString("txnID") + "</TXID>" + " <TXREF>" + xmlReqJSON.getString("txnRef")
          + "</TXREF>" + " <CASHIER>" + xmlReqJSON.getString("sapCode") + "</CASHIER>"
          + " <AMOUNT>" + xmlReqJSON.getString("amount") + "</AMOUNT>" + " <CURRENCY>"
          + xmlReqJSON.getString("currency") + "</CURRENCY>" + " <CARD> <EAN>"
          + xmlReqJSON.getString("ean") + "</EAN> </CARD>" + " <RECEIPT> <CHARSPERLINE>"
          + xmlReqJSON.getString("charsPerLine") + "</CHARSPERLINE> </RECEIPT>" + " <COMMENT>"
          + xmlReqJSON.getString("docNo") + "</COMMENT>" + " </REQUEST>";
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return reqXML;
  }

  public static String generateDiagnosticXMLRequest(final JSONObject xmlReqJSON) {
    String reqXML = null;
    try {
      reqXML = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> <REQUEST type=\""
          + xmlReqJSON.getString("requestType") + "\"" + ">" + " <USERNAME>"
          + xmlReqJSON.getString("userName") + "</USERNAME>" + " <PASSWORD>"
          + xmlReqJSON.getString("password") + "</PASSWORD>" + " <TERMINALID>"
          + xmlReqJSON.getString("terminalID") + "</TERMINALID>" + " <LOCALDATETIME>"
          + xmlReqJSON.getString("date") + "</LOCALDATETIME>" + " <TXID>"
          + xmlReqJSON.getString("txnID") + "</TXID>" + " </REQUEST>";
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return reqXML;
  }

  public static String generateActivationXMLRequest(final JSONObject xmlReqJSON) {
    String reqXML = null;
    try {
      reqXML = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> <REQUEST type=\""
          + xmlReqJSON.getString("requestType") + "\"" + ">" + " <USERNAME>"
          + xmlReqJSON.getString("userName") + "</USERNAME>" + " <PASSWORD>"
          + xmlReqJSON.getString("password") + "</PASSWORD>" + " <TERMINALID>"
          + xmlReqJSON.getString("terminalID") + "</TERMINALID>" + " <LOCALDATETIME>"
          + xmlReqJSON.getString("date") + "</LOCALDATETIME>" + " <TXID>"
          + xmlReqJSON.getString("txnID") + "</TXID>" + " <CASHIER>"
          + xmlReqJSON.getString("sapCode") + "</CASHIER>" + " <AMOUNT>"
          + xmlReqJSON.getString("amount") + "</AMOUNT>" + " <CURRENCY>"
          + xmlReqJSON.getString("currency") + "</CURRENCY>" + " <CARD> <PAN>"
          + xmlReqJSON.getString("serialNo") + "</PAN> <EAN>" + xmlReqJSON.getString("ean")
          + "</EAN> </CARD>" + " <RECEIPT> <CHARSPERLINE>" + xmlReqJSON.getString("charsPerLine")
          + "</CHARSPERLINE> </RECEIPT>" + " <COMMENT>" + xmlReqJSON.getString("docNo")
          + "</COMMENT>" + " </REQUEST>";
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return reqXML;
  }

  public static String generateDeActivationXMLRequest(final JSONObject xmlReqJSON) {
    String reqXML = null;
    try {
      reqXML = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> <REQUEST type=\""
          + xmlReqJSON.getString("requestType") + "\"" + ">" + " <USERNAME>"
          + xmlReqJSON.getString("userName") + "</USERNAME>" + " <PASSWORD>"
          + xmlReqJSON.getString("password") + "</PASSWORD>" + " <TERMINALID>"
          + xmlReqJSON.getString("terminalID") + "</TERMINALID>" + " <LOCALDATETIME>"
          + xmlReqJSON.getString("date") + "</LOCALDATETIME>" + " <TXID>"
          + xmlReqJSON.getString("txnID") + "</TXID>" + " <CASHIER>"
          + xmlReqJSON.getString("sapCode") + "</CASHIER>" + " <AMOUNT>"
          + xmlReqJSON.getString("amount") + "</AMOUNT>" + " <CURRENCY>"
          + xmlReqJSON.getString("currency") + "</CURRENCY>" + " <CARD> <PAN>"
          + xmlReqJSON.getString("serialNo") + "</PAN> <EAN>" + xmlReqJSON.getString("ean")
          + "</EAN> </CARD>" + " <RECEIPT> <CHARSPERLINE>" + xmlReqJSON.getString("charsPerLine")
          + "</CHARSPERLINE> </RECEIPT>" + " <COMMENT>" + xmlReqJSON.getString("docNo")
          + "</COMMENT>" + " </REQUEST>";
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return reqXML;
  }
  public static String generateSubscriptionActivationXMLRequest(final JSONObject xmlReqJSON) {
    String reqXML = null;
    try {
      reqXML = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> <REQUEST type=\""
          + xmlReqJSON.getString("requestType") + "\"" + ">" + " <USERNAME>"
          + xmlReqJSON.getString("userName") + "</USERNAME>" + " <PASSWORD>"
          + xmlReqJSON.getString("password") + "</PASSWORD>" + " <TERMINALID>"
          + xmlReqJSON.getString("terminalID") + "</TERMINALID>" + " <LOCALDATETIME>"
          + xmlReqJSON.getString("date") + "</LOCALDATETIME>" + " <TXID>"
          + xmlReqJSON.getString("txnID") + "</TXID>" + " <CASHIER>"
          + xmlReqJSON.getString("sapCode") + "</CASHIER>" + " <AMOUNT>"
          + xmlReqJSON.getString("amount") + "</AMOUNT>" + " <CURRENCY>"
          + xmlReqJSON.getString("currency") + "</CURRENCY>" + " <CARD> <PAN>"
          + xmlReqJSON.getString("serialNo") + "</PAN>" + " <EAN>"          
          + xmlReqJSON.getString("ean") + "</EAN> </CARD>" + " <EXTRADATA> <DATA name=\"CONTRACT\">"
          + xmlReqJSON.getString("contractId") + "</DATA> <DATA name=\"REDIRECTIONDIVISIONID\">"
          + xmlReqJSON.getString("redirectionDivisionId") + "</DATA> </EXTRADATA>" + " <RECEIPT> <CHARSPERLINE>"
          + xmlReqJSON.getString("charsPerLine") + "</CHARSPERLINE> </RECEIPT>" + " <COMMENT>"
          + xmlReqJSON.getString("docNo") + "</COMMENT>" + " </REQUEST>";
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return reqXML;
  }  
  public static String generateSubscriptionPINPRINTXMLRequest(final JSONObject xmlReqJSON) {
    String reqXML = null;
    try {
      reqXML = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> <REQUEST type=\""
          + xmlReqJSON.getString("requestType") + "\"" + ">" + " <USERNAME>"
          + xmlReqJSON.getString("userName") + "</USERNAME>" + " <PASSWORD>"
          + xmlReqJSON.getString("password") + "</PASSWORD>" + " <TERMINALID>"
          + xmlReqJSON.getString("terminalID") + "</TERMINALID>" + " <LOCALDATETIME>"
          + xmlReqJSON.getString("date") + "</LOCALDATETIME>" + " <TXID>"
          + xmlReqJSON.getString("txnID") + "</TXID>" + " <CASHIER>"
          + xmlReqJSON.getString("sapCode") + "</CASHIER>" + " <AMOUNT>"
          + xmlReqJSON.getString("amount") + "</AMOUNT>" + " <CURRENCY>"
          + xmlReqJSON.getString("currency") + "</CURRENCY>" + " <CARD> <EAN>"
          + xmlReqJSON.getString("ean") + "</EAN> </CARD>" + " <EXTRADATA> <DATA name=\"CONTRACT\">"
          + xmlReqJSON.getString("contractId") + "</DATA> <DATA name=\"REDIRECTIONDIVISIONID\">"
          + xmlReqJSON.getString("redirectionDivisionId") + "</DATA> </EXTRADATA>" + " <RECEIPT> <CHARSPERLINE>"
          + xmlReqJSON.getString("charsPerLine") + "</CHARSPERLINE> </RECEIPT>" + " <COMMENT>"
          + xmlReqJSON.getString("docNo") + "</COMMENT>" + " </REQUEST>";
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return reqXML;
  }    
  public static String generateSubscribePINPRINTCancelXMLRequest(final JSONObject xmlReqJSON) {
    String reqXML = null;
    try {
      reqXML = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> <REQUEST type=\""
          + xmlReqJSON.getString("requestType") + "\"" + ">" + "<AUTHORIZATION><USERNAME>"
          + xmlReqJSON.getString("userName") + "</USERNAME>" + " <PASSWORD>"
          + xmlReqJSON.getString("password") + "</PASSWORD></AUTHORIZATION>" + " <TERMINALID>"
          + xmlReqJSON.getString("terminalID") + "</TERMINALID>" + " <LOCALDATETIME>"
          + xmlReqJSON.getString("date") + "</LOCALDATETIME>" + " <TXID>"
          + xmlReqJSON.getString("txnID") + "</TXID>" + " <TXREF>" + xmlReqJSON.getString("txnRef")
          + "</TXREF>" + " <CASHIER>" + xmlReqJSON.getString("sapCode") + "</CASHIER>"
          + " <AMOUNT>" + xmlReqJSON.getString("amount") + "</AMOUNT>" + " <CURRENCY>"
          + xmlReqJSON.getString("currency") + "</CURRENCY>" + " <CARD> <EAN>"
          + xmlReqJSON.getString("ean") + "</EAN> </CARD>" + " <RECEIPT> <CHARSPERLINE>"
          + xmlReqJSON.getString("charsPerLine") + "</CHARSPERLINE> </RECEIPT>" + " <COMMENT>"
          + xmlReqJSON.getString("docNo") + "</COMMENT>" + " </REQUEST>";
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return reqXML;
  }
  public static String generateSubscribeActivationCancelXMLRequest(final JSONObject xmlReqJSON) {
    String reqXML = null;
    try {
      reqXML = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> <REQUEST type=\""
          + xmlReqJSON.getString("requestType") + "\"" + ">" + "<AUTHORIZATION><USERNAME>"
          + xmlReqJSON.getString("userName") + "</USERNAME>" + " <PASSWORD><"
          + xmlReqJSON.getString("password") + "</PASSWORD>/AUTHORIZATION>" + " <TERMINALID>"
          + xmlReqJSON.getString("terminalID") + "</TERMINALID>" + " <LOCALDATETIME>"
          + xmlReqJSON.getString("date") + "</LOCALDATETIME>" + " <TXID>"
          + xmlReqJSON.getString("txnID") + "</TXID>" + " <TXREF>" + xmlReqJSON.getString("txnRef")
          + "</TXREF>" + " <CASHIER>" + xmlReqJSON.getString("sapCode") + "</CASHIER>"
          + " <AMOUNT>" + xmlReqJSON.getString("amount") + "</AMOUNT>" + " <CURRENCY>"
          + xmlReqJSON.getString("currency") + "</CURRENCY>" + " <CARD> <PAN>"
          + xmlReqJSON.getString("pan") + "</PAN> <EAN>" 
          + xmlReqJSON.getString("ean") + "</EAN> </CARD>" + " <RECEIPT> <CHARSPERLINE>"
          + xmlReqJSON.getString("charsPerLine") + "</CHARSPERLINE> </RECEIPT>" + " <COMMENT>"
          + xmlReqJSON.getString("docNo") + "</COMMENT>" + " </REQUEST>";
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return reqXML;
  }

}