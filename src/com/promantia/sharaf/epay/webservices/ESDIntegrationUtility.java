package com.promantia.sharaf.epay.webservices;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URL;
import java.security.SecureRandom;
import java.util.Date;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.plm.Product;
import org.openbravo.retail.posterminal.OBPOSApplications;
import org.openbravo.service.json.JsonConstants;

import com.promantia.sharaf.epay.CpseESDConfig;
import com.promantia.sharaf.epay.CpseESDTransactions;

public class ESDIntegrationUtility {
  public static CpseESDTransactions esdObj = null;

  private static Logger log = Logger.getLogger(ESDIntegrationUtility.class);

  public static JSONObject fetchESDConfigurations(JSONObject jsonsent) {
    JSONObject retJSON = new JSONObject();

    try {
      OBPOSApplications pos = OBDal.getInstance().get(OBPOSApplications.class,
          jsonsent.getString("pos"));

      if (pos != null) {
        retJSON.put("posTermicalSearchKey", pos.getSearchKey());
      }

      Organization org = OBDal.getInstance().get(Organization.class,
          jsonsent.getString("organization"));
      if (org != null && org.getSapobmpFiliale() != null) {
        retJSON.put("storeId", org.getSapobmpFiliale());
      }

      // check epay webservice window configuration
      CpseESDConfig esdConfigObj = null;
      OBCriteria<CpseESDConfig> esdConfigCriteria = OBDal.getInstance()
          .createCriteria(CpseESDConfig.class);
      if (esdConfigCriteria.list().size() > 1 || esdConfigCriteria.list().size() == 0) {
        retJSON.put("esdConfigWindowError", true);
      } else {
        esdConfigObj = esdConfigCriteria.list().get(0);
        if (esdConfigObj.getValidatePartner() == null || esdConfigObj.getReturnToken() == null
            || esdConfigObj.getCreateOrder() == null) {
          retJSON.put("esdConfigWindowError", true);
        } else if (esdConfigObj.getCpseClient() == null
            || esdConfigObj.getCpseClientSecret() == null || esdConfigObj.getReseller() == null) {
          retJSON.put("esdCredentialError", true);
        } else if (jsonsent.isNull("upc")) {
          retJSON.put("upcError", true);
        }
        retJSON.put("validatePartner", esdConfigObj.getValidatePartner());
        retJSON.put("returnToken", esdConfigObj.getReturnToken());
        retJSON.put("clientId", esdConfigObj.getCpseClient());
        retJSON.put("clientSecret", esdConfigObj.getCpseClientSecret());
        retJSON.put("resellerId", esdConfigObj.getReseller());
        retJSON.put("createOrder", esdConfigObj.getCreateOrder());
        retJSON.put("countryCode", org.getObretcoDbpCountryid().getISOCountryCode());
      }
    } catch (HibernateException e) {
      log.error(e.getMessage(), e);
    } catch (JSONException e) {
      try {
        retJSON.put("countryCode", "AE");
      } catch (JSONException e1) {
        log.error(e.getMessage(), e);
      }
      log.error(e.getMessage(), e);
    }
    return retJSON;

  }

  public static JSONObject checkIfConfigMissing(JSONObject epayConfig) {
    JSONObject errorObj = new JSONObject();
    Boolean terminalError = false;
    Boolean urlError = false;
    Boolean upcError = false;

    try {
      if (epayConfig.has("esdCredentialError")) {
        terminalError = true;
      } else if (epayConfig.has("esdConfigWindowError")) {
        urlError = true;
      } else if (epayConfig.has("upcError")) {
        upcError = true;
      }

      if (terminalError) {
        errorObj.put("errMsg", "Client Id/Client Secret/Reseller Id configuration is missing");
      } else if (urlError) {
        errorObj.put("errMsg",
            "API endpoints are missing in ESD configuration window. Please contact PoS support team");
      } else if (upcError && !epayConfig.has("isDiagnostic")) {
        errorObj.put("errMsg",
            "UPC/EAN is missing for Digital products. Please contact Product Registration Team.");
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    return errorObj;
  }

  public static JSONObject invokeESD(JSONObject esdConfig, JSONObject requestBody,
      JSONObject jsonFromPOS, Date txnDate) {
    String res = null;
    JSONObject responseJSON = new JSONObject();
    JSONObject requestBody2 = requestBody;
    HttpsURLConnection connection = null;
    OBContext.setAdminMode(true);
    try {
      BufferedReader in = null;
      DataOutputStream wr = null;
      InputStream is = null;

      // Set up our SSLContext
      SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
      sslContext.init(null, null, new SecureRandom());

      // from configuration
      String urlPath = esdConfig.getString("url");
      String urlMethod = esdConfig.getString("urlMethod");
      String clientId = esdConfig.getString("clientId");
      String clientSecret = esdConfig.getString("clientSecret");
      String requestType = null;
      
      if (jsonFromPOS.has("isPinPrint")) {
        requestType = "PINPRINTING";
        requestBody2 = createPinPrintRequestBody(esdConfig, jsonFromPOS, txnDate);
      } else if (jsonFromPOS.has("isPinCancel")) {
        requestType = "CANCEL";
        requestBody2 = createPinCancelRequestBody(jsonFromPOS.getJSONObject("esdResponse"));
      } else {
        requestBody2 = requestBody;
      }
      esdObj = saveESDData(requestBody2, jsonFromPOS, txnDate);

      String date = jsonFromPOS.getString("date");
      log.debug("System time from pos is " + date);

      // Calendar calendar = Calendar.getInstance();

      // String txnID = terminalID + calendar.get(Calendar.DATE) + calendar.get(Calendar.MONTH)
      // + calendar.get(Calendar.YEAR) + calendar.get(Calendar.HOUR)
      // + calendar.get(Calendar.MINUTE) + calendar.get(Calendar.SECOND);
      // String txnID = jsonFromPOS.getString("txnId");
      // String charsPerLine = "42";
      int timeOut = 150000;

      URL urlObj = new URL(urlPath);
      connection = (HttpsURLConnection) urlObj.openConnection();
      connection.setRequestMethod(urlMethod);
      connection.setDoOutput(true);
      connection.setRequestProperty("client_secret", clientSecret);
      connection.setRequestProperty("client_id", clientId);
      connection.setRequestProperty("Content-Type", "application/json");
      connection.setRequestProperty("Accept", "application/json");

      connection.setConnectTimeout(timeOut);
      connection.setReadTimeout(timeOut);

      // to access epay services
      connection.setSSLSocketFactory(sslContext.getSocketFactory());

      wr = new DataOutputStream(connection.getOutputStream());
      wr.writeBytes(requestBody2.toString());
      wr.flush();
      wr.close();

      log.debug("response code is " + connection.getResponseCode());
      log.debug("response message is " + connection.getResponseMessage());

      is = connection.getInputStream();
      in = new BufferedReader(new InputStreamReader((is)));
      String inputLine;
      StringBuffer output = new StringBuffer();
      while ((inputLine = in.readLine()) != null) {
        output.append(inputLine);
      }
      in.close();

      log.debug("HTTP response is " + output);
      res = output.toString();

      JSONObject esdResponse = null;
      try {
        esdResponse = new JSONObject(output.toString());
        if (!jsonFromPOS.has("isDiagnostic")) {
          esdResponse.put("TYPE", requestType);
          esdResponse.put("AMOUNT", jsonFromPOS.getString("amount"));
        }
      } catch (Exception e) {
        esdResponse.put("AMOUNT", 1);
        log.error(e, e);
      }

      responseJSON = new JSONObject(res);
      responseJSON.put("HTTPResponseCode", connection.getResponseCode());
      responseJSON.put("HTTPResponseMessage", connection.getResponseMessage());
      responseJSON.put("HTTPResponse", esdResponse);
      responseJSON.put("HTTPRequest", requestBody2.toString());
      responseJSON.put("eResponse", esdResponse.toString());
      if (connection.getResponseCode() == HttpsURLConnection.HTTP_OK) {
        responseJSON.put("eResponseCode", "0");
      } else {
        responseJSON.put("eResponseCode", connection.getResponseCode());
      }

      if (esdResponse != null && esdResponse.has("Message")) {
        responseJSON.put("eResponseMessage", esdResponse.getString("Message"));
      } else if (esdResponse != null && esdResponse.has("error")) {
        responseJSON.put("eResponseMessage", esdResponse.getString("error"));
      } else if (esdResponse != null && esdResponse.has("Header")
          && esdResponse.getJSONObject("Header").has("Message")) {
        responseJSON.put("eResponseMessage",
            esdResponse.getJSONObject("Header").getString("Message"));
      } else if (esdResponse != null && esdResponse.has("TYPE")
          && esdResponse.getString("TYPE").equals("CANCEL")) {
        responseJSON.put("eResponseMessage", "Success");
      }

      if (!responseJSON.has("esdError")) {
        JSONObject eRespObj = null;
        if (jsonFromPOS.has("isPinPrint")) {
          eRespObj = responseJSON.getJSONObject("HTTPResponse");
        } else if (jsonFromPOS.has("isPinCancel")) {
          eRespObj = esdResponse;
        }

        String message = "";
        String status = "";
        String lpoNo = "";
        if (jsonFromPOS.has("isPinPrint")) {
          if (eRespObj != null && eRespObj.has("Status")) {
            status = eRespObj.getJSONObject("HTTPResponse").getString("Status");
          } else if (eRespObj != null && eRespObj.has("Header")
              && eRespObj.getJSONObject("Header").has("Status")) {
            status = eRespObj.getJSONObject("Header").getString("Status");

            if (eRespObj != null && eRespObj.has("Message")) {
              message = eRespObj.getJSONObject("HTTPResponse").getString("Message");
            } else if (eRespObj != null && eRespObj.has("Header")
                && eRespObj.getJSONObject("Header").has("Message")) {
              message = eRespObj.getJSONObject("Header").getString("Message");

              if (eRespObj != null && eRespObj.has("Header")
                  && eRespObj.getJSONObject("Header").has("LPONo")) {
                lpoNo = eRespObj.getJSONObject("Header").getString("LPONo");
              } else {
                try {
                  JSONObject req = new JSONObject(esdResponse.getString("HTTPRequest"));
                  if (req.has("PurchaseOrderId")) {
                    lpoNo = req.getString("PurchaseOrderId");
                  }
                } catch (JSONException e1) {
                  log.error(e1);
                }
              }
            }
          }

        } else if (jsonFromPOS.has("isPinCancel")) {
          if(esdResponse.has("Status")) {
          status = esdResponse.getString("Status");
          }
          if(esdResponse.has("Message")) {
          message = esdResponse.getString("Message");
          }
          if (requestBody2.has("PurchaseOrderId")) {
            lpoNo = requestBody2.getString("PurchaseOrderId");
          }
        }
        if (jsonFromPOS.has("isPinPrint")) {
          esdObj.setESDResponse(responseJSON.getJSONObject("HTTPResponse").toString());
        } else if (jsonFromPOS.has("isPinCancel")) {
          esdObj.setESDResponse(esdResponse.toString());
        }
        esdObj.setProcessed(true);
        esdObj.setStatus(status);
        esdObj.setTransaction(lpoNo);
        esdObj.setResultText(message);
        OBDal.getInstance().save(esdObj);
        OBDal.getInstance().flush();

      }

    } catch (JSONException e) {
      if (!jsonFromPOS.has("isDiagnostic"))
        saveESDErrorData(jsonFromPOS, requestBody2.toString(), res, txnDate, e);
      log.error(e, e);
    } catch (IOException e) {
      boolean isJson = false;
      if (connection.getErrorStream() != null) {
        InputStream is = connection.getErrorStream();
        BufferedReader in = new BufferedReader(new InputStreamReader((is)));
        String inputLine;
        StringBuffer output = new StringBuffer();
        try {
          while ((inputLine = in.readLine()) != null) {
            output.append(inputLine);
          }
          in.close();
          is.close();
        } catch (IOException e1) {
          // TODO Auto-generated catch block
          log.error("Error while reading error stream from server", e1);
        }
        res = output.toString();
        try {
          responseJSON = new JSONObject(res);

          if (responseJSON.has("Result")) {
            responseJSON.put("eResponseMessage", responseJSON.getString("Result"));
            responseJSON.put("eResponseCode", 400);
            responseJSON.put("HTTPResponseCode", 400);
            responseJSON.put("esdError", true);
            isJson = true;
          }
          if (responseJSON.has("errorMessage")) {
            if (responseJSON.has("description")) {
              responseJSON.put("eResponseMessage", responseJSON.getString("description"));
            } else {
              responseJSON.put("eResponseMessage", responseJSON.getString("errorMessage"));
            }
            responseJSON.put("eResponseCode", 500);
            responseJSON.put("HTTPResponseCode", 500);
            responseJSON.put("esdError", true);
            isJson = true;
          }
        } catch (JSONException e1) {
          isJson = false;
          log.error(e1);
        }
      }
      if (!jsonFromPOS.has("isDiagnostic"))
        saveESDErrorData(jsonFromPOS, requestBody2.toString(), res, txnDate, e);
      log.error(e, e);
      if (isJson)
        return responseJSON;
    } catch (Exception exc) {
      if (!jsonFromPOS.has("isDiagnostic"))
        saveESDErrorData(jsonFromPOS, requestBody2.toString(), res, txnDate, exc);
      log.error(exc, exc);
    } finally {
      OBContext.restorePreviousMode();
    }
    return responseJSON;
  }

  public static JSONObject createPinCancelRequestBody(JSONObject esdResponse) {
    JSONObject request = new JSONObject();
    try {
      request.put("ClientTransactionId", esdResponse.getJSONArray("DetailList").getJSONObject(0)
          .getJSONArray("SubsList").getJSONObject(0).getString("ClientTransactionId"));
      request.put("Token", esdResponse.getJSONArray("DetailList").getJSONObject(0)
          .getJSONArray("SubsList").getJSONObject(0).getString("ActivationCode"));
      request.put("PartnerAttributes", "");
      request.put("PurchaseOrderId", esdResponse.getJSONObject("Header").getString("LPONo"));
    } catch (JSONException e) {
      log.error(e, e);
    }
    return request;
  }

  public static JSONObject createPinPrintRequestBody(JSONObject esdConfig, JSONObject jsonFromPOS,
      Date txnDate) {
    JSONObject request = new JSONObject();
    try {
      request.put("resellerId", esdConfig.getString("resellerId"));
      request.put("storeid", esdConfig.getString("storeId"));
      request.put("posid", esdConfig.getString("posTermicalSearchKey"));
      request.put("lpono",
          jsonFromPOS.getString("docNo") + "_" + String.valueOf(System.currentTimeMillis()));
      request.put("countrycode", esdConfig.getString("countryCode"));

      JSONArray itemArray = new JSONArray();
      JSONObject item = new JSONObject();
      item.put("LineId", "10");
      item.put("sku", jsonFromPOS.getString("upc"));
      item.put("quantity", "1");
      item.put("AddlAtt1", "");
      item.put("AddlAtt2", "");
      item.put("AddlAtt3", "");
      itemArray.put(item);

      request.put("itemDetails", itemArray);

    } catch (JSONException e) {
      log.error(e, e);
    }
    return request;
  }

  public static JSONObject processResponse(JSONObject esdResponse) {
    JSONObject respJSON = new JSONObject();
    try {
      if (esdResponse.has("eResponseCode") && esdResponse.has("eResponseMessage")) {
        if (esdResponse.getInt("HTTPResponseCode") == HttpsURLConnection.HTTP_OK) {

          JSONObject esdOriginalRes = esdResponse.getJSONObject("HTTPResponse");
          if (esdOriginalRes != null && ((esdOriginalRes.has("Status")
              && esdOriginalRes.getString("Status").equals("PASS"))
              || (esdOriginalRes.has("Header")
                  && esdOriginalRes.getJSONObject("Header").has("Status")
                  && esdOriginalRes.getJSONObject("Header").getString("Status").equals("PASS"))
              || (esdOriginalRes.has("TYPE") && esdOriginalRes.getString("TYPE") != null
                  && esdOriginalRes.getString("TYPE").equals("CANCEL")))) {
            respJSON.put(JsonConstants.RESPONSE_DATA, esdResponse);
            respJSON.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_SUCCESS);
          } else {
            respJSON.put(JsonConstants.RESPONSE_DATA, esdResponse);
            respJSON.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_FAILURE);
            respJSON.put(JsonConstants.RESPONSE_ERRORMESSAGE, esdOriginalRes.getString("Message"));

          }
          return respJSON;
        } else {
          // Error response from EPAY server
          respJSON.put(JsonConstants.RESPONSE_DATA, esdResponse);
          respJSON.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_FAILURE);
          if (esdResponse.getString("eResponseMessage") == null
              || esdResponse.getString("eResponseMessage").equals("")) {
            respJSON.put(JsonConstants.RESPONSE_ERRORMESSAGE, "An error occured at ESD server");
          } else {
            respJSON.put(JsonConstants.RESPONSE_ERRORMESSAGE,
                esdResponse.getString("eResponseMessage"));
            respJSON.put("message", esdResponse.getString("eResponseMessage"));
          }
          return respJSON;
        }

      } else {
        // No response from EPAY server
        respJSON.put(JsonConstants.RESPONSE_DATA, esdResponse);
        respJSON.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_FAILURE);
        respJSON.put(JsonConstants.RESPONSE_ERRORMESSAGE, "Error Connecting ESD server");
        respJSON.put("message", "Error Connecting ESD server");
        return respJSON;
      }
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return respJSON;
  }

  public static CpseESDTransactions saveESDData(JSONObject requestObj, JSONObject jsonFromPOS,
      Date txnDate) {
    try {
      Organization org = OBDal.getInstance().get(Organization.class,
          jsonFromPOS.getString("organization"));
      Client client = OBDal.getInstance().get(Client.class, jsonFromPOS.getString("client"));
      OBPOSApplications pos = OBDal.getInstance().get(OBPOSApplications.class,
          jsonFromPOS.getString("pos"));
      User user = OBDal.getInstance().get(User.class, jsonFromPOS.getString("salesRep"));
      Product prod = OBDal.getInstance().get(Product.class, jsonFromPOS.getString("prodId"));

      CpseESDTransactions epayTxn = OBProvider.getInstance().get(CpseESDTransactions.class);
      epayTxn.setActive(true);
      epayTxn.setClient(client);
      epayTxn.setOrganization(org);
      epayTxn.setObposApplications(pos);
      epayTxn.setUser(user);
      epayTxn.setAmount(BigDecimal.valueOf(jsonFromPOS.getDouble("amount")));
      epayTxn.setCreatedBy(user);
      epayTxn.setCreationDate(new Date());
      epayTxn.setDocumentno(jsonFromPOS.getString("docNo"));
      epayTxn.setESDRequest(requestObj.toString());
      epayTxn.setProduct(prod);
      epayTxn.setTransactionDate(txnDate);

      // Setting txn type
      String type = null;
      if (jsonFromPOS.has("isPinPrint")) {
        type = "PINPRINTING";
      } else if (jsonFromPOS.has("isPinCancel")) {
        type = "CANCEL";
      }

      epayTxn.setTransactionType(type);
      OBDal.getInstance().save(epayTxn);
      OBDal.getInstance().flush();
      return epayTxn;

    } catch (Exception e) {
      throw new OBException("Error while saving ESD data in ESD Transaction window " + e);
    }

  }

  public static void saveESDErrorData(JSONObject jsonFromPOS, String req, String res, Date txnDate,
      Exception e) {
    try {
      OBCriteria<CpseESDTransactions> esdTxnCriteria = OBDal.getInstance()
          .createCriteria(CpseESDTransactions.class);
      final String id = (String) esdObj.getId();
      esdTxnCriteria.add(Restrictions.eq(CpseESDTransactions.PROPERTY_ID, id));
      List<CpseESDTransactions> esdList = esdTxnCriteria.list();
      CpseESDTransactions esdTransactions = esdList.get(0);
      if (res != null) {
        try {
          JSONObject resJson = new JSONObject(res);

          if (resJson.has("Status")) {
            esdTransactions.setStatus(resJson.getString("Status"));
          }
          if (resJson.has("Result")) {
            esdTransactions.setResultText(resJson.getString("Result"));
          }
        } catch (JSONException e1) {
          log.error("No JSON response", e1);
        }
      }
      if (res != null) {
        esdTransactions.setESDResponse(res);
      }
      esdTransactions.setProcessed(false);
      esdTransactions.setErrorLog(e.getMessage());
      OBDal.getInstance().save(esdTransactions);
      OBDal.getInstance().flush();
    } catch (Exception ex) {
      log.error("Error while saving ESD error data in ESD Transactions window " + ex.getMessage());
      ex.printStackTrace();
    }
  }
}