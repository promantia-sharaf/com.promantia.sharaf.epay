package com.promantia.sharaf.epay.webservices;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.SecureRandom;
import java.util.Date;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import org.json.XML;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.mobile.core.MobileServiceDefinition;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.plm.Product;
import org.openbravo.retail.posterminal.OBPOSApplications;
import org.openbravo.service.json.JsonConstants;

import com.promantia.sharaf.epay.CpseEpayConfig;
import com.promantia.sharaf.epay.CpseEpayTransactions;
import com.promantia.sharaf.epay.util.ModifyJsonKeyUtil;

public class EPayIntegrationUtility {

  private static Logger log = Logger.getLogger(EPayIntegrationUtility.class);
  public static final String STRING_KEY = "String";

  public static final String JSONOBJECT_KEY = "JSONObject";

  public static final String JSONARRAY_KEY = "JSONArray";
 

  public static JSONObject fetchEpayConfigurations(JSONObject jsonsent) {
    JSONObject retJSON = new JSONObject();

    try {
      OBPOSApplications pos = OBDal.getInstance()
          .get(OBPOSApplications.class, jsonsent.getString("pos"));

      // check pos terminal configuration
      if (!(pos.getCpseEpayusername() == null)) {
        retJSON.put("epayUserName", pos.getCpseEpayusername());
      } else {
        retJSON.put("epayUsernameError", true);
      }

      if (!(pos.getCpseEpaypassword() == null)) {
        retJSON.put("epayPassword", pos.getCpseEpaypassword());
      } else {
        retJSON.put("epayPasswordError", true);
      }

      if (!(pos.getCpseEpayterminalid() == null)) {
        retJSON.put("epayTerminalID", pos.getCpseEpayterminalid());
      } else {
        retJSON.put("epayTerminalIDError", true);
      }

      if (jsonsent.isNull("upc")) {
        retJSON.put("upcError", true);
      }

      // check epay webservice window configuration
      CpseEpayConfig epayConfigObj = null;
      OBCriteria<CpseEpayConfig> epayConfigCriteria = OBDal.getInstance()
          .createCriteria(CpseEpayConfig.class);
      if (epayConfigCriteria.list().size() > 1 || epayConfigCriteria.list().size() == 0) {
        retJSON.put("epayConfigWindowError", true);
      } else {
        epayConfigObj = epayConfigCriteria.list().get(0);
        retJSON.put("epayPath", epayConfigObj.getEpayPath());
      }
    } catch (HibernateException e) {
      e.printStackTrace();
    } catch (JSONException e) {
      e.printStackTrace();
    }

    return retJSON;

  }

  public static JSONObject invokeEpay(JSONObject epayConfig, JSONObject jsonFromPOS, Date txnDate) {

    JSONObject epayResponse = new JSONObject();
    CpseEpayTransactions epayTxn = null;
    String xml = null;
    String res = null;
    boolean isduplicate = false;
    try {
      HttpsURLConnection connection = null;
      BufferedReader in = null;
      DataOutputStream wr = null;
      InputStream is = null;

      // Set up our SSLContext
      SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
      sslContext.init(null, null, new SecureRandom());

      // parameters
      // ---------from POS
      String requestType = null;
      String ean = null;
      String amount = null;
      String currency = null;
      String docNo = null;
      String sapCode = null;
      String contractId = null;
      String redirectionDivisionId = null;
      String orgid = jsonFromPOS.getString("organization");
      if (jsonFromPOS.has("isPinPrint")) {
        requestType = "PINPRINTING";
      }

      if (jsonFromPOS.has("isPinCancel")) {
        requestType = "CANCEL";
      }

      if (jsonFromPOS.has("isActivation")) {
        requestType = "ACTIVATE";
      }

      if (jsonFromPOS.has("isDeactivation")) {
        requestType = "DEACTIVATE";
      }

      if (jsonFromPOS.has("isDiagnostic")) {
        requestType = "DIAGNOSTIC";
      }

      if (jsonFromPOS.has("isSubscriptionPINPRINT")
          || jsonFromPOS.has("isSubscriptionActivation")) {
        requestType = "SALE";
      }

      if (jsonFromPOS.has("isSubscribePINPRINTCancel")
          || jsonFromPOS.has("isSubscribeActivationCancel")) {
        requestType = "CANCEL";
      }

      if (jsonFromPOS.has("upc")) {
        ean = jsonFromPOS.getString("upc");
      }

      if (jsonFromPOS.has("amount")) {
        amount = jsonFromPOS.getString("amount");
      }

      if (jsonFromPOS.has("currency")) {
        currency = jsonFromPOS.getString("currency");

        BigDecimal amt = BigDecimal.valueOf(jsonFromPOS.getDouble("amount"));
        if (currency != null && !currency.isEmpty()) {
          OBCriteria<Currency> currencyCriteria = OBDal.getInstance()
              .createCriteria(Currency.class);
          currencyCriteria.add(Restrictions.eq(Currency.PROPERTY_CPSEEPAYISOCODE,
              BigDecimal.valueOf(Long.parseLong(currency))));

          try {
            if (currencyCriteria.list().get(0) != null) {
              Currency currencyConfig = currencyCriteria.list().get(0);
              for (long i = 0L; i < currencyConfig.getStandardPrecision(); i++) {
                amt = amt.multiply(BigDecimal.TEN);
              }
            } else {
              amt = amt.multiply(BigDecimal.valueOf(100));
            }
          } catch (Exception e2) {
            log.error(e2);
            amt = amt.multiply(BigDecimal.valueOf(100));
          }
        } else {
          amt = amt.multiply(BigDecimal.valueOf(100));
        }
        amount = String.valueOf(amt);
        if (amount.contains(".")) {
          amount = amount.split("\\.")[0];
        }
      }

      if (jsonFromPOS.has("docNo")) {
        docNo = jsonFromPOS.getString("docNo");
      }

      if (jsonFromPOS.has("contractId")) {
        contractId = jsonFromPOS.getString("contractId");
      }

      if (jsonFromPOS.has("redirectionDivisionId")) {
        redirectionDivisionId = jsonFromPOS.getString("redirectionDivisionId");
      }

      // from configuration
      String urlPath = epayConfig.getString("epayPath");
      String userName = epayConfig.getString("epayUserName");
      String password = epayConfig.getString("epayPassword");
      String terminalID = epayConfig.getString("epayTerminalID");

      String date = jsonFromPOS.getString("date");
      log.info("System time from pos is " + date);

      // Calendar calendar = Calendar.getInstance();

      // String txnID = terminalID + calendar.get(Calendar.DATE) + calendar.get(Calendar.MONTH)
      // + calendar.get(Calendar.YEAR) + calendar.get(Calendar.HOUR)
      // + calendar.get(Calendar.MINUTE) + calendar.get(Calendar.SECOND);
      String txnID = jsonFromPOS.getString("txnId");
      String charsPerLine = "42";
      int timeOut = 15000;

      URL urlObj = new URL(urlPath);
      connection = (HttpsURLConnection) urlObj.openConnection();
      connection.setRequestMethod("POST");
      connection.setDoOutput(true);
      connection.setRequestProperty("Content-Type", "application/xml");
      connection.setRequestProperty("Accept", "application/xml");

      OBCriteria<MobileServiceDefinition> msCriteria = OBDal.getInstance()
          .createCriteria(MobileServiceDefinition.class);
      if (requestType.equalsIgnoreCase("PINPRINTING")) {
        msCriteria.add(Restrictions.eq(MobileServiceDefinition.PROPERTY_SERVICE,
            "com.promantia.sharaf.epay.process.PINPrintActivation"));
      } else if (requestType.equalsIgnoreCase("CANCEL") && jsonFromPOS.has("isPinCancel")) {
        msCriteria.add(Restrictions.eq(MobileServiceDefinition.PROPERTY_SERVICE,
            "com.promantia.sharaf.epay.process.PINPrintCancellation"));
      } else if (requestType.equalsIgnoreCase("ACTIVATE")) {
        msCriteria.add(Restrictions.eq(MobileServiceDefinition.PROPERTY_SERVICE,
            "com.promantia.sharaf.epay.process.EPAYActivation"));
      } else if (requestType.equalsIgnoreCase("DEACTIVATE")) {
        msCriteria.add(Restrictions.eq(MobileServiceDefinition.PROPERTY_SERVICE,
            "com.promantia.sharaf.epay.process.POSDeactivation"));
      } else if (requestType.equalsIgnoreCase("DIAGNOSTIC")) {
        msCriteria.add(Restrictions.eq(MobileServiceDefinition.PROPERTY_SERVICE,
            "com.promantia.sharaf.epay.process.Diagnostic"));
      } else if (requestType.equalsIgnoreCase("SALE")
          && jsonFromPOS.has("isSubscriptionActivation")) {
        msCriteria.add(Restrictions.eq(MobileServiceDefinition.PROPERTY_SERVICE,
            "com.promantia.sharaf.epay.process.SubscriptionActivation"));
      } else if (requestType.equalsIgnoreCase("SALE")
          && jsonFromPOS.has("isSubscriptionPINPRINT")) {
        msCriteria.add(Restrictions.eq(MobileServiceDefinition.PROPERTY_SERVICE,
            "com.promantia.sharaf.epay.process.SubscriptionPINPRINT"));
      } else if (requestType.equalsIgnoreCase("CANCEL")
          && jsonFromPOS.has("isSubscribeActivationCancel")) {
        msCriteria.add(Restrictions.eq(MobileServiceDefinition.PROPERTY_SERVICE,
            "com.promantia.sharaf.epay.process.SubscribeActivationCancellation"));
      } else if (requestType.equalsIgnoreCase("CANCEL")
          && jsonFromPOS.has("isSubscribePINPRINTCancel")) {
        msCriteria.add(Restrictions.eq(MobileServiceDefinition.PROPERTY_SERVICE,
            "com.promantia.sharaf.epay.process.SubscribePINPRINTCancellation"));
      }

      if (msCriteria.list().get(0).getTimeout() != null) {
        timeOut = msCriteria.list().get(0).getTimeout().intValue() * 1000;
      }
      connection.setConnectTimeout(timeOut);
      connection.setReadTimeout(timeOut);

      // to access epay services
      connection.setSSLSocketFactory(sslContext.getSocketFactory());

      Organization org = OBDal.getInstance().get(Organization.class, orgid);
      if (org.getSapobmpFiliale() != null) {
        sapCode = org.getSapobmpFiliale();
      }

      // setting all required parameters to a JSON obj
      JSONObject xmlReqJSON = new JSONObject();
      xmlReqJSON.put("requestType", requestType);
      xmlReqJSON.put("userName", userName);
      xmlReqJSON.put("password", password);
      xmlReqJSON.put("terminalID", terminalID);
      xmlReqJSON.put("amount", amount);
      xmlReqJSON.put("date", date);
      xmlReqJSON.put("txnID", txnID);
      xmlReqJSON.put("sapCode", sapCode);
      xmlReqJSON.put("ean", ean);
      xmlReqJSON.put("currency", currency);
      xmlReqJSON.put("charsPerLine", charsPerLine);
      xmlReqJSON.put("docNo", docNo);
      if (requestType.equalsIgnoreCase("SALE")) {
        xmlReqJSON.put("contractId", contractId);
        xmlReqJSON.put("redirectionDivisionId", redirectionDivisionId);
      }

      if (requestType.equalsIgnoreCase("SALE") && jsonFromPOS.has("isSubscriptionActivation")) {
        xmlReqJSON.put("serialNo", jsonFromPOS.getString("serialNo"));
      }

      if (requestType.equalsIgnoreCase("CANCEL")
          && jsonFromPOS.has("isSubscribeActivationCancel")) {
        xmlReqJSON.put("serialNo", jsonFromPOS.getString("serialNo"));
        if (jsonFromPOS.has("orignalTerminalId")) {
            xmlReqJSON.put("terminalID", jsonFromPOS.getString("orignalTerminalId"));
          }
      }

      // XML request based on request type
      if (requestType.equalsIgnoreCase("PINPRINTING")) {
        xml = EPAYRequest.generatePINPrintXMLRequest(xmlReqJSON);
      } else if (requestType.equalsIgnoreCase("CANCEL") && jsonFromPOS.has("isPinCancel")) {
        xmlReqJSON.put("txnRef", jsonFromPOS.getString("txnRef"));
        if (jsonFromPOS.has("orignalTerminalId")) {
          xmlReqJSON.put("terminalID", jsonFromPOS.getString("orignalTerminalId"));
        }
        xml = EPAYRequest.generatePINCancelXMLRequest(xmlReqJSON);
      } else if (requestType.equalsIgnoreCase("ACTIVATE")) {
        xmlReqJSON.put("serialNo", jsonFromPOS.getString("serialNo"));
        xml = EPAYRequest.generateActivationXMLRequest(xmlReqJSON);
      } else if (requestType.equalsIgnoreCase("DEACTIVATE")) {
        xmlReqJSON.put("serialNo", jsonFromPOS.getString("serialNo"));
        if (jsonFromPOS.has("orignalTerminalId")) {
          xmlReqJSON.put("terminalID", jsonFromPOS.getString("orignalTerminalId"));
        }
        xml = EPAYRequest.generateDeActivationXMLRequest(xmlReqJSON);
      } else if (requestType.equalsIgnoreCase("DIAGNOSTIC")) {
        xml = EPAYRequest.generateDiagnosticXMLRequest(xmlReqJSON);
      } else if (requestType.equalsIgnoreCase("CANCEL")
          && jsonFromPOS.has("isSubscribePINPRINTCancel")) {
        xmlReqJSON.put("txnRef", jsonFromPOS.getString("txnRef"));
        if (jsonFromPOS.has("orignalTerminalId")) {
            xmlReqJSON.put("terminalID", jsonFromPOS.getString("orignalTerminalId"));
        }
        xml = EPAYRequest.generateSubscribePINPRINTCancelXMLRequest(xmlReqJSON);
      } else if (requestType.equalsIgnoreCase("CANCEL")
          && jsonFromPOS.has("isSubscribeActivationCancel")) {
        xmlReqJSON.put("txnRef", jsonFromPOS.getString("txnRef"));
        xml = EPAYRequest.generateSubscribeActivationCancelXMLRequest(xmlReqJSON);
      } else if (requestType.equalsIgnoreCase("SALE")
          && jsonFromPOS.has("isSubscriptionActivation")) {
        xml = EPAYRequest.generateSubscriptionActivationXMLRequest(xmlReqJSON);
      } else if (requestType.equalsIgnoreCase("SALE")
          && jsonFromPOS.has("isSubscriptionPINPRINT")) {
        xml = EPAYRequest.generateSubscriptionPINPRINTXMLRequest(xmlReqJSON);
      }
     
   // Check duplicate Request
      try {
        txnID = jsonFromPOS.getString("txnId");
        epayTxn = checkDuplicateRequest(txnID,xmlReqJSON);
        if(epayTxn != null && epayTxn.getId() != null) {
          isduplicate  = true;
        }
      }catch(Exception e) {
        log.info("Error while checking Epay duplicate Request " +e.getMessage()); 
       }
      
      if(!isduplicate) {
          epayTxn = saveEpayData(xmlReqJSON, xml, jsonFromPOS, txnDate);
      }
      wr = new DataOutputStream(connection.getOutputStream());
      wr.writeBytes(xml);
      wr.flush();
      wr.close();

      log.info("response code is " + connection.getResponseCode());
      log.info("response message is " + connection.getResponseMessage());

      is = connection.getInputStream();
      in = new BufferedReader(new InputStreamReader((is)));
      String inputLine;
      StringBuffer output = new StringBuffer();
      while ((inputLine = in.readLine()) != null) {
        output.append(inputLine);
      }
      in.close();

      log.info("HTTP response is " + output);
      epayResponse.put("HTTPResponseCode", connection.getResponseCode());
      epayResponse.put("HTTPResponseMessage", connection.getResponseMessage());
      epayResponse.put("HTTPResponse", output);
      epayResponse.put("HTTPRequest", xml);
      res = output.toString();

      // convert XML to JSON
      JSONObject xmlJSONObj = convertXMLtoJSON(epayResponse);
      epayResponse.put("eResponseCode", xmlJSONObj.getString("RESULT")); // epay-server-response-code
      epayResponse.put("eResponseMessage", xmlJSONObj.getString("RESULTTEXT")); // epay-server-response-message
      epayResponse.put("eResponse", xmlJSONObj);
      
      JSONObject eRespObj = new JSONObject(epayResponse.getString("eResponse"));
      
      log.info("JSON response is " + xmlJSONObj);
      saveEpayResponseData(epayResponse, xml, res, txnDate,epayTxn);
      
    } catch (MalformedURLException e) {
      saveEpayErrorData(jsonFromPOS, xml, res, txnDate,epayTxn.getId(), e);
      log.error("Error in Epay due to malformed URL " + e.getMessage());
      e.printStackTrace();
    } catch (ProtocolException e) {
      saveEpayErrorData(jsonFromPOS, xml, res, txnDate,epayTxn.getId(), e);
      log.error("Error in Epay due to protocol exception " + e.getMessage());
      e.printStackTrace();
    } catch (JSONException e) {
      saveEpayErrorData(jsonFromPOS, xml, res, txnDate,epayTxn.getId(), e);
      log.error("Error in Epay due to JSON exception " + e.getMessage());
      e.printStackTrace();
    } catch (IOException e) {
      saveEpayErrorData(jsonFromPOS, xml, res, txnDate,epayTxn.getId(), e);
      log.error("Error in Epay due to IOException " + e.getMessage());
      e.printStackTrace();
    } catch (Exception exc) {
      saveEpayErrorData(jsonFromPOS, xml, res, txnDate, epayTxn.getId(),exc);
      log.error("Error in Epay " + exc.getMessage());
      exc.printStackTrace();
    }

    return epayResponse;
  }

  public static JSONObject convertXMLtoJSON(JSONObject epayResponse) {
    org.json.JSONObject xmlJSONObj;
    JSONObject jsonObj = null;
    net.sf.json.JSONObject object = null;
    String str = null;

    try {
      String TEST_XML_STRING = epayResponse.getString("HTTPResponse");
      if (TEST_XML_STRING.contains("<LINE>Serial number:</LINE>")) {
        int indexOfKey = TEST_XML_STRING.indexOf("<LINE>Serial number:</LINE>");
        String sub = TEST_XML_STRING.substring(indexOfKey + 6);
        int secondKey = sub.indexOf("<LINE>");
        int secondFinalKey = indexOfKey + secondKey + 6;
        String firstPart = TEST_XML_STRING.substring(0, indexOfKey);
        String secondPart = TEST_XML_STRING.substring(secondFinalKey + 6);
        TEST_XML_STRING = firstPart + "<LINE>Serial number:" + secondPart;
      }
      xmlJSONObj = XML.toJSONObject(TEST_XML_STRING);

      jsonObj = new JSONObject(xmlJSONObj.get("RESPONSE").toString());
      str = xmlJSONObj.get("RESPONSE").toString();
      object = net.sf.json.JSONObject.fromObject(str);
      object = ModifyJsonKeyUtil.transferJsonKey(object);
      String finalStr = object.toString();
      jsonObj = new JSONObject(finalStr);

      // jsonPrettyPrintString = xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);
    } catch (org.json.JSONException e) {
      e.printStackTrace();
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return jsonObj;

  }

  public static JSONObject checkIfConfigMissing(JSONObject epayConfig) {
    JSONObject errorObj = new JSONObject();
    Boolean terminalError = false;
    Boolean urlError = false;
    Boolean upcError = false;

    try {
      if (epayConfig.has("epayUsernameError")) {
        terminalError = true;
      } else if (epayConfig.has("epayPasswordError")) {
        terminalError = true;
      } else if (epayConfig.has("epayTerminalIDError")) {
        terminalError = true;
      } else if (epayConfig.has("epayConfigWindowError")) {
        urlError = true;
      } else if (epayConfig.has("upcError")) {
        upcError = true;
      }

      if (terminalError) {
        errorObj.put("errMsg", "User Name/password/terminalID configuration is missing");
      } else if (urlError) {
        errorObj.put("errMsg", "URL is missing in EPAY configuration window");
      } else if (upcError && !epayConfig.has("isDiagnostic")) {
        errorObj.put("errMsg",
            "UPC/EAN is missing for Digital products. Please contact Product Registration Team.");
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    return errorObj;
  }

  public static JSONObject processResponse(JSONObject epayResponse) {
    JSONObject respJSON = new JSONObject();
    try {
      if (epayResponse.has("eResponseCode") && epayResponse.has("eResponseMessage")) {
        if (epayResponse.getInt("eResponseCode") == JsonConstants.RPCREQUEST_STATUS_SUCCESS
            && epayResponse.getInt("HTTPResponseCode") == HttpsURLConnection.HTTP_OK) {
          // success response from EPAY server
          respJSON.put(JsonConstants.RESPONSE_DATA, epayResponse);
          respJSON.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_SUCCESS);
          return respJSON;
        } else {
          // Error response from EPAY server
          respJSON.put(JsonConstants.RESPONSE_DATA, epayResponse);
          respJSON.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_FAILURE);
          respJSON.put(JsonConstants.RESPONSE_ERRORMESSAGE,
              epayResponse.getString("eResponseMessage"));
          return respJSON;
        }

      } else {
        // No response from EPAY server
        respJSON.put(JsonConstants.RESPONSE_DATA, epayResponse);
        respJSON.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_FAILURE);
        respJSON.put(JsonConstants.RESPONSE_ERRORMESSAGE, "Error Connecting EPAY server");
        return respJSON;
      }
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return respJSON;
  }

  public static CpseEpayTransactions saveEpayData(JSONObject xmlReqJSON, String xml,
      JSONObject jsonFromPOS, Date txnDate) {
    try {
      Organization org = OBDal.getInstance()
          .get(Organization.class, jsonFromPOS.getString("organization"));
      Client client = OBDal.getInstance().get(Client.class, jsonFromPOS.getString("client"));
      OBPOSApplications pos = OBDal.getInstance()
          .get(OBPOSApplications.class, jsonFromPOS.getString("pos"));
      User user = OBDal.getInstance().get(User.class, jsonFromPOS.getString("salesRep"));
      Product prod = OBDal.getInstance().get(Product.class, jsonFromPOS.getString("prodId"));

      OBCriteria<Currency> currencyCriteria = OBDal.getInstance().createCriteria(Currency.class);
      currencyCriteria.add(Restrictions.eq(Currency.PROPERTY_CPSEEPAYISOCODE,
          BigDecimal.valueOf(Long.parseLong(xmlReqJSON.getString("currency")))));

      CpseEpayTransactions epayTxn = OBProvider.getInstance().get(CpseEpayTransactions.class);
      epayTxn.setActive(true);
      epayTxn.setClient(client);
      epayTxn.setOrganization(org);
      epayTxn.setPOSTerminal(pos);
      epayTxn.setUserContact(user);
      epayTxn.setAmount(BigDecimal.valueOf(jsonFromPOS.getDouble("amount")));
      epayTxn.setCreatedBy(user);
      epayTxn.setCreationDate(new Date());
      epayTxn.setDocumentNo(jsonFromPOS.getString("docNo"));
      epayTxn.setEpayRequest(xml);
      epayTxn.setProduct(prod);
      epayTxn.setTransactionDate(txnDate);

      // Setting txn type
      String type = null;

      if (xmlReqJSON.getString("requestType").equalsIgnoreCase("PINPRINTING")) {
        type = "P";
      } else if (xmlReqJSON.getString("requestType").equalsIgnoreCase("CANCEL")) {
        type = "C";
      } else if (xmlReqJSON.getString("requestType").equalsIgnoreCase("ACTIVATE")) {
        type = "A";
      } else if (xmlReqJSON.getString("requestType").equalsIgnoreCase("DEACTIVATE")) {
        type = "D";
      } else if (xmlReqJSON.getString("requestType").equalsIgnoreCase("SALE")) {
        type = "S";
      }

      epayTxn.setTransactionType(type);
      OBDal.getInstance().save(epayTxn);
      OBDal.getInstance().flush();
      return epayTxn;
    } catch (Exception e) {
      throw new OBException("Error while saving EPAY data in EPAY Transaction window " + e);
    }

  }

  public static void saveEpayErrorData(JSONObject jsonFromPOS, String req, String res, Date txnDate,String epayTxnId,
      Exception e) {
    try {
      OBCriteria<CpseEpayTransactions> epayTxnCriteria = OBDal.getInstance()
          .createCriteria(CpseEpayTransactions.class);
      final String id = (String) epayTxnId;
      epayTxnCriteria.add(Restrictions.eq(CpseEpayTransactions.PROPERTY_ID, id));
      List<CpseEpayTransactions> epayTxnList = epayTxnCriteria.list();
      CpseEpayTransactions epayTxnUpdate = epayTxnList.get(0);
      if (res != null) {
        epayTxnUpdate.setEpayResponse(res);
      }
      epayTxnUpdate.setProcessed(false);
      epayTxnUpdate.setErrorLog(e.getMessage());
      OBDal.getInstance().save(epayTxnUpdate);
      OBDal.getInstance().flush();

    } catch (Exception ex) {
      log.error(
          "Error while saving EPAY error data in EPAY Transactions window " + ex.getMessage());
      ex.printStackTrace();
    }
  }
  public static void saveEpayResponseData(JSONObject epayResponse, String req, String res, Date txnDate,
		  CpseEpayTransactions epayTxn) {
	  	  log.info("Inside Save Epay Response Data"+epayResponse);
	  try {
		  JSONObject eRespObj = new JSONObject(epayResponse.getString("eResponse"));
		  OBCriteria<CpseEpayTransactions> epayTxnCriteria = OBDal.getInstance()
	            .createCriteria(CpseEpayTransactions.class);
	      final String id = (String) epayTxn.getId();
	      epayTxnCriteria.add(Restrictions.eq(CpseEpayTransactions.PROPERTY_ID, id));
	      List<CpseEpayTransactions> epayTxnList = epayTxnCriteria.list();
	      CpseEpayTransactions epayTxnUpdate = epayTxnList.get(0);
	      epayTxnUpdate.setEpayResponse(epayResponse.getString("HTTPResponse"));
	      epayTxnUpdate.setTransaction(eRespObj.getString("TXID"));
	      epayTxnUpdate.setStatus(eRespObj.getString("RESULTTEXT"));
	      epayTxnUpdate.setProcessed(true);
	      OBDal.getInstance().save(epayTxnUpdate);
	      OBDal.getInstance().flush();
	      
	    } catch (Exception ex) {
	      log.error(
	          "Error while saving EPAY Response data in EPAY Transactions window " + ex.getMessage());
	      ex.printStackTrace();
	    }
	  }
  
  public static CpseEpayTransactions checkDuplicateRequest(String txnID,JSONObject xmlReqJSON) {

    try {
        String arr[]=txnID.split("_");
        txnID = arr[0]+"_";
        log.info(txnID);
        String type=null;
        if (xmlReqJSON.getString("requestType").equalsIgnoreCase("PINPRINTING")) {
          type = "P";
        } else if (xmlReqJSON.getString("requestType").equalsIgnoreCase("CANCEL")) {
          type = "C";
        } else if (xmlReqJSON.getString("requestType").equalsIgnoreCase("ACTIVATE")) {
          type = "A";
        } else if (xmlReqJSON.getString("requestType").equalsIgnoreCase("DEACTIVATE")) {
          type = "D";
        } else if (xmlReqJSON.getString("requestType").equalsIgnoreCase("SALE")) {
          type = "S";
        }
        log.info("type "+type);
        OBQuery<CpseEpayTransactions> oldtrans = OBDal.getInstance()
                    .createQuery(CpseEpayTransactions.class, CpseEpayTransactions.PROPERTY_TRANSACTION + " like :txnID" +" and "
                              + CpseEpayTransactions.PROPERTY_TRANSACTIONTYPE + " = :type");
        oldtrans.setNamedParameter("txnID", txnID+'%');
        oldtrans.setNamedParameter("type",type);
        oldtrans.setFilterOnActive(false);
        oldtrans.setMaxResult(1);
        CpseEpayTransactions oldRequest =oldtrans.uniqueResult();
        return oldRequest;
      } catch (Exception ex) {
        log.info(
            "Epay Duplicate Request check " + ex.getMessage());
      }
    return null;
    }
}
