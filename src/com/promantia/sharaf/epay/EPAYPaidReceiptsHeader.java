package com.promantia.sharaf.epay;

import java.text.ParseException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.businessUtility.Preferences;
import org.openbravo.erpCommon.utility.PropertyException;
import org.openbravo.mobile.core.process.ProcessHQLQuery;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.retail.posterminal.PaidReceiptsHeader;
import org.openbravo.retail.posterminal.PaidReceiptsHeaderHook;
import org.openbravo.retail.posterminal.TerminalType;

public class EPAYPaidReceiptsHeader extends ProcessHQLQuery {
  public static final Logger log = Logger.getLogger(PaidReceiptsHeader.class);

  @Inject
  @Any
  private Instance<PaidReceiptsHeaderHook> paidReceiptHeaderHooks;

  @Override
  protected boolean isAdminMode() {
    return true;
  }

  @Override
  protected Map<String, Object> getParameterValues(JSONObject jsonsent) throws JSONException {
    try {
      OBContext.setAdminMode(true);
      boolean useContains = true;
      try {
        OBContext.setAdminMode(false);
        useContains = "Y".equals(Preferences.getPreferenceValue("OBPOS_remote.order_usesContains",
            true, OBContext.getOBContext().getCurrentClient(),
            OBContext.getOBContext().getCurrentOrganization(), OBContext.getOBContext().getUser(),
            OBContext.getOBContext().getRole(), null));
      } catch (PropertyException e) {
        log.error("Error getting preference OBPOS_remote.order_usesContains " + e.getMessage(), e);
      } finally {
        OBContext.restorePreviousMode();
      }
      Map<String, Object> paramValues = new HashMap<String, Object>();

      JSONObject json = jsonsent.getJSONObject("filters");
      if (!json.getString("filterText").isEmpty()) {
        if (json.has("isReinvoice") && json.getBoolean("isReinvoice")) {
          paramValues.put("filterT1", json.getString("filterText").trim());
        } else if (useContains) {
          paramValues.put("filterT1", ("%" + json.getString("filterText").trim() + "%"));
        } else {
          paramValues.put("filterT1", (json.getString("filterText").trim() + "%"));
        }
      }
      if (jsonsent.has("filters")) {
        if (!json.isNull("documentType")) {
          if (json.getBoolean("isReinvoice")) {
            if (OBContext.getOBContext().getCurrentOrganization().getObposCrossStore() == null) {
              paramValues.put("documentTypes", json.getJSONArray("documentType"));
            } else {
              Set<String> orgTree = OBContext.getOBContext().getOrganizationStructureProvider()
                  .getNaturalTree(OBContext.getOBContext().getCurrentOrganization()
                      .getObposCrossStore().getId());
              String orgList = "";
              Set<String> docTypesList = new HashSet<String>();
              for (String orgId : orgTree) {
                Organization org = OBDal.getInstance().get(Organization.class, orgId);
                if (!orgList.equals("")) {
                  orgList += ",";
                }
                orgList += "'" + org.getId() + "'";
                getTerminalTypeDocument(docTypesList, org, false, true);
              }

              paramValues.put("documentTypes", docTypesList);
              jsonsent.put("organizationList", orgList);
            }
          } else {
            paramValues.put("documentTypes", json.getJSONArray("documentType"));
          }
        }
        if (!json.getString("docstatus").isEmpty() && !json.getString("docstatus").equals("null")) {
          paramValues.put("docstatus", json.getString("docstatus"));
        }
        try {
          if (!json.getString("startDate").isEmpty()) {
            paramValues.put("startDate",
                getDateFormated(json.getString("startDate"), "yyyy-MM-dd"));
          }
          if (!json.getString("endDate").isEmpty()) {
            paramValues.put("endDate", getDateFormated(json.getString("endDate"), "yyyy-MM-dd"));
          }
        } catch (ParseException e) {
          log.error(e.getMessage(), e);
        }
      }
      return paramValues;

    } finally {
      OBContext.restorePreviousMode();
    }
  }

  @Override
  protected List<String> getQuery(JSONObject jsonsent) throws JSONException {

    // OBContext.setAdminMode(true);
    JSONObject json = jsonsent.getJSONObject("filters");
    String strIsLayaway = "false";
    if (json.getBoolean("isLayaway")) {
      strIsLayaway = "true";
    }
    String hqlPaidReceipts = "select ord.id as id, ord.documentNo as documentNo, ord.orderDate as orderDate, "
        + "ord.businessPartner.name as businessPartner, ord.grandTotalAmount as totalamount, ord.documentType.id as documentTypeId, '"
        + strIsLayaway + "' as isLayaway from Order as ord " + "where ord.client='"
        + json.getString("client") + "' and ord.obposIsDeleted = false ";

    hqlPaidReceipts += " and ord.organization.id in (:organization) ";

    if (!json.getString("filterText").isEmpty()) {
      String hqlFilter = "";
      if (json.getBoolean("isReinvoice")) {
        hqlFilter = "ord.documentNo = :filterT1 ";
      } else {
        hqlFilter = "ord.documentNo like :filterT1 or upper(ord.businessPartner.name) like upper(:filterT1)";
      }
      for (PaidReceiptsHeaderHook hook : paidReceiptHeaderHooks) {
        try {
          String hql = hook.exec(hqlFilter, json.getString("filterText"));
          hqlFilter = hql;
        } catch (Exception e) {
          throw new OBException("An error happened when computing a filter in PaidReceipts", e);
        }
      }
      hqlPaidReceipts += " and (" + hqlFilter + ") ";
    }
    if (!json.isNull("documentType")) {
      hqlPaidReceipts += " and ( ord.documentType.id in (:documentTypes) ) ";
    }
    if (!json.getString("docstatus").isEmpty() && !json.getString("docstatus").equals("null")) {
      hqlPaidReceipts += " and ord.documentStatus= :docstatus";
    }
    if (!json.getString("startDate").isEmpty()) {
      hqlPaidReceipts += " and ord.orderDate >= :startDate ";
    }
    if (!json.getString("endDate").isEmpty()) {
      hqlPaidReceipts += " and ord.orderDate <= :endDate ";
    }

    if (json.has("isQuotation") && json.getBoolean("isQuotation")) {
      // not more filters
    } else if (json.getBoolean("isLayaway")) {
      // (It is Layaway)
      hqlPaidReceipts += " and ord.obposApplications is not null and ord.obposIslayaway = true and ord.cancelledorder is null and ord.documentStatus = 'CO'";

    } else if (json.getBoolean("isReturn")) {
      // (It is a Return)
      hqlPaidReceipts += " and ord.obposApplications is not null and ord.cancelledorder is null and exists (select 1 from ord.orderLineList where deliveredQuantity > 0) ";
    } else if (json.has("isReinvoice") && json.getBoolean("isReinvoice")) {
      // (show verified returned lines)
      hqlPaidReceipts += " and ord.obposApplications is not null and exists (select 1 from ord.orderLineList where deliveredQuantity < 0 and cpseEpayInvoicetype = 'Re Invoice') ";
    } else {
      // (It is not Layaway and It is not a Return)
      hqlPaidReceipts += " and ord.obposApplications is not null and exists (select 1 from ord.orderLineList where deliveredQuantity != 0) ";
    }

    hqlPaidReceipts += " order by ord.orderDate desc, ord.documentNo desc";
    return Arrays.asList(new String[] { hqlPaidReceipts });
  }

  @Override
  protected boolean bypassPreferenceCheck() {
    return true;
  }

  private void getTerminalTypeDocument(Set<String> docTypesList, Organization org,
      boolean isQuotation, boolean includeReturns) {
    OBCriteria<TerminalType> terminalTypeCri = OBDal.getInstance()
        .createCriteria(TerminalType.class);
    terminalTypeCri.add(Restrictions.eq(TerminalType.PROPERTY_ORGANIZATION, org));
    terminalTypeCri.setFilterOnReadableOrganization(false);
    for (TerminalType typeObj : terminalTypeCri.list()) {
      if (isQuotation && typeObj.getDocumentTypeForQuotations() != null) {
        docTypesList.add(typeObj.getDocumentTypeForQuotations().getId());
      } else if (!isQuotation) {
        docTypesList.add(typeObj.getDocumentType().getId());
        if (includeReturns) {
          docTypesList.add(typeObj.getDocumentTypeForReturns().getId());
        }
      }
    }
  }
}
